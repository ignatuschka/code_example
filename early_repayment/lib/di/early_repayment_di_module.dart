import 'package:api/api_client.dart';
import 'package:early_repayment/data/data_sources/early_repayment_remote_data_source.dart';
import 'package:early_repayment/data/network/early_repayment_api.dart';
import 'package:early_repayment/data/network/precomputation_api.dart';
import 'package:early_repayment/data/repository/early_repayment_repository_impl.dart';
import 'package:early_repayment/data/repository/repayment_state_repository_impl.dart';
import 'package:early_repayment/domain/entity/create_application_params.dart';
import 'package:early_repayment/domain/repository/early_repayment_repository.dart';
import 'package:early_repayment/domain/repository/repayment_state_repository.dart';
import 'package:early_repayment/domain/usecase/cancel_application_usecase.dart';
import 'package:early_repayment/domain/usecase/create_application_usecase.dart';
import 'package:early_repayment/domain/usecase/get_application_data_usecase.dart';
import 'package:early_repayment/domain/usecase/get_applications_usecase.dart';
import 'package:early_repayment/domain/usecase/get_availability_usecase.dart';
import 'package:early_repayment/domain/usecase/get_precomputation_usecase.dart';
import 'package:early_repayment/domain/usecase/get_state_use_case.dart';
import 'package:early_repayment/domain/usecase/get_status_cancellation.dart';
import 'package:early_repayment/domain/usecase/get_status_creation_usecase.dart';
import 'package:early_repayment/domain/usecase/init_applications_use_case.dart';
import 'package:early_repayment/domain/usecase/init_state_use_case.dart';
import 'package:early_repayment/domain/usecase/otp_verify_usecase.dart';
import 'package:early_repayment/domain/usecase/repeat_otp_code.dart';
import 'package:early_repayment/domain/usecase/send_otp_code.dart';
import 'package:early_repayment/domain/usecase/update_state_use_case.dart';
import 'package:early_repayment/l10n/early_repayment.localizations.dart';
import 'package:early_repayment/navigation/early_repayment_navigator.dart';
import 'package:early_repayment/presentation/pages/confirm_bottom_sheet/sms_confirm_bloc/sms_confirm_bloc.dart';
import 'package:early_repayment/presentation/pages/confirm_bottom_sheet/sms_confirm_bottom_sheet.dart';
import 'package:early_repayment/presentation/pages/confirmation_third_step/bloc/confirmation_third_step_bloc.dart';
import 'package:early_repayment/presentation/pages/confirmation_third_step/confirmation_third_step_screen.dart';
import 'package:early_repayment/presentation/pages/early_repayment_details_screen/bloc/early_repayment_details_screen_bloc.dart';
import 'package:early_repayment/presentation/pages/early_repayment_details_screen/early_repayment_details_screen.dart';
import 'package:early_repayment/presentation/pages/early_repayment_first_step/bloc/early_repayment_first_step_bloc.dart';
import 'package:early_repayment/presentation/pages/early_repayment_first_step/early_repayment_first_step_screen.dart';
import 'package:early_repayment/presentation/pages/early_repayment_screen/bloc/early_repayment_screen_bloc.dart';
import 'package:early_repayment/presentation/pages/early_repayment_screen/early_repayment_screen.dart';
import 'package:early_repayment/presentation/pages/early_repayment_waiting/cubit/early_repayment_waiting_screen_cubit.dart';
import 'package:early_repayment/presentation/pages/error_screen/error_screen.dart';
import 'package:early_repayment/presentation/pages/full_repayment_second_step/bloc/full_repayment_second_step_bloc.dart';
import 'package:early_repayment/presentation/pages/full_repayment_second_step/full_repayment_second_step_screen.dart';
import 'package:early_repayment/presentation/pages/part_repayment_second_step/bloc/part_repayment_second_step_screen_bloc.dart';
import 'package:early_repayment/presentation/pages/part_repayment_second_step/part_repayment_second_step_screen.dart';
import 'package:early_repayment/presentation/pages/result_screen/bloc/early_repayment_result_cubit.dart';
import 'package:early_repayment/presentation/pages/result_screen/cancel_application_result_screen.dart';
import 'package:early_repayment/presentation/pages/result_screen/early_repayment_result_screen.dart';
import 'package:early_repayment/presentation/pages/early_repayment_waiting/early_repayment_waiting_screen.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_atb_common/di/get_it_extensions.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';

class EarlyRepaymentDIModule extends BaseDIModule {
  @override
  void updateInjections(GetIt instance) {
    instance.registerFactory<EarlyRepaymentRemoteDataSource>(() => EarlyRepaymentRemoteDataSourceImpl(
        api: instance.get(),
        precomputationApi: instance.get(),
        requestPullOrchestrator: instance.get(),
        remoteConfig: instance.get()));

    instance.registerFactory<EarlyRepaymentRepository>(
        () => EarlyRepaymentRepositoryImpl(remoteDataSource: instance.get()));
    instance.registerLazySingleton<RepaymentStateRepository>(() => RepaymentStateRepositoryImpl());

    instance.registerFactory(() => EarlyRepaymentApi(
        instance.get(instanceName: kDebugMode ? ApiClientType.longTimeout.name : ApiClientType.base.name)));
    instance.registerFactory(() => PrecomputationApi(instance.get(instanceName: ApiClientType.base.name)));

    instance.registerFactory(() => GetAvailabilityUseCase(instance.get()));
    instance.registerFactory(() => GetApplicationsUseCase(instance.get()));
    instance.registerFactory(() => GetApplicationDataUseCase(instance.get()));
    instance.registerFactory(() => GetPrecomputationUseCase(instance.get()));
    instance.registerFactory(() => SendOtpCodeUseCase(instance.get()));
    instance.registerFactory(() => RepeatOtpCodeUseCase(instance.get()));
    instance.registerFactory(() => CreateApplicationUseCase(instance.get()));
    instance.registerFactory(() => GetStateUseCase(instance.get()));
    instance.registerFactory(() => UpdateStateUseCase(instance.get()));
    instance.registerFactory(() => InitStateUseCase(instance.get()));
    instance.registerFactory(() => CancelApplicationUseCase(instance.get()));
    instance.registerFactory(() => InitApplicationsUseCase(instance.get()));
    instance.registerFactory(() => OtpVerifyCodeUseCase(instance.get()));
    instance.registerFactory(() => GetStatusCreationUseCase(instance.get()));
    instance.registerFactory(() => GetStatusCancellationUseCase(instance.get()));

    instance.registerFactory(() => EarlyRepaymentInternalNavigator());
  }
}

class EarlyRepayment extends StatelessWidget {
  const EarlyRepayment(
      {Key? key, required this.productId, required this.courtDebt, required this.currency, required this.accountNumber})
      : super(key: key);

  final String productId;
  final double courtDebt;
  final String currency;
  final String accountNumber;

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => EarlyRepaymentScreenBloc(
          initApplicationsUseCase: getIt(),
          errorHandler: getIt(),
          getApplicationsUseCase: getIt(),
          productId: productId,
          currency: currency,
          courtDebt: courtDebt,
          navigator: getIt(),
          getAvailabilityUseCase: getIt(),
          accountNumber: accountNumber,
          initStateUseCase: getIt()),
      child: const EarlyRepaymentScreen(),
    );
  }
}

class EarlyRepaymentResultPage extends StatelessWidget {
  const EarlyRepaymentResultPage({super.key});

  @override
  Widget build(BuildContext context) => BlocProvider(
        create: (context) => EarlyRepaymentResultCubit(getIt(), getIt(), getIt(), getIt()),
        child: const EarlyRepaymentResultView(),
      );
}

class CancelApplicationResultPage extends StatelessWidget {
  const CancelApplicationResultPage({super.key});

  @override
  Widget build(BuildContext context) => BlocProvider(
        create: (context) => EarlyRepaymentResultCubit(getIt(), getIt(), getIt(), getIt()),
        child: const CancelApplicationResultView(),
      );
}

class EarlyRepaymentDetails extends StatelessWidget {
  const EarlyRepaymentDetails({super.key, required this.applicationId});

  final String applicationId;

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => EarlyRepaymentDetailsScreenBloc(
        errorHandler: getIt(),
        applicationDataUseCase: getIt(),
        applicationId: applicationId,
        cancelApplicationUseCase: getIt(),
        accountTransferNavigator: getIt(),
        getAccountByNumberUseCase: getIt(),
        getStateUseCase: getIt(),
        earlyRepaymentNavigator: getIt(),
        updateStateUseCase: getIt(),
      ),
      child: const EarlyRepaymentDetailsScreen(),
    );
  }
}

class EarlyRepaymentFirstStep extends StatelessWidget {
  const EarlyRepaymentFirstStep({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => EarlyRepaymentFirstStepBloc(
          getStateUseCase: getIt(), errorHandler: getIt(), updateStateUseCase: getIt(), navigator: getIt()),
      child: const EarlyRepaymentFirstStepScreen(),
    );
  }
}

class PartRepaymentSecondStep extends StatelessWidget {
  const PartRepaymentSecondStep({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => PartRepaymentSecondStepScreenBloc(
        getPrecomputationUseCase: getIt(),
        getStateUseCase: getIt(),
        updateStateUseCase: getIt(),
        navigator: getIt(),
        errorHandler: getIt(),
      ),
      child: const PartRepaymentSecondStepScreen(),
    );
  }
}

class FullRepaymentSecondStep extends StatelessWidget {
  const FullRepaymentSecondStep({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => FullRepaymentSecondStepScreenBloc(
        getPrecomputationUseCase: getIt(),
        errorHandler: getIt(),
        navigator: getIt(),
        getStateUseCase: getIt(),
        updateStateUseCase: getIt(),
      ),
      child: const FullRepaymentSecondStepScreen(),
    );
  }
}

class ConfirmationThirdStep extends StatelessWidget {
  const ConfirmationThirdStep({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => ConfirmationThirdStepBloc(
        localizations: EarlyRepaymentLocalizations.of(context),
        createApplicationUseCase: getIt(),
        errorHandler: getIt(),
        repeatOtpCodeUseCase: getIt(),
        sendOtpCodeUseCase: getIt(),
        navigator: getIt(),
        getStateUseCase: getIt(),
      ),
      child: const ConfirmationThirdStepScreen(),
    );
  }
}

class SmsConfirmBottomSheet extends StatelessWidget {
  const SmsConfirmBottomSheet({super.key});

  static Future<void> showBottomSheet({
    required BuildContext context,
    required int repeatSeconds,
    required String title,
    required CreateApplicationParams params,
    required String smsVerificationSessionId,
  }) async {
    showModalBottomSheet(
      isScrollControlled: true,
      context: context,
      shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.only(topLeft: Radius.circular(24), topRight: Radius.circular(24))),
      builder: (_) => BlocProvider<SmsConfirmBloc>(
        create: (_) => SmsConfirmBloc(
          sendOtpCodeUseCase: getIt(),
          repeatOtpCodeUseCase: getIt(),
          otpVerifyCodeUseCase: getIt(),
          createApplicationUseCase: getIt(),
          params: params,
          navigator: getIt(),
          repeatSeconds: repeatSeconds,
          title: title,
          smsVerificationSessionId: smsVerificationSessionId,
          updateStateUseCase: getIt(),
        ),
        lazy: false,
        child: const SmsConfirmBottomSheetView(),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return const Placeholder();
  }
}

class EarlyRepaymentErrorScreen extends StatelessWidget {
  final String? code;
  final bool isCreatingAppScreenError;
  const EarlyRepaymentErrorScreen({super.key, required this.code, required this.isCreatingAppScreenError});

  @override
  Widget build(BuildContext context) {
    return EarlyRepaymentErrorView(
        onTap: getIt<EarlyRepaymentNavigator>().navigateToChat,
        code: code,
        isCreatingAppScreenError: isCreatingAppScreenError);
  }
}

class EarlyRepaymentWaiting extends StatelessWidget {
  const EarlyRepaymentWaiting({super.key});

  @override
  Widget build(BuildContext context) => BlocProvider(
        create: (context) => EarlyRepaymentWaitingScreenCubit(getIt(), getIt(), getIt(), getIt()),
        child: const EarlyRepaymentWaitingScreen(),
      );
}
