import 'package:early_repayment/domain/entity/otp_code_entity.dart';
import 'package:early_repayment/domain/repository/early_repayment_repository.dart';
import 'package:flutter_atb_common/usecase/usecase.dart';

class RepeatOtpCodeUseCase extends UseCase<OtpCodeEntity, RepeatOtpCodeUseCaseParams> {
  final EarlyRepaymentRepository repository;

  const RepeatOtpCodeUseCase(this.repository);

  @override
  Future<UseCaseResult<OtpCodeEntity>> call(RepeatOtpCodeUseCaseParams params) =>
      innerCall(() async => repository.repeatOtpCode(params.sessionId));
}

class RepeatOtpCodeUseCaseParams {
  const RepeatOtpCodeUseCaseParams({required this.sessionId});

  final String sessionId;
}
