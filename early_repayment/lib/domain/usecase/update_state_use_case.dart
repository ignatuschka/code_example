import 'package:early_repayment/domain/entity/repayment_state.dart';
import 'package:early_repayment/domain/repository/repayment_state_repository.dart';
import 'package:flutter_atb_common/usecase/usecase.dart';

class UpdateStateUseCase extends SyncUseCase<void, RepaymentStatePartial> {
  final RepaymentStateRepository _repository;

  UpdateStateUseCase(this._repository);

  @override
  UseCaseResult<void> call(RepaymentStatePartial params) => innerCall(() => _repository.updateState(params));
}
