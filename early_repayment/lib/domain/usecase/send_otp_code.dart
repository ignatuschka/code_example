import 'package:early_repayment/domain/entity/otp_code_entity.dart';
import 'package:early_repayment/domain/repository/early_repayment_repository.dart';
import 'package:flutter_atb_common/usecase/usecase.dart';

class SendOtpCodeUseCase extends UseCase<OtpCodeEntity, EmptyUsecaseParams> {
  const SendOtpCodeUseCase(this.repository);

  final EarlyRepaymentRepository repository;

  @override
  Future<UseCaseResult<OtpCodeEntity>> call(EmptyUsecaseParams params) =>
      innerCall(() async => repository.sendOtpCode());
}
