import 'package:early_repayment/domain/entity/sign_application_entity.dart';
import 'package:early_repayment/domain/repository/early_repayment_repository.dart';
import 'package:flutter_atb_common/usecase/usecase.dart';

class SignApplicationUseCase extends UseCase<SignApplicationEntity, SignApplicationUseCaseParams> {
  const SignApplicationUseCase(this.repository);

  final EarlyRepaymentRepository repository;

  @override
  Future<UseCaseResult<SignApplicationEntity>> call(SignApplicationUseCaseParams params) =>
      innerCall(() async => repository.signApplication(
            params.documentId,
            params.sessionId,
            params.value,
            params.productId,
            params.type,
            params.withMonthlyPaymentReduction,
            params.amount,
            params.date,
          ));
}

class SignApplicationUseCaseParams {
  const SignApplicationUseCaseParams(
      {required this.documentId,
      required this.sessionId,
      required this.value,
      required this.productId,
      required this.type,
      required this.withMonthlyPaymentReduction,
      required this.amount,
      required this.date});

  final String documentId;

  final String productId;

  final String value;

  final String sessionId;

  final String type;

  final bool withMonthlyPaymentReduction;

  final double amount;

  final String date;
}
