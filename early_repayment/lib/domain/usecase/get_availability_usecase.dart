import 'package:early_repayment/domain/entity/availability_entity.dart';
import 'package:early_repayment/domain/repository/early_repayment_repository.dart';
import 'package:flutter_atb_common/usecase/usecase.dart';

class GetAvailabilityUseCase extends UseCase<AvailabilityEntity, GetAvailabilityUseCaseParams> {
  const GetAvailabilityUseCase(this.repository);

  final EarlyRepaymentRepository repository;

  @override
  Future<UseCaseResult<AvailabilityEntity>> call(GetAvailabilityUseCaseParams params) =>
      innerCall(() async => repository.getAvailability(params.productId));
}

class GetAvailabilityUseCaseParams {
  const GetAvailabilityUseCaseParams({required this.productId});

  final String productId;
}
