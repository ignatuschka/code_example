import 'package:early_repayment/domain/entity/repayment_state.dart';
import 'package:early_repayment/domain/repository/repayment_state_repository.dart';
import 'package:flutter_atb_common/usecase/usecase.dart';

class GetStateUseCase extends SyncUseCase<RepaymentState, EmptyUsecaseParams> {
  final RepaymentStateRepository _repository;

  GetStateUseCase(this._repository);

  @override
  UseCaseResult<RepaymentState> call(EmptyUsecaseParams params) => innerCall(() => _repository.state);
}
