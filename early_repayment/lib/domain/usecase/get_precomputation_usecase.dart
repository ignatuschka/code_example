import 'package:early_repayment/domain/entity/precomputation_entity.dart';
import 'package:early_repayment/domain/enum/early_repayment_statuses.dart';
import 'package:early_repayment/domain/repository/early_repayment_repository.dart';
import 'package:flutter_atb_common/usecase/usecase.dart';
import 'package:intl/intl.dart';

final _format = DateFormat('yyyy-MM-dd');

class GetPrecomputationUseCase extends UseCase<PrecomputationEntity, GetPrecomputationUseCaseParams> {
  const GetPrecomputationUseCase(this.repository);

  final EarlyRepaymentRepository repository;

  @override
  Future<UseCaseResult<PrecomputationEntity>> call(GetPrecomputationUseCaseParams params) =>
      innerCall(() async => repository.getPrecomputation(
            params.productId,
            params.type.name.toUpperCase(),
            params.withMonthlyPaymentReduction,
            params.amount,
            _format.format(params.date),
          ));
}

class GetPrecomputationUseCaseParams {
  const GetPrecomputationUseCaseParams(
      {required this.productId, required this.type, this.withMonthlyPaymentReduction, this.amount, required this.date});

  final String productId;

  final EarlyRepaymentType type;

  final bool? withMonthlyPaymentReduction;

  final double? amount;

  final DateTime date;
}
