import 'package:early_repayment/domain/entity/application_status_entity.dart';
import 'package:early_repayment/domain/repository/early_repayment_repository.dart';
import 'package:flutter_atb_common/usecase/usecase.dart';

class GetStatusCreationUseCase extends UseCase<ApplicationStatusEntity, GetStatusCreationUseCaseParams> {
  final EarlyRepaymentRepository repository;

  const GetStatusCreationUseCase(this.repository);

  @override
  Future<UseCaseResult<ApplicationStatusEntity>> call(GetStatusCreationUseCaseParams params) =>
      innerCall(() async => repository.getStatusCreation(params.correlationId));
}

class GetStatusCreationUseCaseParams {
  const GetStatusCreationUseCaseParams({required this.correlationId});

  final String correlationId;
}
