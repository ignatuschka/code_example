import 'package:early_repayment/domain/entity/precomputation_entity.dart';
import 'package:early_repayment/domain/repository/early_repayment_repository.dart';
import 'package:flutter_atb_common/usecase/usecase.dart';

class GetNewPrecomputationUseCase extends UseCase<PrecomputationEntity, GetPrecomputationUseCaseParams> {
  const GetNewPrecomputationUseCase(this.repository);

  final EarlyRepaymentRepository repository;

  @override
  Future<UseCaseResult<PrecomputationEntity>> call(GetPrecomputationUseCaseParams params) =>
      innerCall(() async => repository.getNewPrecomputation(
            params.productId,
            params.type,
            params.withMonthlyPaymentReduction,
            params.amount,
            params.date,
          ));
}

class GetPrecomputationUseCaseParams {
  const GetPrecomputationUseCaseParams(
      {required this.productId, required this.type, this.withMonthlyPaymentReduction, this.amount, required this.date});

  final String productId;

  final String type;

  final bool? withMonthlyPaymentReduction;

  final double? amount;

  final String date;
}
