import 'package:early_repayment/data/models/create_application_model.dart';
import 'package:early_repayment/domain/entity/create_application_params.dart';
import 'package:early_repayment/domain/repository/early_repayment_repository.dart';
import 'package:flutter_atb_common/usecase/usecase.dart';

class CreateApplicationUseCase extends UseCase<CreateApplicationModel, CreateApplicationUseCaseParams> {
  const CreateApplicationUseCase(this.repository);

  final EarlyRepaymentRepository repository;

  @override
  Future<UseCaseResult<CreateApplicationModel>> call(CreateApplicationUseCaseParams params) => innerCall(() async {
    final result = await repository.createApplication(
      productId: params.appParams.productId,
      otp: params.otp,
      sessionId: params.session,
      type: params.appParams.type,
      withMonthlyPaymentReduction: params.appParams.withMonthlyPaymentReduction,
      amount: params.appParams.amount,
      date: params.appParams.date,
    );
    return result;
  });
}

class CreateApplicationUseCaseParams {
  final String otp;
  final String session;
  final CreateApplicationParams appParams;

  const CreateApplicationUseCaseParams({required this.otp, required this.session, required this.appParams});
}
