import 'package:early_repayment/domain/entity/application_data_entity.dart';
import 'package:early_repayment/domain/repository/early_repayment_repository.dart';
import 'package:flutter_atb_common/usecase/usecase.dart';

class GetApplicationDataUseCase extends UseCase<ApplicationDataEntity, GetApplicationDataUseCaseParams> {
  const GetApplicationDataUseCase(this.repository);

  final EarlyRepaymentRepository repository;

  @override
  Future<UseCaseResult<ApplicationDataEntity>> call(GetApplicationDataUseCaseParams params) =>
      innerCall(() async => repository.getApplicationData(params.productId, params.applicationId));
}

class GetApplicationDataUseCaseParams {
  const GetApplicationDataUseCaseParams({required this.productId, required this.applicationId});

  final String productId;

  final String applicationId;
}
