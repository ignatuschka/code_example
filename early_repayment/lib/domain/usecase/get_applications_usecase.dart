import 'package:early_repayment/domain/entity/applications_entity.dart';
import 'package:early_repayment/domain/repository/early_repayment_repository.dart';
import 'package:flutter_atb_common/usecase/usecase.dart';

class GetApplicationsUseCase extends UseCase<List<ApplicationsEntity>, GetApplicationsUseCaseParams> {
  const GetApplicationsUseCase(this.repository);

  final EarlyRepaymentRepository repository;

  @override
  Future<UseCaseResult<List<ApplicationsEntity>>> call(GetApplicationsUseCaseParams params) => innerCall(() async {
        return repository.getApplications(params.productId);
      });
}

class GetApplicationsUseCaseParams {
  const GetApplicationsUseCaseParams({required this.productId});

  final String productId;
}
