import 'package:early_repayment/domain/entity/otp_verify_entity.dart';
import 'package:early_repayment/domain/repository/early_repayment_repository.dart';
import 'package:flutter_atb_common/usecase/usecase.dart';

class OtpVerifyCodeUseCase extends UseCase<OtpVerifyEntity, OtpVerifyCodeUseCaseParams> {
  final EarlyRepaymentRepository repository;

  const OtpVerifyCodeUseCase(this.repository);

  @override
  Future<UseCaseResult<OtpVerifyEntity>> call(OtpVerifyCodeUseCaseParams params) =>
      innerCall(() async => repository.verifyOtpCode(params.sessionId, params.valueOtp));
}

class OtpVerifyCodeUseCaseParams {
  const OtpVerifyCodeUseCaseParams({required this.sessionId, required this.valueOtp});

  final String sessionId;

  final String valueOtp;
}
