import 'package:early_repayment/domain/entity/cancel_application_entity.dart';
import 'package:early_repayment/domain/repository/early_repayment_repository.dart';
import 'package:flutter_atb_common/usecase/usecase.dart';

class CancelApplicationUseCase extends UseCase<CancelApplicationEntity, CancelApplicationUseCaseParams> {
  const CancelApplicationUseCase(this.repository);

  final EarlyRepaymentRepository repository;

  @override
  Future<UseCaseResult<CancelApplicationEntity>> call(CancelApplicationUseCaseParams params) => innerCall(() async {
        final result = await repository.cancelApplication(params.productId, params.applicationId);
        return result;
      });
}

class CancelApplicationUseCaseParams {
  const CancelApplicationUseCaseParams({required this.productId, required this.applicationId});

  final String productId;

  final String applicationId;
}
