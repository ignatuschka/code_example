import 'package:early_repayment/domain/entity/application_status_entity.dart';
import 'package:early_repayment/domain/repository/early_repayment_repository.dart';
import 'package:flutter_atb_common/usecase/usecase.dart';

class GetStatusCancellationUseCase extends UseCase<ApplicationStatusEntity, GetStatusCancellationUseCaseParams> {
  final EarlyRepaymentRepository repository;

  const GetStatusCancellationUseCase(this.repository);

  @override
  Future<UseCaseResult<ApplicationStatusEntity>> call(GetStatusCancellationUseCaseParams params) =>
      innerCall(() async => repository.getStatusCancellation(params.correlationId));
}

class GetStatusCancellationUseCaseParams {
  const GetStatusCancellationUseCaseParams({required this.correlationId});

  final String correlationId;
}
