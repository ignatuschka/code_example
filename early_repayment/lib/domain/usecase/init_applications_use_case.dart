import 'package:early_repayment/domain/repository/early_repayment_repository.dart';
import 'package:flutter_atb_common/usecase/usecase.dart';

class InitApplicationsUseCase extends UseCase<void, EmptyUsecaseParams> {
  final EarlyRepaymentRepository repository;

  InitApplicationsUseCase(this.repository);

  @override
  Future<UseCaseResult<void>> call(EmptyUsecaseParams params) => innerCall(() async {
        repository.initApplications();
        // По аналитике надо подождать 10 сек)
        await Future.delayed(const Duration(seconds: 10));
      });
}
