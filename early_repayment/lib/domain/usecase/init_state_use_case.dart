import 'package:early_repayment/domain/entity/repayment_state.dart';
import 'package:early_repayment/domain/repository/repayment_state_repository.dart';
import 'package:flutter_atb_common/usecase/usecase.dart';

class InitStateUseCase extends SyncUseCase<void, RepaymentState> {
  final RepaymentStateRepository _repository;

  InitStateUseCase(this._repository);

  @override
  UseCaseResult<void> call(RepaymentState params) => innerCall(() => _repository.initializeState(params));
}
