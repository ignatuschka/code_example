enum ApplicationStatus { notProcessed, canceled, completed, unknown }

enum EarlyRepaymentType { full, part }
