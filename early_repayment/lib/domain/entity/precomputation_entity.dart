class PrecomputationEntity {
  final String typeRecalculation;

  final DateTime? newDate;

  final double? newAmount;

  final DateTime maturityDate;
  final DateTime? oldPlanEndDate;
  final DateTime? loanPaymentDate;

  final double monthlyPaymentAmount;

  final double? amount;

  final double? totalDebt;

  final double? courtDebt;

  final double? loanInterest;

  final double? overdueDept;

  final double accountBalance;

  final double replenishmentAmount;

  PrecomputationEntity(
      {required this.typeRecalculation,
      this.newDate,
      this.loanPaymentDate,
      this.oldPlanEndDate,
      this.newAmount,
      required this.maturityDate,
      required this.monthlyPaymentAmount,
      this.amount,
      this.totalDebt,
      this.courtDebt,
      this.loanInterest,
      this.overdueDept,
      required this.accountBalance,
      required this.replenishmentAmount});
}
