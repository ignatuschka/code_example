import 'package:early_repayment/l10n/early_repayment.localizations.dart';

enum ReductionType {
  duration,
  amount,
}

extension LocalizeType on ReductionType {
  String? localize(EarlyRepaymentLocalizations localizations) => switch (this) {
        ReductionType.amount => localizations.reducingMonthlyPayments,
        ReductionType.duration => localizations.reductionLoanTerm,
      };
}
