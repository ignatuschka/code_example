class CancelApplicationEntity {
  final String correlationId;

  CancelApplicationEntity({required this.correlationId});
}
