class OtpVerifyEntity {
  final String status;
  final DateTime nextTimeFrom;
  final DateTime nextTimeUntil;

  OtpVerifyEntity({required this.status, required this.nextTimeFrom, required this.nextTimeUntil});
}
