class SignApplicationEntity {
  final String statusSign;

  final String status;

  final DateTime nextTimeFrom;

  final DateTime nextTimeUntil;

  final String applicationId;

  final String errorCode;

  final String errorDescription;

  final String type;

  final bool withMonthlyPaymentReduction;

  final double amount;

  final DateTime plannedExecutionDate;

  final double monthlyPaymentAmount;

  final double accountBalance;

  final double replenishmentAmount;

  SignApplicationEntity(
      {required this.statusSign,
      required this.status,
      required this.nextTimeFrom,
      required this.nextTimeUntil,
      required this.applicationId,
      required this.errorCode,
      required this.errorDescription,
      required this.type,
      required this.withMonthlyPaymentReduction,
      required this.amount,
      required this.plannedExecutionDate,
      required this.monthlyPaymentAmount,
      required this.accountBalance,
      required this.replenishmentAmount});
}
