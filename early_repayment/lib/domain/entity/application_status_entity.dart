class ApplicationStatusEntity {
  final String status;

  final String? code;

  final String? description;

  ApplicationStatusEntity({required this.status, required this.code, required this.description});
}
