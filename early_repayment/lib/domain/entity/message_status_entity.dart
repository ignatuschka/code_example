class MessageStatusEntity {
  final String? source;
  final String? messageUUID;
  final String? status;
  final String? address;
  final String? statusRaw;
  final DateTime? registerTime;
  final DateTime? statusTime;
  final String? errorType;
  final String? nativeErrorMessage;

  MessageStatusEntity({
    required this.source,
    required this.messageUUID,
    required this.status,
    required this.address,
    required this.statusRaw,
    required this.registerTime,
    required this.statusTime,
    required this.errorType,
    required this.nativeErrorMessage
  });
}
