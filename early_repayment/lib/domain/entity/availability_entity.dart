class AvailabilityEntity {
  final bool earlyRepaymentEnabled;
  final bool repaymentPageAvailable;
  final bool newRepaymentAvailable;

  AvailabilityEntity(
      {required this.earlyRepaymentEnabled, required this.repaymentPageAvailable, required this.newRepaymentAvailable});
}
