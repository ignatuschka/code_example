import 'package:early_repayment/domain/enum/early_repayment_statuses.dart';

class ApplicationDataEntity {
  final String id;

  final String type;

  final String creditId;

  final String kind;

  final String number;

  final bool withMonthlyPaymentReduction;

  final double amount;

  final DateTime plannedExecutionDate;

  final ApplicationStatus status;

  final double monthlyPaymentAmount;

  final double overdueDept;

  final double accountBalance;

  final double replenishmentAmount;

  final String accountId;

  ApplicationDataEntity(
      {required this.id,
      required this.type,
      required this.creditId,
      required this.kind,
      required this.number,
      required this.withMonthlyPaymentReduction,
      required this.amount,
      required this.plannedExecutionDate,
      required this.status,
      required this.monthlyPaymentAmount,
      required this.overdueDept,
      required this.accountBalance,
      required this.replenishmentAmount,
      required this.accountId});
}
