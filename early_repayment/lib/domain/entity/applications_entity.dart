import 'package:early_repayment/domain/enum/early_repayment_statuses.dart';

class ApplicationsEntity {
  final String id;

  final String type;

  final DateTime plannedExecutionDate;

  final ApplicationStatus status;

  ApplicationsEntity({required this.id, required this.type, required this.plannedExecutionDate, required this.status});
}
