class CreateApplicationParams {
  final String productId;
  final String type;
  final bool withMonthlyPaymentReduction;
  final double amount;
  final String date;

  const CreateApplicationParams({
    required this.productId,
    required this.type,
    required this.withMonthlyPaymentReduction,
    required this.amount,
    required this.date
  });
}