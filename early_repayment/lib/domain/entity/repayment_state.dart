import 'package:early_repayment/domain/entity/reduction_type.dart';
import 'package:early_repayment/domain/enum/early_repayment_statuses.dart';

class RepaymentState {
  final String productId;
  final String currency;
  final String accountNumber;
  final double courtDebt;
  final EarlyRepaymentType? type;
  final double? totalDebt;
  final double? loanInterest;
  final double? accountBalance;
  final double? replenishmentAmount;
  final double? amount;
  final ReductionType? typeRecalculation;
  final DateTime? maturityDate;
  final double? monthlyPaymentAmount;
  final double? overdueDept;
  final DateTime? plannedExecutionDate;
  final DateTime? loanPaymentDate;
  final bool? isCreateResultScreen;
  final String? correlationId;

  RepaymentState(
      {this.totalDebt,
      required this.accountNumber,
      this.loanPaymentDate,
      this.plannedExecutionDate,
      this.loanInterest,
      this.accountBalance,
      this.replenishmentAmount,
      this.type,
      this.amount,
      this.typeRecalculation,
      this.maturityDate,
      this.monthlyPaymentAmount,
      this.overdueDept,
      required this.productId,
      required this.currency,
      required this.courtDebt,
      this.isCreateResultScreen,
      this.correlationId});

  RepaymentState copyWith({
    String? accountNumber,
    String? productId,
    String? currency,
    double? courtDebt,
    EarlyRepaymentType? type,
    double? totalDebt,
    double? loanInterest,
    double? accountBalance,
    double? replenishmentAmount,
    double? amount,
    ReductionType? typeRecalculation,
    DateTime? maturityDate,
    double? monthlyPaymentAmount,
    double? overdueDept,
    DateTime? plannedExecutionDate,
    DateTime? loanPaymentDate,
    bool? isCreateResultScreen,
    String? correlationId,
  }) =>
      RepaymentState(
        accountNumber: accountNumber ?? this.accountNumber,
        amount: amount ?? this.amount,
        type: type ?? this.type,
        productId: productId ?? this.productId,
        currency: currency ?? this.currency,
        courtDebt: courtDebt ?? this.courtDebt,
        totalDebt: totalDebt ?? this.totalDebt,
        loanInterest: loanInterest ?? this.loanInterest,
        accountBalance: accountBalance ?? this.accountBalance,
        replenishmentAmount: replenishmentAmount ?? this.replenishmentAmount,
        typeRecalculation: typeRecalculation ?? this.typeRecalculation,
        maturityDate: maturityDate ?? this.maturityDate,
        monthlyPaymentAmount: monthlyPaymentAmount ?? this.monthlyPaymentAmount,
        overdueDept: overdueDept ?? this.overdueDept,
        plannedExecutionDate: plannedExecutionDate ?? this.plannedExecutionDate,
        loanPaymentDate: loanPaymentDate ?? this.loanPaymentDate,
        isCreateResultScreen: isCreateResultScreen ?? this.isCreateResultScreen,
        correlationId: correlationId ?? this.correlationId,
      );

  RepaymentState merge(RepaymentStatePartial other) => copyWith(
        type: other.type ?? type,
        productId: other.productId ?? productId,
        currency: other.currency ?? currency,
        courtDebt: other.courtDebt ?? courtDebt,
        totalDebt: other.totalDebt ?? totalDebt,
        loanInterest: other.loanInterest ?? loanInterest,
        accountBalance: other.accountBalance ?? accountBalance,
        replenishmentAmount: other.replenishmentAmount ?? replenishmentAmount,
        typeRecalculation: other.typeRecalculation ?? typeRecalculation,
        maturityDate: other.maturityDate ?? maturityDate,
        monthlyPaymentAmount: other.monthlyPaymentAmount ?? monthlyPaymentAmount,
        overdueDept: other.overdueDept ?? overdueDept,
        amount: other.amount ?? amount,
        plannedExecutionDate: other.plannedExecutionDate ?? plannedExecutionDate,
        loanPaymentDate: other.loanPaymentDate ?? loanPaymentDate,
        isCreateResultScreen: other.isCreateResultScreen ?? isCreateResultScreen,
        correlationId: other.correlationId ?? correlationId,
      );
}

class RepaymentStatePartial {
  final String? productId;
  final String? currency;
  final double? courtDebt;
  final EarlyRepaymentType? type;
  final double? totalDebt;
  final double? loanInterest;
  final double? accountBalance;
  final double? replenishmentAmount;
  final double? amount;
  final ReductionType? typeRecalculation;
  final DateTime? newDate;
  final DateTime? maturityDate;
  final double? monthlyPaymentAmount;
  final double? overdueDept;
  final DateTime? plannedExecutionDate;
  final DateTime? loanPaymentDate;
  final bool? isCreateResultScreen;
  final String? correlationId;

  RepaymentStatePartial(
      {this.totalDebt,
      this.courtDebt,
      this.plannedExecutionDate,
      this.typeRecalculation,
      this.newDate,
      this.maturityDate,
      this.monthlyPaymentAmount,
      this.overdueDept,
      this.amount,
      this.loanInterest,
      this.accountBalance,
      this.replenishmentAmount,
      this.productId,
      this.currency,
      this.loanPaymentDate,
      this.type,
      this.isCreateResultScreen,
      this.correlationId});
}
