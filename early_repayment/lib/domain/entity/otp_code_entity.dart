import 'package:early_repayment/domain/entity/message_status_entity.dart';

class OtpCodeEntity {
  final DateTime date;
  final String sessionId;
  final String validUntil;
  final String repeatFrom;
  final MessageStatusEntity messageStatus;

  OtpCodeEntity({
    required this.date,
    required this.sessionId,
    required this.validUntil,
    required this.repeatFrom,
    required this.messageStatus
  });
}
