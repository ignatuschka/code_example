import 'package:early_repayment/data/models/create_application_model.dart';
import 'package:early_repayment/domain/entity/application_data_entity.dart';
import 'package:early_repayment/domain/entity/application_status_entity.dart';
import 'package:early_repayment/domain/entity/applications_entity.dart';
import 'package:early_repayment/domain/entity/availability_entity.dart';
import 'package:early_repayment/domain/entity/cancel_application_entity.dart';
import 'package:early_repayment/domain/entity/otp_code_entity.dart';
import 'package:early_repayment/domain/entity/otp_verify_entity.dart';
import 'package:early_repayment/domain/entity/precomputation_entity.dart';
import 'package:early_repayment/domain/entity/sign_application_entity.dart';

abstract class EarlyRepaymentRepository {
  Future<AvailabilityEntity> getAvailability(String productId);

  Future<List<ApplicationsEntity>> getApplications(String productId);

  Future<ApplicationDataEntity> getApplicationData(String productId, String applicationId);

  Future<PrecomputationEntity> getPrecomputation(
      String productId, String type, bool? withMonthlyPaymentReduction, double? amount, String date);

  Future<PrecomputationEntity> getNewPrecomputation(
      String productId, String type, bool? withMonthlyPaymentReduction, double? amount, String date);

  Future<CreateApplicationModel> createApplication({
    required String productId,
    required String sessionId,
    required String otp,
    required String type,
    required bool withMonthlyPaymentReduction,
    required double amount,
    required String date,
  });

  Future<SignApplicationEntity> signApplication(String documentId, String sessionId, String value, String productId,
      String type, bool withMonthlyPaymentReduction, double amount, String date);

  Future<CancelApplicationEntity> cancelApplication(String productId, String applicationId);

  Future<OtpCodeEntity> sendOtpCode();

  Future<OtpCodeEntity> repeatOtpCode(String sessionId);

  Future<OtpVerifyEntity> verifyOtpCode(String sessionId, String valueOtp);

  Future<void> initApplications();

  Future<ApplicationStatusEntity> getStatusCreation(String correlationId);

  Future<ApplicationStatusEntity> getStatusCancellation(String correlationId);
}
