import 'package:early_repayment/domain/entity/repayment_state.dart';

abstract class RepaymentStateRepository {
  void initializeState([RepaymentState? state]);

  RepaymentState get state;

  void updateState(RepaymentStatePartial state);
}
