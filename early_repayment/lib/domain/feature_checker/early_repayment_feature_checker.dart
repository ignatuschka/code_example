import 'package:feature_checker/domain/entity/checker.dart';
import 'package:feature_checker/domain/use_case/checker_deleagate.dart';

const _toggleKey = "CORE-204-credit-repayment-application";

class EarlyRepaymentChecker implements Checker {
  final CheckerDelegate _delegate;

  EarlyRepaymentChecker(this._delegate);

  @override
  bool isEnable() => _delegate.isEnable(_toggleKey);
}
