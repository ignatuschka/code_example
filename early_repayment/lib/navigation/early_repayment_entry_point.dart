import 'package:early_repayment/navigation/early_repayment_navigator.dart';
import 'package:flutter_atb_common/di/get_it_extensions.dart';

abstract class EarlyRepaymentEntryPoint {
  static final _nav = getIt<EarlyRepaymentNavigator>();
  static Future<void> navigateToEarlyRepaymentScreen(String creditId, double courtDebt, String currency, String accountNumber) => _nav.navigateToEarlyRepaymentScreen(creditId, courtDebt, currency, accountNumber);
}