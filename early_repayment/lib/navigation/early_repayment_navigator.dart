import 'package:flutter_atb_common/di/get_it_extensions.dart';

abstract class EarlyRepaymentNavigator {
  void navigateBack();

  navigateToRoot();

  Future<void> navigateToEarlyRepaymentScreen(String creditId, double courtDebt, String currency, String accountNumber);

  Future<void> navigateToEarlyRepaymentDetailsScreen(String applicationId);

  Future<void> navigateToEarlyRepaymentFirstStepScreen();

  Future<void> navigateToPartRepaymentSecondStepScreen();

  Future<void> navigateToFullRepaymentSecondStepScreen();

  // Future<void> navigateToConfirmationThirdStepScreen(String creditId, String currency, String type,
  //     bool withMonthlyPaymentReduction, double? amount, double? totalDebt, PrecomputationEntity precomputation);
  Future<void> navigateToConfirmationThirdStepScreen();

  Future<void> navigateToResultScreen();

  Future<void> navigateToCancelResultScreen();

  Future<void> navigateToChat();

  Future<void> navigateToErrorScreen(String code, bool isCreatingAppScreenError);

  Future<void> navigateToWaitingScreen();
}

class EarlyRepaymentInternalNavigator {
  static final _nav = getIt<EarlyRepaymentNavigator>();

  static navigateBack() => _nav.navigateBack();

  static Future<void> navigateToEarlyRepaymentScreen(
          String creditId, double courtDebt, String currency, String accountNumber) =>
      _nav.navigateToEarlyRepaymentScreen(creditId, courtDebt, currency, accountNumber);

  navigateToRoot() => _nav.navigateToRoot();

  Future<void> navigateToEarlyRepaymentDetailsScreen(String applicationId) =>
      _nav.navigateToEarlyRepaymentDetailsScreen(applicationId);

  Future<void> navigateToEarlyRepaymentFirstStepScreen() => _nav.navigateToEarlyRepaymentFirstStepScreen();

  Future<void> navigateToResultScreen() => _nav.navigateToResultScreen();

  Future<void> navigateToCancelResultScreen() => _nav.navigateToCancelResultScreen();

  Future<void> navigateToPartRepaymentSecondStepScreen() => _nav.navigateToPartRepaymentSecondStepScreen();

  Future<void> navigateToFullRepaymentSecondStepScreen() => _nav.navigateToFullRepaymentSecondStepScreen();

  Future<void> navigateToConfirmationThirdStepScreen() => _nav.navigateToConfirmationThirdStepScreen();

  Future<void> navigateToChat() => _nav.navigateToChat();

  Future<void> navigateToWaitingScreen() => _nav.navigateToWaitingScreen();
}
