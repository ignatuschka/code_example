import 'package:auto_route/auto_route.dart';
import 'package:auto_route/empty_router_widgets.dart';
import 'package:early_repayment/di/early_repayment_di_module.dart';

class EarlyRepaymentRouter {
  static const root = 'RootEarlyRepayment';
  static const router = AutoRoute(
    path: root,
    name: root,
    page: EmptyRouterPage,
    children: [
      AutoRoute(
        page: EarlyRepayment,
        path: earlyRepayment,
        initial: true,
      ),
      AutoRoute(
        page: EarlyRepaymentDetails,
        path: earlyRepaymentDetails,
      ),
      AutoRoute(
        page: EarlyRepaymentFirstStep,
        path: earlyRepaymentFirstStep,
      ),
      AutoRoute(
        page: PartRepaymentSecondStep,
        path: partRepaymentSecondStep,
      ),
      AutoRoute(
        page: FullRepaymentSecondStep,
        path: fullRepaymentSecondStep,
      ),
      AutoRoute(
        page: ConfirmationThirdStep,
        path: confirmationThirdStep,
      ),
      AutoRoute(
        page: EarlyRepaymentResultPage,
        path: earlyRepaymentResult,
      ),
      AutoRoute(
        page: CancelApplicationResultPage,
        path: cancelApplicationResult,
      ),
      AutoRoute(
        page: EarlyRepaymentErrorScreen,
        path: errorScreen,
      ),
      AutoRoute(
        page: EarlyRepaymentWaiting,
        path: earlyRepaymentWaiting,
      ),
      AutoRoute(
        page: EarlyRepayment,
        path: earlyRepayment,
        initial: true,
      ),
    ],
  );

  static const earlyRepayment = 'earlyRepayment';
  static const earlyRepaymentResult = 'earlyRepaymentResult';
  static const earlyRepaymentDetails = 'earlyRepaymentDetails';
  static const earlyRepaymentFirstStep = 'earlyRepaymentFirstStep';
  static const partRepaymentSecondStep = 'partRepaymentSecondStep';
  static const fullRepaymentSecondStep = 'fullRepaymentSecondStep';
  static const confirmationThirdStep = 'confirmationThirdStep';
  static const cancelApplicationResult = 'cancelApplicationResult';
  static const errorScreen = 'earlyRepaymentErrorScreen';
  static const earlyRepaymentWaiting = 'earlyRepaymentWaiting';
}
