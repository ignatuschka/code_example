import 'package:early_repayment/domain/enum/early_repayment_statuses.dart';

const applicationStatusMap = {
  'NOT_PROCESSED': ApplicationStatus.notProcessed,
  'CANCELED': ApplicationStatus.canceled,
  'COMPLETED': ApplicationStatus.completed,
  'UNKNOWN': ApplicationStatus.unknown
};
