import 'package:early_repayment/data/models/message_status_model.dart';
import 'package:early_repayment/domain/entity/otp_code_entity.dart';
import 'package:early_repayment/extensions/date_time_extensions.dart';
import 'package:flutter_atb_common/entity/command_entity.dart';
import 'package:json_annotation/json_annotation.dart';

part 'otp_code_model.g.dart';

@JsonSerializable(createToJson: false)
class OtpCodeModel with CommandEntity {
  final String date;
  final String sessionId;
  final String validUntil;
  final String repeatFrom;
  final MessageStatusModel messageStatus;

  OtpCodeModel(
      {required this.date,
      required this.sessionId,
      required this.validUntil,
      required this.repeatFrom,
      required this.messageStatus});

  factory OtpCodeModel.fromJson(Map<String, dynamic> json) => _$OtpCodeModelFromJson(json);

  OtpCodeEntity toEntity() {
    return OtpCodeEntity(
      date: date.isoToDateTime(),
      sessionId: sessionId,
      validUntil: validUntil,
      repeatFrom: repeatFrom,
      messageStatus: messageStatus.toEntity(),
    );
  }
}
