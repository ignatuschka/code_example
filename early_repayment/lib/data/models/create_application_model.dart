import 'package:early_repayment/data/models/error_model.dart';
import 'package:early_repayment/data/models/repayment_data_dto.dart';
import 'package:json_annotation/json_annotation.dart';

import 'package:early_repayment/data/models/otp_result_dto.dart';

part 'create_application_model.g.dart';

@JsonSerializable(createToJson: false)
class CreateApplicationModel {
  @JsonKey(name: "OTPResult")
  final OTPResultDto? otpResult;
  final String correlationId;
  final RepaymentDataDto? repaymentData;

  // TODO: временное решение, удалить как новый checkNetErrors будет готов
  final ErrorModel? error;

  CreateApplicationModel(this.error,
      {required this.repaymentData, required this.correlationId, required this.otpResult});

  factory CreateApplicationModel.fromJson(Map<String, dynamic> json) => _$CreateApplicationModelFromJson(json);
}
