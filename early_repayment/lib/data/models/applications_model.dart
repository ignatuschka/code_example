import 'package:early_repayment/data/mapper/application_status_map.dart';
import 'package:early_repayment/domain/entity/applications_entity.dart';
import 'package:json_annotation/json_annotation.dart';

part 'applications_model.g.dart';

@JsonSerializable(createToJson: false)
class ApplicationsModel {
  final String id;

  final String type;

  final DateTime plannedExecutionDate;

  final String status;

  ApplicationsModel({required this.id, required this.type, required this.plannedExecutionDate, required this.status});

  factory ApplicationsModel.fromJson(Map<String, dynamic> json) => _$ApplicationsModelFromJson(json);

  ApplicationsEntity toEntity() {
    return ApplicationsEntity(
        id: id, type: type, plannedExecutionDate: plannedExecutionDate, status: applicationStatusMap[status]!);
  }
}
