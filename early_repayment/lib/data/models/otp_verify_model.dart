import 'package:early_repayment/domain/entity/otp_verify_entity.dart';
import 'package:early_repayment/extensions/date_time_extensions.dart';
import 'package:flutter_atb_common/entity/command_entity.dart';
import 'package:json_annotation/json_annotation.dart';

part 'otp_verify_model.g.dart';

@JsonSerializable(createToJson: false)
class OtpVerifyModel with CommandEntity {
  final String status;
  final String nextTimeFrom;
  final String nextTimeUntil;

  OtpVerifyModel({required this.status, required this.nextTimeFrom, required this.nextTimeUntil});

  factory OtpVerifyModel.fromJson(Map<String, dynamic> json) => _$OtpVerifyModelFromJson(json);

  OtpVerifyEntity toEntity() {
    return OtpVerifyEntity(
      status: status,
      nextTimeFrom: nextTimeFrom.isoToDateTime(),
      nextTimeUntil: nextTimeUntil.isoToDateTime(),
    );
  }
}
