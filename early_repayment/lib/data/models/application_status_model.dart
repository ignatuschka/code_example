import 'package:early_repayment/domain/entity/application_status_entity.dart';
import 'package:json_annotation/json_annotation.dart';

part 'application_status_model.g.dart';

@JsonSerializable(createToJson: false)
class ApplicationStatusModel {
  final String status;

  final String? code;

  final String? description;

  ApplicationStatusModel({required this.status, required this.code, required this.description});

  factory ApplicationStatusModel.fromJson(Map<String, dynamic> json) => _$ApplicationStatusModelFromJson(json);

  ApplicationStatusEntity toEntity() {
    return ApplicationStatusEntity(status: status, code: code, description: description);
  }
}
