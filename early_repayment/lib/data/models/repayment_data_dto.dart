import 'package:json_annotation/json_annotation.dart';

part 'repayment_data_dto.g.dart';

@JsonSerializable()
class RepaymentDataDto {
  final String? type;
  final bool? withMonthlyPaymentReduction;
  final String? plannedExecutionDate;
  final double? monthlyPaymentAmount;
  final double? courtDept;
  final double? loanInterest;
  final double? overdueDept;
  final double? accountBalance;
  final double? replenishmentAmount;
  final double? amount;

  RepaymentDataDto({
    this.type,
    this.amount,
    this.withMonthlyPaymentReduction,
    this.plannedExecutionDate,
    this.monthlyPaymentAmount,
    this.courtDept,
    this.loanInterest,
    this.overdueDept,
    this.accountBalance,
    this.replenishmentAmount,
  });

  factory RepaymentDataDto.fromJson(Map<String, dynamic> json) => _$RepaymentDataDtoFromJson(json);
}
