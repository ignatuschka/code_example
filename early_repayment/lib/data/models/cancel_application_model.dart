import 'package:early_repayment/domain/entity/cancel_application_entity.dart';
import 'package:json_annotation/json_annotation.dart';

part 'cancel_application_model.g.dart';

@JsonSerializable(createToJson: false)
class CancelApplicationModel {
  final String correlationId;

  CancelApplicationModel({required this.correlationId});

  factory CancelApplicationModel.fromJson(Map<String, dynamic> json) => _$CancelApplicationModelFromJson(json);

  CancelApplicationEntity toEntity() {
    return CancelApplicationEntity(correlationId: correlationId);
  }
}
