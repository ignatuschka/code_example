import 'package:early_repayment/data/models/otp_verify_model.dart';
import 'package:early_repayment/data/models/result_model.dart';
import 'package:flutter_atb_common/entity/command_entity.dart';
import 'package:json_annotation/json_annotation.dart';

part 'otp_verify_response.g.dart';

@JsonSerializable(createToJson: false)
class OtpVerifyResponse with CommandEntity {
  final ResultModel? result;
  final OtpVerifyModel data;

  OtpVerifyResponse({this.result, required this.data});

  factory OtpVerifyResponse.fromJson(Map<String, dynamic> json) => _$OtpVerifyResponseFromJson(json);
}
