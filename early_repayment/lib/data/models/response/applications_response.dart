import 'package:early_repayment/data/models/applications_model.dart';
import 'package:early_repayment/data/models/result_model.dart';
import 'package:flutter_atb_common/entity/command_entity.dart';
import 'package:json_annotation/json_annotation.dart';

part 'applications_response.g.dart';

@JsonSerializable(createToJson: false)
class ApplicationsResponse with CommandEntity {
  final ResultModel? result;

  final List<ApplicationsModel> data;

  ApplicationsResponse({this.result, required this.data});

  factory ApplicationsResponse.fromJson(Map<String, dynamic> json) => _$ApplicationsResponseFromJson(json);
}
