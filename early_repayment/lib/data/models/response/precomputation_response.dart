import 'package:early_repayment/data/models/precomputation_model.dart';
import 'package:early_repayment/data/models/result_model.dart';
import 'package:flutter_atb_common/entity/command_entity.dart';
import 'package:json_annotation/json_annotation.dart';

part 'precomputation_response.g.dart';

@JsonSerializable(createToJson: false)
class PrecomputationResponse with CommandEntity {
  final ResultModel? result;

  final PrecomputationModel data;

  PrecomputationResponse({this.result, required this.data});

  factory PrecomputationResponse.fromJson(Map<String, dynamic> json) => _$PrecomputationResponseFromJson(json);
}
