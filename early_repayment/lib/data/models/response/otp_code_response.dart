import 'package:early_repayment/data/models/otp_code_model.dart';
import 'package:early_repayment/data/models/result_model.dart';
import 'package:flutter_atb_common/entity/command_entity.dart';
import 'package:json_annotation/json_annotation.dart';

part 'otp_code_response.g.dart';

@JsonSerializable(createToJson: false)
class OtpCodeResponse with CommandEntity {
  final ResultModel? result;

  final OtpCodeModel data;

  OtpCodeResponse({this.result, required this.data});

  factory OtpCodeResponse.fromJson(Map<String, dynamic> json) {
    final model = _$OtpCodeResponseFromJson(json);
    return model;
  }
}
