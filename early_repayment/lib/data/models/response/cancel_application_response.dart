import 'package:early_repayment/data/models/result_model.dart';
import 'package:early_repayment/data/models/cancel_application_model.dart';
import 'package:flutter_atb_common/entity/command_entity.dart';
import 'package:json_annotation/json_annotation.dart';

part 'cancel_application_response.g.dart';

@JsonSerializable(createToJson: false)
class CancelApplicationResponse with CommandEntity {
  final ResultModel? result;

  final CancelApplicationModel data;

  CancelApplicationResponse({this.result, required this.data});

  factory CancelApplicationResponse.fromJson(Map<String, dynamic> json) => _$CancelApplicationResponseFromJson(json);
}
