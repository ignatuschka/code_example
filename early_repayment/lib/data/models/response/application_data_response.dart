import 'package:early_repayment/data/models/application_data_model.dart';
import 'package:early_repayment/data/models/result_model.dart';
import 'package:flutter_atb_common/entity/command_entity.dart';
import 'package:json_annotation/json_annotation.dart';

part 'application_data_response.g.dart';

@JsonSerializable(createToJson: false)
class ApplicationDataResponse with CommandEntity {
  final ResultModel? result;

  final ApplicationDataModel data;

  ApplicationDataResponse({this.result, required this.data});

  factory ApplicationDataResponse.fromJson(Map<String, dynamic> json) => _$ApplicationDataResponseFromJson(json);
}
