import 'package:early_repayment/data/models/result_model.dart';
import 'package:early_repayment/data/models/sign_application_model.dart';
import 'package:flutter_atb_common/entity/command_entity.dart';
import 'package:json_annotation/json_annotation.dart';

part 'sign_application_response.g.dart';

@JsonSerializable(createToJson: false)
class SignApplicationResponse with CommandEntity {
  final ResultModel? result;

  final SignApplicationModel data;

  SignApplicationResponse({this.result, required this.data});

  factory SignApplicationResponse.fromJson(Map<String, dynamic> json) => _$SignApplicationResponseFromJson(json);
}
