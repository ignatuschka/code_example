import 'package:early_repayment/data/models/result_model.dart';
import 'package:early_repayment/data/models/create_application_model.dart';
import 'package:flutter_atb_common/entity/command_entity.dart';
import 'package:json_annotation/json_annotation.dart';

part 'create_application_response.g.dart';

@JsonSerializable(createToJson: false)
class CreateApplicationResponse with CommandEntity {
  final ResultModel? result;
  final CreateApplicationModel data;

  CreateApplicationResponse({this.result, required this.data});

  factory CreateApplicationResponse.fromJson(Map<String, dynamic> json) => _$CreateApplicationResponseFromJson(json);
}
