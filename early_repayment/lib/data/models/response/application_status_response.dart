import 'package:early_repayment/data/models/application_status_model.dart';
import 'package:early_repayment/data/models/result_model.dart';
import 'package:flutter_atb_common/entity/command_entity.dart';
import 'package:json_annotation/json_annotation.dart';

part 'application_status_response.g.dart';

@JsonSerializable(createToJson: false)
class ApplicationStatusResponse with CommandEntity {
  final ResultModel? result;
  final ApplicationStatusModel data;

  ApplicationStatusResponse({this.result, required this.data});

  factory ApplicationStatusResponse.fromJson(Map<String, dynamic> json) => _$ApplicationStatusResponseFromJson(json);
}
