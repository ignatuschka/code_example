import 'package:early_repayment/data/models/availability_model.dart';
import 'package:early_repayment/data/models/result_model.dart';
import 'package:flutter_atb_common/entity/command_entity.dart';
import 'package:json_annotation/json_annotation.dart';

part 'availability_response.g.dart';

@JsonSerializable(createToJson: false)
class AvailabilityResponse with CommandEntity {
  final ResultModel? result;

  final AvailabilityModel data;

  AvailabilityResponse({this.result, required this.data});

  factory AvailabilityResponse.fromJson(Map<String, dynamic> json) => _$AvailabilityResponseFromJson(json);
}
