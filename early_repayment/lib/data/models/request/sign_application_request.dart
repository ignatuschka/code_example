import 'package:json_annotation/json_annotation.dart';

part 'sign_application_request.g.dart';

@JsonSerializable(includeIfNull: false)
class SignApplicationRequest {
  const SignApplicationRequest(
      {required this.documentId,
      required this.sessionId,
      required this.value,
      required this.productId,
      required this.type,
      required this.withMonthlyPaymentReduction,
      required this.amount,
      required this.date});

  final String documentId;

  final String productId;

  final String value;

  final String sessionId;

  final String type;

  final bool withMonthlyPaymentReduction;

  final double amount;

  final String date;

  Map<String, dynamic> toJson() => _$SignApplicationRequestToJson(this);
}
