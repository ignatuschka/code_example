import 'package:json_annotation/json_annotation.dart';

part 'cancel_application_request.g.dart';

@JsonSerializable(includeIfNull: false)
class CancelApplicationRequest {
  const CancelApplicationRequest({required this.productId, required this.applicationId});

  final String productId;

  final String applicationId;

  Map<String, dynamic> toJson() => _$CancelApplicationRequestToJson(this);
}
