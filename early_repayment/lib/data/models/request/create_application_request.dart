import 'package:json_annotation/json_annotation.dart';

part 'create_application_request.g.dart';

@JsonSerializable(includeIfNull: false)
class CreateApplicationRequest {
  const CreateApplicationRequest(
      {required this.productId,
      required this.value,
      required this.type,
      required this.sessionId,
      required this.withMonthlyPaymentReduction,
      required this.amount,
      required this.date});

  final String productId;

  final String value;

  final String sessionId;

  final String type;

  final bool withMonthlyPaymentReduction;

  final double amount;

  final String date;

  Map<String, dynamic> toJson() => _$CreateApplicationRequestToJson(this);
}
