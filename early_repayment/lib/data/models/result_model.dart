import 'package:early_repayment/extensions/date_time_extensions.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:early_repayment/domain/entity/result_entity.dart';

part 'result_model.g.dart';

@JsonSerializable(createToJson: false)
class ResultModel {
  final String? timestamp;

  final int? status;

  final String? service;

  final String? code;

  final String? message;

  const ResultModel({this.timestamp, this.status, this.service, this.code, this.message});

  factory ResultModel.fromJson(Map<String, dynamic> json) => _$ResultModelFromJson(json);

  ResultEntity toEntity() {
    return ResultEntity(
      timestamp: timestamp!.isoToDateTime(),
      status: status,
      service: service,
      code: code,
      message: message,
    );
  }
}
