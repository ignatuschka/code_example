import 'package:early_repayment/domain/entity/availability_entity.dart';
import 'package:json_annotation/json_annotation.dart';

part 'availability_model.g.dart';

@JsonSerializable(createToJson: false)
class AvailabilityModel {
  final bool earlyRepaymentEnabled;
  final bool repaymentPageAvailable;
  final bool newRepaymentAvailable;

  AvailabilityModel(
      {required this.earlyRepaymentEnabled, required this.repaymentPageAvailable, required this.newRepaymentAvailable});

  factory AvailabilityModel.fromJson(Map<String, dynamic> json) => _$AvailabilityModelFromJson(json);

  AvailabilityEntity toEntity() {
    return AvailabilityEntity(
        earlyRepaymentEnabled: earlyRepaymentEnabled,
        repaymentPageAvailable: repaymentPageAvailable,
        newRepaymentAvailable: newRepaymentAvailable);
  }
}
