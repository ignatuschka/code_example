import 'package:early_repayment/domain/entity/sign_application_entity.dart';
import 'package:early_repayment/extensions/date_time_extensions.dart';
import 'package:json_annotation/json_annotation.dart';

part 'sign_application_model.g.dart';

@JsonSerializable(createToJson: false)
class SignApplicationModel {
  final String statusSign;

  final String status;

  final String nextTimeFrom;

  final String nextTimeUntil;

  final String applicationId;

  final String errorCode;

  final String errorDescription;

  final String type;

  final bool withMonthlyPaymentReduction;

  final double amount;

  final String plannedExecutionDate;

  final double monthlyPaymentAmount;

  final double accountBalance;

  final double replenishmentAmount;

  SignApplicationModel(
      {required this.statusSign,
      required this.status,
      required this.nextTimeFrom,
      required this.nextTimeUntil,
      required this.applicationId,
      required this.errorCode,
      required this.errorDescription,
      required this.type,
      required this.withMonthlyPaymentReduction,
      required this.amount,
      required this.plannedExecutionDate,
      required this.monthlyPaymentAmount,
      required this.accountBalance,
      required this.replenishmentAmount});

  factory SignApplicationModel.fromJson(Map<String, dynamic> json) => _$SignApplicationModelFromJson(json);

  SignApplicationEntity toEntity() {
    return SignApplicationEntity(
        statusSign: statusSign,
        status: status,
        nextTimeFrom: nextTimeFrom.isoToDateTime(),
        nextTimeUntil: nextTimeUntil.isoToDateTime(),
        applicationId: applicationId,
        errorCode: errorCode,
        errorDescription: errorDescription,
        type: type,
        withMonthlyPaymentReduction: withMonthlyPaymentReduction,
        amount: amount,
        plannedExecutionDate: plannedExecutionDate.isoToDateTime(),
        monthlyPaymentAmount: monthlyPaymentAmount,
        accountBalance: accountBalance,
        replenishmentAmount: replenishmentAmount);
  }
}
