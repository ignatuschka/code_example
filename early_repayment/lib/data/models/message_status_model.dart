import 'package:early_repayment/domain/entity/message_status_entity.dart';
import 'package:early_repayment/extensions/date_time_extensions.dart';
import 'package:json_annotation/json_annotation.dart';

part 'message_status_model.g.dart';

@JsonSerializable(createToJson: false)
class MessageStatusModel {
  final String? source;
  final String? messageUUID;
  final String? status;
  final String? address;
  final String? statusRaw;
  final String? registerTime;
  final String? statusTime;
  final String? errorType;
  final String? nativeErrorMessage;

  MessageStatusModel({
    required this.source,
    required this.messageUUID,
    required this.status,
    required this.address,
    required this.statusRaw,
    required this.registerTime,
    required this.statusTime,
    required this.errorType,
    required this.nativeErrorMessage
  });

  factory MessageStatusModel.fromJson(Map<String, dynamic> json) => _$MessageStatusModelFromJson(json);

  MessageStatusEntity toEntity() {
    return MessageStatusEntity(
        source: source,
        messageUUID: messageUUID,
        status: status,
        address: address,
        statusRaw: statusRaw,
        registerTime: registerTime?.isoToDateTime(),
        statusTime: statusTime?.isoToDateTime(),
        errorType: errorType,
        nativeErrorMessage: nativeErrorMessage);
  }
}
