import 'package:early_repayment/domain/entity/precomputation_entity.dart';
import 'package:early_repayment/extensions/date_time_extensions.dart';
import 'package:json_annotation/json_annotation.dart';

part 'precomputation_model.g.dart';

@JsonSerializable(createToJson: false)
class PrecomputationModel {
  final String typeRecalculation;

  final String? newDate;
  final String? oldPlanEndDate;
  final String? loanPaymentDate;

  final double? newAmount;

  final String maturityDate;

  final double monthlyPaymentAmount;

  final double? amount;

  final double? totalDebt;

  final double? courtDept;

  final double? loanInterest;

  final double? overdueDept;

  final double accountBalance;

  final double replenishmentAmount;

  PrecomputationModel(
      {required this.typeRecalculation,
      this.newDate,
      this.loanPaymentDate,
      this.newAmount,
      this.oldPlanEndDate,
      required this.maturityDate,
      required this.monthlyPaymentAmount,
      this.amount,
      this.totalDebt,
      this.courtDept,
      this.loanInterest,
      this.overdueDept,
      required this.accountBalance,
      required this.replenishmentAmount});

  factory PrecomputationModel.fromJson(Map<String, dynamic> json) => _$PrecomputationModelFromJson(json);

  PrecomputationEntity toEntity() {
    return PrecomputationEntity(
      typeRecalculation: typeRecalculation,
      newDate: newDate?.isoToDateTime(),
      newAmount: newAmount,
      maturityDate: maturityDate.isoToDateTime(),
      monthlyPaymentAmount: monthlyPaymentAmount,
      amount: amount,
      totalDebt: totalDebt,
      courtDebt: courtDept,
      loanInterest: loanInterest,
      overdueDept: overdueDept,
      accountBalance: accountBalance,
      replenishmentAmount: replenishmentAmount,
      oldPlanEndDate: oldPlanEndDate?.isoToDateTimeWithDate(),
      loanPaymentDate: loanPaymentDate?.isoToDateTimeWithDate(),
    );
  }
}
