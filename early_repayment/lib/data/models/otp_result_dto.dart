
import 'package:json_annotation/json_annotation.dart';

part 'otp_result_dto.g.dart';

@JsonSerializable()
class OTPResultDto {
  final String? sessionId;
  final String? status;
  final String? nextTimeFrom;
  final String? nextTimeUntil;

  OTPResultDto({
    this.sessionId,
    this.status,
    this.nextTimeFrom,
    this.nextTimeUntil,
  });

  factory OTPResultDto.fromJson(Map<String, dynamic> json) => _$OTPResultDtoFromJson(json);
}
