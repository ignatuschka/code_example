import 'package:early_repayment/data/mapper/application_status_map.dart';
import 'package:early_repayment/domain/entity/application_data_entity.dart';
import 'package:early_repayment/extensions/date_time_extensions.dart';
import 'package:json_annotation/json_annotation.dart';

part 'application_data_model.g.dart';

@JsonSerializable(createToJson: false)
class ApplicationDataModel {
  final String id;

  final String type;

  final String creditId;

  final String kind;

  final String number;

  final bool withMonthlyPaymentReduction;

  final double amount;

  final String plannedExecutionDate;

  final String status;

  final double monthlyPaymentAmount;

  final double overdueDept;

  final double accountBalance;

  final double replenishmentAmount;

  final String accountId;

  ApplicationDataModel(
      {required this.id,
      required this.type,
      required this.creditId,
      required this.kind,
      required this.number,
      required this.withMonthlyPaymentReduction,
      required this.amount,
      required this.plannedExecutionDate,
      required this.status,
      required this.monthlyPaymentAmount,
      required this.overdueDept,
      required this.accountBalance,
      required this.replenishmentAmount,
      required this.accountId});

  factory ApplicationDataModel.fromJson(Map<String, dynamic> json) => _$ApplicationDataModelFromJson(json);

  ApplicationDataEntity toEntity() {
    return ApplicationDataEntity(
        id: id,
        type: type,
        creditId: creditId,
        kind: kind,
        number: number,
        withMonthlyPaymentReduction: withMonthlyPaymentReduction,
        amount: amount,
        plannedExecutionDate: plannedExecutionDate.isoToDateTimeWithDate(),
        status: applicationStatusMap[status]!,
        monthlyPaymentAmount: monthlyPaymentAmount,
        overdueDept: overdueDept,
        accountBalance: accountBalance,
        replenishmentAmount: replenishmentAmount,
        accountId: accountId);
  }
}
