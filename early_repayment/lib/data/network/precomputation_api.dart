import 'package:api/api_client/api_client.dart';
import 'package:dio/dio.dart';
import 'package:early_repayment/data/models/response/precomputation_response.dart';

abstract class PrecomputationApi {
  factory PrecomputationApi(ApiClient client) => PrecomputationApiImpl(client.dio);

  Future<PrecomputationResponse> fetchNewPrecomputation(
      String productId,String type,bool? withMonthlyPaymentReduction,double? amount,String date);
}

class PrecomputationApiImpl implements PrecomputationApi {
  late final Dio _dio;
  String? baseUrl;
  CancelToken? cancelToken;

  PrecomputationApiImpl(
      this._dio, {
        this.baseUrl,
      });

  @override
  Future<PrecomputationResponse> fetchNewPrecomputation(
      productId,
      type,
      withMonthlyPaymentReduction,
      amount,
      date,
      ) async {
    cancelToken?.cancel();
    cancelToken = CancelToken();
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{
      r'type': type,
      r'withMonthlyPaymentReduction': withMonthlyPaymentReduction,
      r'amount': amount,
      r'date': date,
    };
    queryParameters.removeWhere((k, v) => v == null);
    final _headers = <String, dynamic>{r'productId': productId};
    _headers.removeWhere((k, v) => v == null);
    final _data = <String, dynamic>{};
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<PrecomputationResponse>(Options(
          method: 'GET',
          headers: _headers,
          extra: _extra,
        )
            .compose(
          _dio.options,
          '/atb-gateway/mobile/api/msfl/v1/repayment/pre-computation',
          queryParameters: queryParameters,
          data: _data,
          cancelToken: cancelToken,
        )
            .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = PrecomputationResponse.fromJson(_result.data!);
    cancelToken = null;
    return value;
  }

  RequestOptions _setStreamType<T>(RequestOptions requestOptions) {
    if (T != dynamic &&
        !(requestOptions.responseType == ResponseType.bytes || requestOptions.responseType == ResponseType.stream)) {
      if (T == String) {
        requestOptions.responseType = ResponseType.plain;
      } else {
        requestOptions.responseType = ResponseType.json;
      }
    }
    return requestOptions;
  }
}