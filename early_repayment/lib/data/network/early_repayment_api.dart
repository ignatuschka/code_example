import 'package:dio/dio.dart' hide Headers;
import 'package:api/api_client/api_client.dart';
import 'package:early_repayment/data/models/request/cancel_application_request.dart';
import 'package:early_repayment/data/models/request/create_application_request.dart';
import 'package:early_repayment/data/models/request/sign_application_request.dart';
import 'package:early_repayment/data/models/response/application_data_response.dart';
import 'package:early_repayment/data/models/response/application_status_response.dart';
import 'package:early_repayment/data/models/response/applications_response.dart';
import 'package:early_repayment/data/models/response/availability_response.dart';
import 'package:early_repayment/data/models/response/cancel_application_response.dart';
import 'package:early_repayment/data/models/response/create_application_response.dart';
import 'package:early_repayment/data/models/response/otp_code_response.dart';
import 'package:early_repayment/data/models/response/otp_verify_response.dart';
import 'package:early_repayment/data/models/response/precomputation_response.dart';
import 'package:early_repayment/data/models/response/sign_application_response.dart';
import 'package:retrofit/retrofit.dart';

part 'early_repayment_api.g.dart';

@RestApi()
abstract class EarlyRepaymentApi {
  factory EarlyRepaymentApi(ApiClient client) => _EarlyRepaymentApi(client.dio);

  @GET('/atb-gateway/mobile/api/msfl/v1/repayment/availability')
  Future<AvailabilityResponse> fetchAvailability(@Header('productId') String productId);

  @GET('/atb-gateway/mobile/api/msfl/v1/repayment/applications')
  Future<ApplicationsResponse> fetchApplications(@Header('productId') String productId);

  @GET('/atb-gateway/mobile/api/msfl/v1/repayment/application-data')
  Future<ApplicationDataResponse> fetchApplicationData(
      @Header('productId') String productId, @Header('applicationId') String applicationId);

  @GET('/atb-gateway/mobile/api/msfl/v1/repayment/pre-computation')
  Future<PrecomputationResponse> fetchPrecomputation(
      @Header('productId') String productId,
      @Query('type') String type,
      @Query('withMonthlyPaymentReduction') bool? withMonthlyPaymentReduction,
      @Query('amount') double? amount,
      @Query('date') String date);

  @POST('/atb-gateway/mobile/api/msfl/v1/repayment/repayment-application')
  Future<CreateApplicationResponse> createApplication(
    @Body() CreateApplicationRequest body,
  );

  @POST('/atb-gateway/mobile/api/msfl/v1/repayment/signRepaymentApplication')
  Future<SignApplicationResponse> signApplication(
    @Body() SignApplicationRequest body,
  );

  @POST('/atb-gateway/mobile/api/msfl/v1/repayment/cancel-repayment-application')
  Future<CancelApplicationResponse> cancelApplication(
    @Body() CancelApplicationRequest body,
  );

  @POST('/atb-gateway/mobile/api/msfl/v1/otp/send')
  Future<OtpCodeResponse> sendOtpCode();

  @POST('/atb-gateway/mobile/api/msfl/v1/otp/repeat')
  Future<OtpCodeResponse> repeatOtpCode(@Header('sessionId') String sessionId);

  @POST('/atb-gateway/mobile/api/msfl/v1/otp/verify')
  Future<OtpVerifyResponse> verifyOtpCode(@Header('sessionId') String sessionId, @Query('value') String valueOtp);

  @GET('/atb-gateway/mobile/api/msfl/v1/repayment/init-applications')
  Future<void> initApplications();

  @GET('/atb-gateway/mobile/api/msfl/v1/repayment/status-creation')
  Future<ApplicationStatusResponse> fetchStatusCreation(@Header('correlation-id') String correlationId);

  @GET('/atb-gateway/mobile/api/msfl/v1/repayment/status-cancellation')
  Future<ApplicationStatusResponse> fetchStatusCancellation(@Header('correlation-id') String correlationId);
}
