import 'package:early_repayment/data/models/request/cancel_application_request.dart';
import 'package:early_repayment/data/models/request/create_application_request.dart';
import 'package:early_repayment/data/models/request/sign_application_request.dart';
import 'package:early_repayment/data/models/response/application_data_response.dart';
import 'package:early_repayment/data/models/response/application_status_response.dart';
import 'package:early_repayment/data/models/response/applications_response.dart';
import 'package:early_repayment/data/models/response/availability_response.dart';
import 'package:early_repayment/data/models/response/cancel_application_response.dart';
import 'package:early_repayment/data/models/response/create_application_response.dart';
import 'package:early_repayment/data/models/response/otp_code_response.dart';
import 'package:early_repayment/data/models/response/otp_verify_response.dart';
import 'package:early_repayment/data/models/response/precomputation_response.dart';
import 'package:early_repayment/data/models/response/sign_application_response.dart';
import 'package:api/exceptions.dart';
import 'package:early_repayment/data/network/early_repayment_api.dart';
import 'package:early_repayment/data/network/precomputation_api.dart';
import 'package:early_repayment/presentation/pages/early_repayment_waiting/cubit/early_repayment_waiting_screen_cubit.dart';
import 'package:flutter_atb_common/orchestrator/request_pull_orchestrator.dart';
import 'package:remote_config/domain/repository/remote_config.dart';

abstract class EarlyRepaymentRemoteDataSource {
  Future<AvailabilityResponse> getAvailability(String productId);

  Future<ApplicationsResponse> getApplications(String productId);

  Future<ApplicationDataResponse> getApplicationData(String productId, String applicationId);

  Future<PrecomputationResponse> getPrecomputation(
      String productId, String type, bool? withMonthlyPaymentReduction, double? amount, String date);

  Future<PrecomputationResponse> getNewPrecomputation(
      String productId, String type, bool? withMonthlyPaymentReduction, double? amount, String date);

  Future<CreateApplicationResponse> postCreateApplication({
    required String productId,
    required String value,
    required String sessionId,
    required String type,
    required bool withMonthlyPaymentReduction,
    required double amount,
    required String date,
  });

  Future<SignApplicationResponse> postSignApplication(
    String documentId,
    String productId,
    String value,
    String sessionId,
    String type,
    bool withMonthlyPaymentReduction,
    double amount,
    String date,
  );

  Future<CancelApplicationResponse> postCancelApplication(String productId, String applicationId);

  Future<OtpCodeResponse> sendOtpCode();

  Future<OtpCodeResponse> repeatOtpCode(String sessionId);

  Future<OtpVerifyResponse> verifyOtpCode(String sessionId, String valueOtp);

  Future<void> initApplications();

  Future<ApplicationStatusResponse> getStatusCreation(String correlationId);

  Future<ApplicationStatusResponse> getStatusCancellation(String correlationId);
}

class EarlyRepaymentRemoteDataSourceImpl implements EarlyRepaymentRemoteDataSource {
  const EarlyRepaymentRemoteDataSourceImpl(
      {required this.api,
      required this.precomputationApi,
      required this.requestPullOrchestrator,
      required this.remoteConfig});

  final RequestPullOrchestrator requestPullOrchestrator;

  final defaultTimeoutDuration = 60;

  final RemoteConfig remoteConfig;

  final EarlyRepaymentApi api;

  final PrecomputationApi precomputationApi;

  @override
  Future<AvailabilityResponse> getAvailability(String productId) {
    return checkNetErrors(() async {
      final model = await api.fetchAvailability(productId);
      return model;
    });
  }

  @override
  Future<ApplicationsResponse> getApplications(String productId) {
    return checkNetErrors(() async {
      final model = await api.fetchApplications(productId);
      return model;
    });
  }

  @override
  Future<ApplicationDataResponse> getApplicationData(String productId, String applicationId) {
    return checkNetErrors(() async {
      final model = await api.fetchApplicationData(productId, applicationId);
      return model;
    });
  }

  @override
  Future<PrecomputationResponse> getPrecomputation(
      String productId, String type, bool? withMonthlyPaymentReduction, double? amount, String date) {
    return checkNetErrors(() async {
      final model = await api.fetchPrecomputation(productId, type, withMonthlyPaymentReduction, amount, date);
      return model;
    });
  }

  @override
  Future<PrecomputationResponse> getNewPrecomputation(
      String productId, String type, bool? withMonthlyPaymentReduction, double? amount, String date) {
    return checkNetErrors(() async {
      final model =
          await precomputationApi.fetchNewPrecomputation(productId, type, withMonthlyPaymentReduction, amount, date);
      return model;
    });
  }

  @override
  Future<CreateApplicationResponse> postCreateApplication({
    required String productId,
    required String value,
    required String type,
    required String sessionId,
    required bool withMonthlyPaymentReduction,
    required double amount,
    required String date,
  }) {
    return checkNetErrors(() async {
      final model = await api.createApplication(CreateApplicationRequest(
          productId: productId,
          value: value,
          sessionId: sessionId,
          type: type,
          withMonthlyPaymentReduction: withMonthlyPaymentReduction,
          amount: amount,
          date: date));
      return model;
    });
  }

  @override
  Future<SignApplicationResponse> postSignApplication(
    String documentId,
    String productId,
    String value,
    String sessionId,
    String type,
    bool withMonthlyPaymentReduction,
    double amount,
    String date,
  ) {
    return checkNetErrors(() async {
      final model = await api.signApplication(SignApplicationRequest(
          documentId: documentId,
          productId: productId,
          value: value,
          sessionId: sessionId,
          type: type,
          withMonthlyPaymentReduction: withMonthlyPaymentReduction,
          amount: amount,
          date: date));
      return model;
    });
  }

  @override
  Future<CancelApplicationResponse> postCancelApplication(String productId, String applicationId) {
    return checkNetErrors(() async {
      final model =
          await api.cancelApplication(CancelApplicationRequest(productId: productId, applicationId: applicationId));
      return model;
    });
  }

  @override
  Future<OtpCodeResponse> repeatOtpCode(String sessionId) {
    return checkNetErrors(() async {
      final model = await api.repeatOtpCode(sessionId);
      return model;
    });
  }

  @override
  Future<OtpCodeResponse> sendOtpCode() {
    return checkNetErrors(() async {
      final model = await api.sendOtpCode();
      return model;
    });
  }

  @override
  Future<OtpVerifyResponse> verifyOtpCode(String sessionId, String valueOtp) {
    return checkNetErrors(() async {
      final model = await api.verifyOtpCode(sessionId, valueOtp);
      return model;
    });
  }

  @override
  Future<void> initApplications() => checkNetErrors(api.initApplications);

  @override
  Future<ApplicationStatusResponse> getStatusCreation(String correlationId) {
    int timeoutDuration = remoteConfig.getInt(key: 'earlyRepaymentTimeoutDuration') ?? defaultTimeoutDuration;
    return requestPullOrchestrator.startPulling<ApplicationStatusResponse>(
      () => checkNetErrors(() async {
        final model = await api.fetchStatusCreation(correlationId);
        return model;
      }),
      key: 'getApplicationCreationStatus',
      strategy: BaseRequestPullStrategy(),
      timeoutDuration: Duration(seconds: timeoutDuration),
      successCompletePredicate: (res) {
        return res.data.status == ApplicationResultStatus.ok;
      },
    );
  }

  @override
  Future<ApplicationStatusResponse> getStatusCancellation(String correlationId) {
    int timeoutDuration = remoteConfig.getInt(key: 'earlyRepaymentTimeoutDuration') ?? defaultTimeoutDuration;
    return requestPullOrchestrator.startPulling<ApplicationStatusResponse>(
      () => checkNetErrors(() async {
        final model = await api.fetchStatusCancellation(correlationId);
        return model;
      }),
      key: 'getApplicationCancellationStatus',
      strategy: BaseRequestPullStrategy(),
      timeoutDuration: Duration(seconds: timeoutDuration),
      successCompletePredicate: (res) {
        return res.data.status == ApplicationResultStatus.ok;
      },
    );
  }
}
