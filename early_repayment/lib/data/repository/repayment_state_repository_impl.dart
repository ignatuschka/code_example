import 'package:early_repayment/domain/entity/repayment_state.dart';
import 'package:early_repayment/domain/repository/repayment_state_repository.dart';

class RepaymentStateRepositoryImpl extends RepaymentStateRepository {
  late RepaymentState _state;

  @override
  RepaymentState get state => _state;

  @override
  void initializeState([RepaymentState? state]) {
    _state = state ?? RepaymentState(productId: '', currency: '', courtDebt: 123, accountNumber: '');
  }

  @override
  void updateState(RepaymentStatePartial state) {
    _state = _state.merge(state);
  }
}
