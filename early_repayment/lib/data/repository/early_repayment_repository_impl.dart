import 'package:early_repayment/data/data_sources/early_repayment_remote_data_source.dart';
import 'package:early_repayment/data/models/create_application_model.dart';
import 'package:early_repayment/domain/entity/application_data_entity.dart';
import 'package:early_repayment/domain/entity/application_status_entity.dart';
import 'package:early_repayment/domain/entity/applications_entity.dart';
import 'package:early_repayment/domain/entity/availability_entity.dart';
import 'package:early_repayment/domain/entity/cancel_application_entity.dart';
import 'package:early_repayment/domain/entity/otp_code_entity.dart';
import 'package:early_repayment/domain/entity/otp_verify_entity.dart';
import 'package:early_repayment/domain/entity/precomputation_entity.dart';
import 'package:early_repayment/domain/entity/sign_application_entity.dart';
import 'package:early_repayment/domain/repository/early_repayment_repository.dart';

class EarlyRepaymentRepositoryImpl implements EarlyRepaymentRepository {
  const EarlyRepaymentRepositoryImpl({required this.remoteDataSource});

  final EarlyRepaymentRemoteDataSource remoteDataSource;

  @override
  Future<CancelApplicationEntity> cancelApplication(String productId, String applicationId) async {
    final result = await remoteDataSource.postCancelApplication(productId, applicationId);
    return result.data.toEntity();
  }

  @override
  Future<CreateApplicationModel> createApplication({
    required String productId,
    required String otp,
    required String sessionId,
    required String type,
    required bool withMonthlyPaymentReduction,
    required double amount,
    required String date,
  }) async {
    final result = await remoteDataSource.postCreateApplication(
      productId: productId,
      value: otp,
      sessionId: sessionId,
      type: type,
      withMonthlyPaymentReduction: withMonthlyPaymentReduction,
      amount: amount,
      date: date,
    );
    return result.data;
  }

  @override
  Future<ApplicationDataEntity> getApplicationData(String productId, String applicationId) async {
    final result = await remoteDataSource.getApplicationData(productId, applicationId);
    return result.data.toEntity();
  }

  @override
  Future<List<ApplicationsEntity>> getApplications(String productId) async {
    final result = await remoteDataSource.getApplications(productId);
    return result.data.map((e) => e.toEntity()).toList();
  }

  @override
  Future<AvailabilityEntity> getAvailability(String productId) async {
    final result = await remoteDataSource.getAvailability(productId);
    return result.data.toEntity();
  }

  @override
  Future<PrecomputationEntity> getPrecomputation(
      String productId, String type, bool? withMonthlyPaymentReduction, double? amount, String date) async {
    final result = await remoteDataSource.getPrecomputation(productId, type, withMonthlyPaymentReduction, amount, date);
    return result.data.toEntity();
  }

  @override
  Future<PrecomputationEntity> getNewPrecomputation(
      String productId, String type, bool? withMonthlyPaymentReduction, double? amount, String date) async {
    final result = await remoteDataSource.getPrecomputation(productId, type, withMonthlyPaymentReduction, amount, date);
    return result.data.toEntity();
  }

  @override
  Future<SignApplicationEntity> signApplication(String documentId, String sessionId, String value, String productId,
      String type, bool withMonthlyPaymentReduction, double amount, String date) async {
    final result = await remoteDataSource.postSignApplication(
        documentId, sessionId, value, productId, type, withMonthlyPaymentReduction, amount, date);
    return result.data.toEntity();
  }

  @override
  Future<OtpCodeEntity> repeatOtpCode(String sessionId) async {
    final result = await remoteDataSource.repeatOtpCode(sessionId);
    return result.data.toEntity();
  }

  @override
  Future<OtpCodeEntity> sendOtpCode() async {
    final result = await remoteDataSource.sendOtpCode();
    return result.data.toEntity();
  }

  @override
  Future<OtpVerifyEntity> verifyOtpCode(String sessionId, String valueOtp) async {
    final result = await remoteDataSource.verifyOtpCode(sessionId, valueOtp);
    return result.data.toEntity();
  }

  @override
  Future<void> initApplications() => remoteDataSource.initApplications();

  @override
  Future<ApplicationStatusEntity> getStatusCreation(String correlationId) async {
    final result = await remoteDataSource.getStatusCreation(correlationId);
    return result.data.toEntity();
  }

  @override
  Future<ApplicationStatusEntity> getStatusCancellation(String correlationId) async {
    final result = await remoteDataSource.getStatusCancellation(correlationId);
    return result.data.toEntity();
  }
}
