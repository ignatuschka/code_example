abstract class EarlyRepaymentText {
  static const earlyRepaymentAppBarTitle = 'Досрочное погашение';
  static const partRepaymentAppBarTitle = 'Частичное погашение';
  static const fullRepaymentAppBarTitle = 'Полное погашение';
  static const full = 'Полное';
  static const repaymentApplicationAppBarTitle = 'Заявка на погашение';
  static const apply = 'Подать заявку';
  static const currentApplication = 'Текущая заявка';
  static const applicationArchive = 'Архив заявок';
  static const repaymentType = 'Тип погашения';
  static const executionDate = 'Дата исполнения';
  static const recalculationType = 'Тип перерасчета графика платежей';
  static const reducingMonthlyPayments = 'Сокращение суммы ежемесячных платежей';
  static const reductionLoanTerm = 'Сокращение срока кредита';
  static const monthlyPaymentAmount = 'Сумма ежемесячного платежа';
  static const amountPartEarlyRepayment = 'Сумма частично-досрочного погашения';
  static const amountEarlyRepayment = 'Сумма досрочного погашения';
  static const amountPartRepayment = 'Сумма частичного погашения';
  static const amountFullEarlyRepayment = 'Сумма полного досрочного погашения';
  static const paymentAmount = 'Сумма платежа';
  static const partEarly = 'Частично-досрочное';
  static const partEarlyRepayment = 'Частично-досрочное погашение';
  static const fullEarlyRepayment = 'Полное досрочное погашение';
  static const accountBalance = 'Остаток на счете погашения';
  static const needToReplenish = 'Необходимо пополнить';
  static const needToReplenishment = 'Необходимо внести';
  static const inProcessing = 'В обработке';
  static const completed = 'Исполнена';
  static const canceled = 'Аннулирована';
  static const status = 'Статус';
  static const replenish = 'Пополнить';
  static const cancel = 'Удалить';
  static const creditKind = 'Тип кредита';
  static const contractNumber = 'Номер договора';
  static const appDate = 'Дата заявки';
  static const amountRepayment = 'Сумма погашения';
  static const unknown = 'Неизвестно';
  static const stepOneOfThree = 'Шаг 1 из 3';
  static const stepTwoOfThree = 'Шаг 2 из 3';
  static const stepThreeOfThree = 'Шаг 3 из 3';
  static const chooseRepaymentType = 'Выберите тип погашения';
  static const debtAmount = 'Сумма задолженности';
  static const continueButtonText = 'Продолжить';
  static const chooseOptionErrorText = 'Необходимо выбрать тип погашения';
  static const aboutFullRepayment = 'О полном погашении';
  static const mainDebt = 'Основной долг';
  static const creditAgreementRequirements = 'Требования по кредитному договору';
  static const repaymentAccountBalance = 'Остаток на счете погашения';
  static const confirmationAppBarTitle = 'Подтверждение';
  static const firstContentCheckBoxText =
      'Настоящим прошу Банк внести изменения в график погашения по вышеуказанному Кредитному договору';
  static const secondContentCheckBoxText =
      'Подтверждаю и согласен(а), с условием обеспечить на моем счете наличие суммы достаточной для исполнения заявки, иначе Банком будет списан имеющийся на счете остаток, а при отсутствии средств данная заявка будет аннулирована в дату наступления срока ее исполнения';
  static const needToChooseRepaymentType = 'Необходимо выбрать тип перерасчета';
  static const paymentSchedulRecalcType = 'Тип перерасчета графика платежей';
  static const chooseRecalcType = 'Выберите тип перерасчета';
  static const recalcType = 'Тип перерасчета';
  static const aboutNextPayment = 'О ближайшем платеже';
  static const overdueDept = 'Просроченная задолженность';
  static const secondStepText =
      'Расчет является предварительным на основе полученных данных по договору. Окончательная сумма ежемесячного платежа будет рассчитана после исполнения заявки на частично-досрочное погашение кредита.';

  static const verifyOTP = "Для подписания Заявления введите код из СМС";
  static const invalidOTP = "Неверный код";
  static const timeOut = "Время ожидания истекло";
  static const verifyOTPAgain = "Введите код еще раз";
  static const partEarlyRepaymentType = 'ЧАСТ_ДОС_ГАШ';
  static const fullEarlyRepaymentType = 'ПОЛН_ДОС_ГАШ';
  static const appCancellation = "Аннулирование заявки";
  static const yourAppProcessind = "Ваша заявка обрабатывается";
  static const appNotCancelled = "Заявка не аннулирована";
  static const cancellationError = "Произошла ошибка.\nОтправьте номер ошибки в чат поддержки\nи оператор поможет вам";
  static const appIsCancelled = "Заявка аннулирована";
  static const cancellationSucsses = "Вы можете снова оформитьзаявку\nна досрочное погашение в мобильном\nприложении";
  static const registrApp = "Регистрация заявки";
  static const registrAppSubtitle = "Ваша заявка обрабатывается.\nОжидайте окончания регистрации";
  static const copied = "Скопировано";
}
