import 'package:early_repayment/domain/entity/create_application_params.dart';
import 'package:early_repayment/domain/entity/otp_code_entity.dart';
import 'package:early_repayment/domain/entity/precomputation_entity.dart';
import 'package:early_repayment/domain/entity/repayment_state.dart';
import 'package:early_repayment/domain/usecase/create_application_usecase.dart';
import 'package:early_repayment/domain/usecase/send_otp_code.dart';
import 'package:early_repayment/domain/usecase/update_state_use_case.dart';
import 'package:early_repayment/extensions/date_time_extensions.dart';
import 'package:early_repayment/navigation/early_repayment_navigator.dart';
import 'package:early_repayment/resources/early_repayment_text.dart';
import 'package:flutter_atb_common/usecase/usecase.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:verification/sms_verification/presentation/bloc/sms_confirm_bloc/sms_confirm_bloc_delegate.dart';
import 'package:verification/sms_verification/presentation/bloc/sms_confirm_bloc/sms_confirm_event.dart';
import 'package:verification/sms_verification/presentation/bloc/sms_confirm_bloc/sms_confirm_state.dart';

class EarlyRepaymentConfirmation extends SmsConfirmBlocDelegate {
  final SendOtpCodeUseCase sendOtpCodeUseCase;
  final CreateApplicationUseCase createApplicationUseCase;
  final CreateApplicationParams params;
  final EarlyRepaymentNavigator navigator;
  final PrecomputationEntity precomputation;
  late OtpCodeEntity otpCodeEntity;
  final UpdateStateUseCase updateStateUseCase;

  EarlyRepaymentConfirmation({
    required this.updateStateUseCase,
    required this.createApplicationUseCase,
    required this.sendOtpCodeUseCase,
    required this.precomputation,
    required this.params,
    required this.navigator,
  });

  @override
  Future<int> sendCode(SendCodeSmsConfirmEvent event, Emitter<SmsConfirmState> emit) async {
    final UseCaseResult<OtpCodeEntity> result = await sendOtpCodeUseCase.call(EmptyUsecaseParams());
    if (result.isSuccessful) {
      otpCodeEntity = result.result;
      return _getDateTimeDifference(DateTime.tryParse(otpCodeEntity.repeatFrom));
    } else {
      /// Можно написать обработку ошибку и отправлять нужный текст
      throw result.exception;
    }
  }

  ///Если в ответе сервис возвращает OTPResult.status = DELIVERY или SEND или WAIT,
  ///тогда МП обновляет счетчик в ботомшите как и при первом выводе ботомшита:
  // Если в ответе сервис возвращает OTPResult.status =  ERROR или NOT_DELIVERY или UNKNOWN,
  // тогда МП выводит экран ошибки подтверждения
  @override
  Future<void> confirmCode(ConfirmCodeSmsConfirmEvent event, Emitter<SmsConfirmState> emit) async {
    final result = await createApplicationUseCase(CreateApplicationUseCaseParams(
      appParams: params,
      session: otpCodeEntity.sessionId,
      otp: event.value,
    ));

    if (result.isSuccessful) {
      final res = result.result;
      final otpResult = result.result.otpResult;
      final otpResultStatus = otpResult?.status;
      if (otpResultStatus == OTPCommand.invalid) {
        throw InvalidOTP(EarlyRepaymentText.invalidOTP);
      } else if (otpResultStatus == OTPCommand.delivery ||
          otpResultStatus == OTPCommand.send ||
          otpResultStatus == OTPCommand.wait) {
      } else {
        /// навигация на экран ошибки
        throw Exception('Navigate to error page');
      }
      updateStateUseCase(RepaymentStatePartial(
        plannedExecutionDate: res.repaymentData?.plannedExecutionDate?.isoToDateTime(),
        monthlyPaymentAmount: res.repaymentData?.monthlyPaymentAmount,
        courtDebt: res.repaymentData?.courtDept,
        loanInterest: res.repaymentData?.loanInterest,
        overdueDept: res.repaymentData?.overdueDept,
        accountBalance: res.repaymentData?.accountBalance,
        replenishmentAmount: res.repaymentData?.replenishmentAmount,
        amount: res.repaymentData?.amount,
      ));
    } else {
      /// Можно написать обработку ошибку и отправлять нужный текст
      throw result.exception;
    }
  }

  @override
  Future<void> onErrorConfirm() async {}

  @override
  Future<void> onSuccessConfirm() async {
    navigator.navigateToResultScreen();
  }

  int _getDateTimeDifference(DateTime? dateUntil) {
    final now = DateTime.now();
    final difference = dateUntil?.difference(now);
    return (difference?.inSeconds) ?? 0;
  }
}

/// Неверный код из смс
class InvalidOTP implements Exception {
  final String message;

  InvalidOTP(this.message);
}

/// Обновить счетчик
class RefreshTimer implements Exception {
  final int repeatSeconds;

  RefreshTimer(this.repeatSeconds);
}

class OTPCommand {
  static const invalid = "INVALID";
  static const delivery = "DELIVERY";
  static const send = "SEND";
  static const wait = "WAIT";
  static const error = "ERROR";
  static const notDelivery = "NOT_DELIVERY";
  static const unknown = "UNKNOWN";
}
