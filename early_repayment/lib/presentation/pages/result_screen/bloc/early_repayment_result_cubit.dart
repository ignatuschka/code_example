import 'package:accounts/domain/entity/account_entity.dart';
import 'package:accounts/domain/use_case/get_account_by_number_use_case.dart';
import 'package:accounts_transfer/domain/entity/account_transfer_initial_data.dart';
import 'package:accounts_transfer/navigation/accounts_transfer_navigator.dart';
import 'package:early_repayment/domain/usecase/get_state_use_case.dart';
import 'package:early_repayment/navigation/early_repayment_navigator.dart';
import 'package:early_repayment/presentation/pages/result_screen/bloc/early_repayment_result_state.dart';
import 'package:flutter_atb_common/bloc/base_cubit.dart';
import 'package:flutter_atb_common/usecase/usecase.dart';

// TODO: обработать кнопку пополнить
class EarlyRepaymentResultCubit extends BaseCubit<EarlyRepaymentResultState> {
  final GetStateUseCase getStateUseCase;
  final EarlyRepaymentNavigator navigator;
  final GetAccountByNumberUseCase getAccountByNumberUseCase;
  final AccountsTransferNavigator accountsTransferNavigator;

  EarlyRepaymentResultCubit(
    this.getStateUseCase,
    this.navigator,
    this.getAccountByNumberUseCase,
    this.accountsTransferNavigator,
  ) : super(EarlyRepaymentResultState.initial()) {
    init();
  }

  void init() {
    processSyncUseCase(
      getStateUseCase(EmptyUsecaseParams()),
      onSuccess: (result) {
        emit(state.setAppState(result));
      },
    );
    emit(state.setLoading(false));
  }

  void returnHome() => navigator.navigateToRoot();

  void replenish() async {
    final amount = state.appState?.replenishmentAmount ?? 0;
    await processUseCase<AccountEntity>(() => getAccountByNumberUseCase(state.appState?.accountNumber ?? ''),
        onSuccess: (result) {
      final initialData = AccountTransferInitialData(
        accountCredit: result,
        amountWithOutCurrency: amount.toString(),
        allowChangingAmount: true,
      );
      accountsTransferNavigator.navigateToTransferBetweenAccountsScreen(initialData: initialData);
    }, onError: (error) {
      accountsTransferNavigator.navigateToTransferBetweenAccountsScreen();
    });
  }
}
