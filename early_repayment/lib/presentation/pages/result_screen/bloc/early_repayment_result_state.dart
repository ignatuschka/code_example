import 'package:built_value/built_value.dart';
import 'package:early_repayment/domain/entity/application_status_entity.dart';
import 'package:early_repayment/domain/entity/repayment_state.dart';
import 'package:flutter_atb_common/usecase/def_state.dart';

part 'early_repayment_result_state.g.dart';

abstract class EarlyRepaymentResultState
    with DefState<EarlyRepaymentResultState>
    implements Built<EarlyRepaymentResultState, EarlyRepaymentResultStateBuilder> {
  RepaymentState? get appState;

  bool get showRefillButton;

  ApplicationStatusEntity? get status;

  const EarlyRepaymentResultState._();

  factory EarlyRepaymentResultState([void Function(EarlyRepaymentResultStateBuilder)? updates]) =
      _$EarlyRepaymentResultState;

  factory EarlyRepaymentResultState.initial() => EarlyRepaymentResultState((b) => b
    ..isLoading = true
    ..showRefillButton = true
    ..status
    ..appState);

  EarlyRepaymentResultState setAppState(RepaymentState state) => rebuild((b) => (b)..appState = state);
  EarlyRepaymentResultState setShowRefill(RepaymentState state) => rebuild((b) => (b)..appState = state);
  EarlyRepaymentResultState setStatus(ApplicationStatusEntity status) => rebuild((b) => (b)..status = status);
}
