import 'package:early_repayment/domain/entity/reduction_type.dart';
import 'package:early_repayment/domain/enum/early_repayment_statuses.dart';
import 'package:early_repayment/extensions/string_extensions.dart';
import 'package:early_repayment/l10n/early_repayment.localizations.dart';
import 'package:early_repayment/presentation/pages/result_screen/bloc/early_repayment_result_cubit.dart';
import 'package:early_repayment/presentation/pages/result_screen/bloc/early_repayment_result_state.dart';
import 'package:flutter/material.dart';
import 'package:flutter_atb_common/extensions/build_context_extenstions.dart';
import 'package:flutter_atb_common/localization/localized_widget.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:ui_kit/theme.dart';
import 'package:ui_kit/ui_kit.dart';

final _format = DateFormat('dd.MM.yyyy');

class EarlyRepaymentResultView extends StatefulWidget {
  const EarlyRepaymentResultView({super.key});

  @override
  State<EarlyRepaymentResultView> createState() => _EarlyRepaymentResultViewState();
}

class _EarlyRepaymentResultViewState extends State<EarlyRepaymentResultView>
    with LocalizedWidget<EarlyRepaymentLocalizations, EarlyRepaymentResultView> {
  @override
  Widget build(BuildContext context) {
    final textThemeExtension = Theme.of(context).extension<TextThemeExtension>();
    return WillPopScope(
      onWillPop: () async {
        return false;
      },
      child: Scaffold(
        appBar: TopStatusBar(statusBarHeight: context.mediaQueryPadding.top),
        body: SafeArea(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16),
            child: BlocBuilder<EarlyRepaymentResultCubit, EarlyRepaymentResultState>(
              builder: (context, state) {
                final appState = state.appState;
                if (state.isLoading || appState == null) {
                  return const SizedBox();
                }

                return Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Gap(40),
                    Align(
                      alignment: Alignment.center,
                      child: Text(localization.applicationAccepted,
                          style: textThemeExtension?.titleExtra, textAlign: TextAlign.center),
                    ),
                    if (appState.type == EarlyRepaymentType.part)
                      Padding(
                        padding: const EdgeInsets.only(left: 16, top: 8, bottom: 24, right: 16),
                        child: Text(
                          (appState.typeRecalculation == ReductionType.amount)
                              ? localization.amountResult
                              : (appState.typeRecalculation == ReductionType.duration)
                                  ? localization.durationResult
                                  : '',
                          textAlign: TextAlign.center,
                        ),
                      ),
                    Expanded(
                        child: SingleChildScrollView(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          if (appState.type == EarlyRepaymentType.part) ...[
                            Display(
                              label: localization.executionDate,
                              text: _format.format(appState.plannedExecutionDate ?? DateTime.now()),
                              size: DisplaySize.S,
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 20, bottom: 8),
                              child: Text(localization.aboutNextPayment, style: context.textTheme.titleMedium),
                            ),
                            Display(
                              label: localization.paymentDate,
                              text: _format.format(appState.loanPaymentDate ?? DateTime.now()),
                              size: DisplaySize.S,
                            ),
                            Display(
                                label: localization.paymentAmount,
                                text: '${appState.monthlyPaymentAmount?.formatToAmount(appState.currency)}'),
                            Display(
                                label: localization.amountPartEarlyRepayment,
                                text: '${appState.amount?.formatToAmount(appState.currency)}'),
                          ] else if (appState.type == EarlyRepaymentType.full) ...[
                            Padding(
                              padding: const EdgeInsets.only(top: 54, bottom: 12),
                              child: Text(localization.aboutFullRepaymentCredit, style: context.textTheme.titleMedium),
                            ),
                            Display(
                              label: localization.plannedExecutionDate,
                              text: _format.format(appState.plannedExecutionDate ?? DateTime.now()),
                              size: DisplaySize.S,
                            ),
                            Display(
                                label: localization.amountFullEarlyRepayment,
                                text: '${appState.amount?.formatToAmount(appState.currency)}'),
                            Display(
                              label: localization.mainDebt,
                              text: appState.courtDebt.formatToAmount(appState.currency),
                              size: DisplaySize.S,
                            ),
                            Display(
                                label: localization.creditAgreementRequirements,
                                text: '${appState.loanInterest?.formatToAmount(appState.currency)}'),
                          ] else
                            ...[],
                          Display(
                            label: localization.accountBalance,
                            text: '${appState.accountBalance?.formatToAmount(appState.currency)}',
                            size: DisplaySize.S,
                          ),
                          Display(
                            label: localization.needToReplenish,
                            text: '${appState.replenishmentAmount?.formatToAmount(appState.currency)}',
                            size: DisplaySize.S,
                          ),
                        ],
                      ),
                    )),
                    if ((state.appState?.replenishmentAmount ?? 0) > 0)
                      Padding(
                        padding: const EdgeInsets.only(bottom: 16.0),
                        child: SizedBox(
                          height: 48,
                          child: AtbButton(
                            type: AtbButtonType.primary,
                            title: localization.replenish,
                            onPressed: context.read<EarlyRepaymentResultCubit>().replenish,
                          ),
                        ),
                      ),
                    SizedBox(
                      height: 48,
                      child: AtbButton(
                        type: AtbButtonType.secondary,
                        title: localization.backToMain,
                        onPressed: context.read<EarlyRepaymentResultCubit>().returnHome,
                      ),
                    ),
                  ],
                );
              },
            ),
          ),
        ),
      ),
    );
  }

  @override
  EarlyRepaymentLocalizations? getLocalization(BuildContext context) => EarlyRepaymentLocalizations.of(context);
}
