import 'package:early_repayment/l10n/early_repayment.localizations.dart';
import 'package:early_repayment/presentation/pages/result_screen/bloc/early_repayment_result_cubit.dart';
import 'package:early_repayment/presentation/pages/result_screen/bloc/early_repayment_result_state.dart';
import 'package:flutter/material.dart';
import 'package:flutter_atb_common/extensions/build_context_extenstions.dart';
import 'package:flutter_atb_common/localization/localized_widget.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ui_kit/theme.dart';
import 'package:ui_kit/ui_kit.dart';

class CancelApplicationResultView extends StatefulWidget {
  const CancelApplicationResultView({super.key});

  @override
  State<CancelApplicationResultView> createState() => _CancelApplicationResultViewState();
}

class _CancelApplicationResultViewState extends State<CancelApplicationResultView>
    with LocalizedWidget<EarlyRepaymentLocalizations, CancelApplicationResultView> {
  @override
  Widget build(BuildContext context) {
    final textThemeExtension = Theme.of(context).extension<TextThemeExtension>();
    return WillPopScope(
      onWillPop: () async {
        return false;
      },
      child: Scaffold(
        appBar: TopStatusBar(
          statusBarHeight: context.mediaQueryPadding.top,
        ),
        body: SafeArea(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16),
            child: BlocBuilder<EarlyRepaymentResultCubit, EarlyRepaymentResultState>(
              builder: (context, state) {
                return Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const Gap(32),
                    Align(
                      alignment: Alignment.center,
                      child: Text(localization.cancelResultTitle,
                          style: textThemeExtension?.titleExtra, textAlign: TextAlign.center),
                    ),
                    Align(
                      alignment: Alignment.center,
                      child: Padding(
                        padding: const EdgeInsets.only(left: 16, top: 8, bottom: 24, right: 16),
                        child: Text(
                          localization.cancelResultText,
                          style: Theme.of(context).textTheme.bodyMedium,
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ),
                    const Spacer(),
                    SizedBox(
                      height: 48,
                      child: AtbButton(
                        type: AtbButtonType.secondary,
                        title: localization.backToMain,
                        onPressed: context.read<EarlyRepaymentResultCubit>().returnHome,
                      ),
                    ),
                  ],
                );
              },
            ),
          ),
        ),
      ),
    );
  }

  @override
  EarlyRepaymentLocalizations? getLocalization(BuildContext context) => EarlyRepaymentLocalizations.of(context);
}
