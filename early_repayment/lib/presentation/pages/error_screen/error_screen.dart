import 'package:early_repayment/l10n/early_repayment.localizations.dart';
import 'package:early_repayment/resources/early_repayment_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_atb_common/extensions/build_context_extenstions.dart';
import 'package:flutter_atb_common/localization/localized_widget.dart';
import 'package:ui_kit/theme.dart';
import 'package:ui_kit/ui_kit.dart';

class EarlyRepaymentErrorView extends StatefulWidget {
  final String? code;
  final VoidCallback onTap;
  final bool isCreatingAppScreenError;

  const EarlyRepaymentErrorView({super.key, this.code, required this.onTap, required this.isCreatingAppScreenError});

  @override
  State<EarlyRepaymentErrorView> createState() => _EarlyRepaymentErrorViewState();
}

class _EarlyRepaymentErrorViewState extends State<EarlyRepaymentErrorView>
    with LocalizedWidget<EarlyRepaymentLocalizations, EarlyRepaymentErrorView> {
  @override
  Widget build(BuildContext context) {
    final textThemeExtension = Theme.of(context).extension<TextThemeExtension>();
    final titleText =
        widget.isCreatingAppScreenError ? localization.applicationNotAccepted : EarlyRepaymentText.appNotCancelled;
    final subtitleText =
        widget.isCreatingAppScreenError ? localization.applicationErrorMessage : EarlyRepaymentText.cancellationError;
    return WillPopScope(
      onWillPop: () async {
        return false;
      },
      child: Scaffold(
        appBar: TopStatusBar(statusBarHeight: context.mediaQueryPadding.top, state: TopStatusBarState.error),
        body: SafeArea(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Gap(40),
                Align(
                  alignment: Alignment.center,
                  child: Text(titleText, style: textThemeExtension?.titleExtra, textAlign: TextAlign.center),
                ),
                const Gap(8),
                Align(
                  alignment: Alignment.center,
                  child: Text(subtitleText, style: context.textTheme.bodyMedium, textAlign: TextAlign.center),
                ),
                if (widget.code != null)
                  Padding(
                    padding: const EdgeInsets.only(top: 36.0),
                    child: GestureDetector(
                      onTap: () {
                        Clipboard.setData(ClipboardData(text: widget.code ?? ''));
                        AnimatedSnackBar.rounded(
                          EarlyRepaymentText.copied,
                          snackBarPosition: SnackBarPosition.bottom,
                          curve: Curves.easeOutBack,
                        ).show(context);
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(widget.code ?? '', style: context.textTheme.titleMedium),
                          const Gap(8),
                          const AtbIcon(assetPath: UiAssets.copy24)
                        ],
                      ),
                    ),
                  ),
                const Spacer(),
                SizedBox(
                  height: 48,
                  child: AtbButton(
                    type: AtbButtonType.secondary,
                    title: localization.goToChat,
                    onPressed: widget.onTap,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  @override
  EarlyRepaymentLocalizations? getLocalization(BuildContext context) => EarlyRepaymentLocalizations.of(context);
}
