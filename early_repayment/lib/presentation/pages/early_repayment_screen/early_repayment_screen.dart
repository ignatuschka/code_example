import 'package:early_repayment/l10n/early_repayment.localizations.dart';
import 'package:early_repayment/navigation/early_repayment_navigator.dart';
import 'package:early_repayment/presentation/pages/early_repayment_screen/bloc/early_repayment_screen_bloc.dart';
import 'package:early_repayment/presentation/pages/early_repayment_screen/bloc/early_repayment_screen_event.dart';
import 'package:early_repayment/presentation/pages/early_repayment_screen/bloc/early_repayment_screen_state.dart';
import 'package:early_repayment/resources/early_repayment_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_atb_common/bloc/bloc_instance_provider.dart';
import 'package:flutter_atb_common/extensions/build_context_extenstions.dart';
import 'package:flutter_atb_common/localization/localized_widget.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:ui/widgets/loading_widget.dart';
import 'package:ui/resources/ui_text.dart' as ui_text;
import 'package:ui_kit/theme.dart';
import 'package:ui_kit/ui_kit.dart';

class EarlyRepaymentScreen extends StatefulWidget {
  const EarlyRepaymentScreen({super.key});

  @override
  State<EarlyRepaymentScreen> createState() => _EarlyRepaymentScreenState();
}

class _EarlyRepaymentScreenState extends StateWithBLoc<EarlyRepaymentScreenBloc, EarlyRepaymentScreen>
    with LocalizedWidget<EarlyRepaymentLocalizations, EarlyRepaymentScreen> {
  @override
  Widget build(BuildContext context) {
    final colors = context.theme.extension<AtbColors>();
    return Scaffold(
      appBar: HeaderInner(
        onBackButtonPressed: () => EarlyRepaymentInternalNavigator.navigateBack(),
        title: localization.earlyRepaymentAppBarTitle,
      ),
      body: BlocBuilder<EarlyRepaymentScreenBloc, EarlyRepaymentScreenState>(builder: (context, state) {
        if (state.isLoading) return const LoadingWidget(title: ui_text.UiText.loading);

        String notProcessedAppTitle = '';
        if (state.notProcessedApp?.type == EarlyRepaymentText.fullEarlyRepaymentType) {
          notProcessedAppTitle = EarlyRepaymentText.fullEarlyRepayment;
        } else if (state.notProcessedApp?.type == EarlyRepaymentText.partEarlyRepaymentType) {
          notProcessedAppTitle = EarlyRepaymentText.partEarlyRepayment;
        } else {
          notProcessedAppTitle = state.notProcessedApp?.type ?? '';
        }
        return RefreshIndicator(
          onRefresh: () async => bloc.add(UpdateApplicationsEvent()),
          child: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  if (state.notProcessedApp == null && state.showAddApplication)
                    Padding(
                        padding: const EdgeInsets.symmetric(vertical: 16),
                        child: ChapterCard(
                          title: localization.apply,
                          onTap: () {
                            context.read<EarlyRepaymentScreenBloc>().add(NavigateToFirstStepScreenEvent());
                          },
                          leading: AtbIcon(
                            assetPath: UiAssets.doc24,
                            color: colors?.primary,
                          ),
                        ))
                  else if (state.notProcessedApp == null && !state.showAddApplication)
                    Center(
                      child: Column(
                        children: [
                          const Gap(32),
                          Image.asset(UiAssets.illustrationRadar, package: UiText.packageName),
                          const Gap(24),
                          Text(
                            localization.notAvailable,
                            style: context.textTheme.bodyLarge,
                            textAlign: TextAlign.center,
                          ),
                          const Gap(32),
                          AtbButton(
                            title: localization.getHelp,
                            type: AtbButtonType.secondary,
                            onPressed: () => bloc.add(NavigateToChatEvent()),
                          ),
                          const Gap(44),
                        ],
                      ),
                    ),
                  if (state.notProcessedApp != null) ...[
                    Padding(
                      padding: const EdgeInsets.only(top: 20, bottom: 8),
                      child: Text(
                        localization.currentApplication,
                        style: context.textTheme.titleMedium,
                      ),
                    ),
                    CellDescription(
                      title: notProcessedAppTitle,
                      icon: AtbIcon(width: 24, height: 24, assetPath: UiAssets.check24, color: colors?.primary),
                      description: EarlyRepaymentText.inProcessing,
                      descriptionColor: colors?.tertiary,
                      onTap: () {
                        context
                            .read<EarlyRepaymentScreenBloc>()
                            .add(NavigateToDetailsScreenEvent(state.notProcessedApp?.id ?? ''));
                      },
                    ),
                  ],
                  if (state.processedApps.isNotEmpty)
                    Padding(
                      padding: const EdgeInsets.only(top: 20, bottom: 8),
                      child: Text(
                        localization.applicationArchive,
                        style: context.textTheme.titleMedium,
                      ),
                    ),
                  ListView.builder(
                    itemCount: state.processedApps.length,
                    shrinkWrap: true,
                    physics: const NeverScrollableScrollPhysics(),
                    itemBuilder: (BuildContext context, int index) {
                      String title = '';
                      if (state.processedApps[index].type == EarlyRepaymentText.fullEarlyRepaymentType) {
                        title = EarlyRepaymentText.fullEarlyRepayment;
                      } else if (state.processedApps[index].type == EarlyRepaymentText.partEarlyRepaymentType) {
                        title = EarlyRepaymentText.partEarlyRepayment;
                      } else {
                        title = state.processedApps[index].type;
                      }
                      return CellDescription(
                        title: title,
                        icon: AtbIcon(width: 24, height: 24, assetPath: UiAssets.check24, color: colors?.primary),
                        description: DateFormat('dd.MM.yyyy').format(state.processedApps[index].plannedExecutionDate),
                        onTap: () {
                          context
                              .read<EarlyRepaymentScreenBloc>()
                              .add(NavigateToDetailsScreenEvent(state.processedApps[index].id));
                        },
                      );
                    },
                  ),
                ],
              ),
            ),
          ),
        );
      }),
    );
  }

  @override
  EarlyRepaymentLocalizations? getLocalization(BuildContext context) => EarlyRepaymentLocalizations.of(context);
}
