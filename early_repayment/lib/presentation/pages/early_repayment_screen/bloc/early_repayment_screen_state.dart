import 'package:built_value/built_value.dart';
import 'package:early_repayment/domain/entity/applications_entity.dart';
import 'package:flutter_atb_common/usecase/def_state.dart';

part 'early_repayment_screen_state.g.dart';

abstract class EarlyRepaymentScreenState
    with DefState<EarlyRepaymentScreenState>
    implements Built<EarlyRepaymentScreenState, EarlyRepaymentScreenStateBuilder> {
  List<ApplicationsEntity> get processedApps;

  ApplicationsEntity? get notProcessedApp;

  String get productId;

  bool get showAddApplication;

  EarlyRepaymentScreenState._();

  factory EarlyRepaymentScreenState([void Function(EarlyRepaymentScreenStateBuilder)? updates]) =
      _$EarlyRepaymentScreenState;

  factory EarlyRepaymentScreenState.initial() => EarlyRepaymentScreenState((b) => b
    ..processedApps = []
    ..notProcessedApp = null
    ..productId = ''
    ..isLoading = true
    ..showAddApplication = false);
  EarlyRepaymentScreenState setShowAddApplication(bool showAddApplication) =>
      rebuild((b) => (b)..showAddApplication = showAddApplication);

  EarlyRepaymentScreenState setProcessedApps(List<ApplicationsEntity>? processedApps) =>
      rebuild((b) => (b)..processedApps = processedApps);

  EarlyRepaymentScreenState setNotProcessedApp(ApplicationsEntity? notProcessedApp) =>
      rebuild((b) => (b)..notProcessedApp = notProcessedApp);

  EarlyRepaymentScreenState setProductId(String productId) => rebuild((b) => (b)..productId = productId);
}
