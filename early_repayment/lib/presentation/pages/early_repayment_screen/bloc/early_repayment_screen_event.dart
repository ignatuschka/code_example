sealed class EarlyRepaymentScreenEvent {
  const EarlyRepaymentScreenEvent();
}

class EarlyRepaymentScreenInitialEvent extends EarlyRepaymentScreenEvent {}

class NavigateToDetailsScreenEvent extends EarlyRepaymentScreenEvent {
  final String applicationId;
  const NavigateToDetailsScreenEvent(this.applicationId);
}

class NavigateToFirstStepScreenEvent extends EarlyRepaymentScreenEvent {}

class NavigateToChatEvent extends EarlyRepaymentScreenEvent {}

class UpdateApplicationsEvent extends EarlyRepaymentScreenEvent {}
