import 'package:early_repayment/domain/entity/applications_entity.dart';
import 'package:early_repayment/domain/entity/availability_entity.dart';
import 'package:early_repayment/domain/entity/repayment_state.dart';
import 'package:early_repayment/domain/enum/early_repayment_statuses.dart';
import 'package:early_repayment/domain/usecase/get_applications_usecase.dart';
import 'package:early_repayment/domain/usecase/get_availability_usecase.dart';
import 'package:early_repayment/domain/usecase/init_applications_use_case.dart';
import 'package:early_repayment/domain/usecase/init_state_use_case.dart';
import 'package:early_repayment/navigation/early_repayment_navigator.dart';
import 'package:early_repayment/presentation/pages/early_repayment_screen/bloc/early_repayment_screen_event.dart';
import 'package:early_repayment/presentation/pages/early_repayment_screen/bloc/early_repayment_screen_state.dart';
import 'package:flutter_atb_common/bloc/base_bloc.dart';
import 'package:flutter_atb_common/exception/error_handler.dart';
import 'package:flutter_atb_common/usecase/usecase.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class EarlyRepaymentScreenBloc extends BaseBloc<EarlyRepaymentScreenEvent, EarlyRepaymentScreenState> {
  final GetApplicationsUseCase getApplicationsUseCase;
  final GetAvailabilityUseCase getAvailabilityUseCase;
  final InitApplicationsUseCase initApplicationsUseCase;
  final InitStateUseCase initStateUseCase;
  final String productId;
  final String currency;
  final double courtDebt;
  final String accountNumber;
  final EarlyRepaymentInternalNavigator navigator;

  EarlyRepaymentScreenBloc(
      {required ErrorHandler errorHandler,
      required this.initStateUseCase,
      required this.initApplicationsUseCase,
      required this.getApplicationsUseCase,
      required this.getAvailabilityUseCase,
      required this.productId,
      required this.navigator,
      required this.currency,
      required this.courtDebt,
      required this.accountNumber})
      : super(EarlyRepaymentScreenState.initial(), errorHandler: errorHandler) {
    on<EarlyRepaymentScreenInitialEvent>(_initialEvent);
    on<NavigateToDetailsScreenEvent>((event, emit) => _navigateToDetailsScreen(event.applicationId));
    on<NavigateToFirstStepScreenEvent>((event, emit) => _navigateToFirstStepScreen());
    on<NavigateToChatEvent>((event, emit) => _navigateToChatEvent());
    on<UpdateApplicationsEvent>(_updateApplications);
    add(EarlyRepaymentScreenInitialEvent());
  }

  Future<void> _initialEvent(EarlyRepaymentScreenEvent event, Emitter<EarlyRepaymentScreenState> emit) async {
    emit(state.setLoading(true));
    EarlyRepaymentScreenState newState;
    newState = state.setProductId(productId);
    initStateUseCase(RepaymentState(
      productId: productId,
      currency: currency,
      courtDebt: courtDebt,
      accountNumber: accountNumber,
    ));
    await processUseCase<AvailabilityEntity>(
        () => getAvailabilityUseCase(GetAvailabilityUseCaseParams(productId: productId)), onSuccess: (result) {
      newState = newState.setShowAddApplication(result.earlyRepaymentEnabled);
    });
    add(UpdateApplicationsEvent());
    // newState = newState.setLoading(false);
    emit(newState);
  }

  Future<void> _updateApplications(UpdateApplicationsEvent event, Emitter<EarlyRepaymentScreenState> emit) async {
    emit(state.setLoading(true));
    var newState = state;
    await processUseCase(() => initApplicationsUseCase(EmptyUsecaseParams()), onSuccess: (_) {});

    await processUseCase<List<ApplicationsEntity>>(
      () => getApplicationsUseCase(GetApplicationsUseCaseParams(productId: productId)),
      onSuccess: (applications) {
        if (applications.isNotEmpty) {
          ApplicationsEntity? notProcessedApp;

          if (applications.indexWhere((element) => element.status == ApplicationStatus.notProcessed) != -1) {
            notProcessedApp = applications.firstWhere((element) => element.status == ApplicationStatus.notProcessed);
          }
          List<ApplicationsEntity> processedApps = [];
          processedApps
              .addAll(applications.where((element) => element.status != ApplicationStatus.notProcessed).toList());
          newState = newState.setProcessedApps(processedApps).setNotProcessedApp(notProcessedApp);
        } else {
          newState = newState.setNotProcessedApp(null);
        }
      },
    );
    emit(newState.setLoading(false));
  }

  void _navigateToDetailsScreen(String applicationId) {
    navigator.navigateToEarlyRepaymentDetailsScreen(applicationId);
  }

  void _navigateToFirstStepScreen() {
    navigator.navigateToEarlyRepaymentFirstStepScreen();
  }

  void _navigateToChatEvent() {
    navigator.navigateToChat();
  }
}
