import 'package:built_value/built_value.dart';
import 'package:early_repayment/domain/entity/application_status_entity.dart';
import 'package:early_repayment/domain/entity/repayment_state.dart';
import 'package:flutter_atb_common/usecase/def_state.dart';

part 'early_repayment_waiting_screen_state.g.dart';

const int _timerStartValue = 59;

abstract class EarlyRepaymentWaitingScreenState
    with DefState<EarlyRepaymentWaitingScreenState>
    implements Built<EarlyRepaymentWaitingScreenState, EarlyRepaymentWaitingScreenStateBuilder> {
  RepaymentState? get appState;

  ApplicationStatusEntity? get status;

  int get showTimeLeft;

  const EarlyRepaymentWaitingScreenState._();

  factory EarlyRepaymentWaitingScreenState([void Function(EarlyRepaymentWaitingScreenStateBuilder)? updates]) =
      _$EarlyRepaymentWaitingScreenState;

  factory EarlyRepaymentWaitingScreenState.initial() => EarlyRepaymentWaitingScreenState((b) => b
    ..isLoading = true
    ..showTimeLeft = _timerStartValue
    ..status
    ..appState);

  EarlyRepaymentWaitingScreenState setAppState(RepaymentState state) => rebuild((b) => (b)..appState = state);
  EarlyRepaymentWaitingScreenState setStatus(ApplicationStatusEntity status) => rebuild((b) => (b)..status = status);
  EarlyRepaymentWaitingScreenState setShowTimeLeft(int showTimeLeft) => rebuild((b) => b..showTimeLeft = showTimeLeft);
}
