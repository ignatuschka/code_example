import 'dart:async';

import 'package:early_repayment/domain/entity/application_status_entity.dart';
import 'package:early_repayment/domain/usecase/get_state_use_case.dart';
import 'package:early_repayment/domain/usecase/get_status_cancellation.dart';
import 'package:early_repayment/domain/usecase/get_status_creation_usecase.dart';
import 'package:early_repayment/navigation/early_repayment_navigator.dart';
import 'package:early_repayment/presentation/pages/early_repayment_waiting/cubit/early_repayment_waiting_screen_state.dart';
import 'package:flutter_atb_common/bloc/base_cubit.dart';
import 'package:flutter_atb_common/usecase/usecase.dart';

// TODO: обработать кнопку пополнить
class EarlyRepaymentWaitingScreenCubit extends BaseCubit<EarlyRepaymentWaitingScreenState> {
  final GetStateUseCase getStateUseCase;
  final EarlyRepaymentNavigator navigator;
  final GetStatusCancellationUseCase getStatusCancellationUseCase;
  final GetStatusCreationUseCase getStatusCreationUseCase;

  EarlyRepaymentWaitingScreenCubit(
    this.getStateUseCase,
    this.navigator,
    this.getStatusCancellationUseCase,
    this.getStatusCreationUseCase,
  ) : super(EarlyRepaymentWaitingScreenState.initial()) {
    init();
  }

  void init() async {
    final appState = getStateUseCase(EmptyUsecaseParams());
    if (appState.isSuccessful) {
      if (appState.result.isCreateResultScreen!) {
        emit(state.setShowTimeLeft(59));
        _startTimer();
        await processUseCase<ApplicationStatusEntity>(
            () => getStatusCreationUseCase(
                GetStatusCreationUseCaseParams(correlationId: appState.result.correlationId ?? '')),
            onSuccess: (result) {
          if (result.status == ApplicationResultStatus.ok && result.code != null && result.description != null) {
            navigator.navigateToErrorScreen(result.code ?? '', true);
          }
          if (result.status == ApplicationResultStatus.ok && result.code == null && result.description == null) {
            navigator.navigateToResultScreen();
          }
        });
      } else {
        emit(state.setShowTimeLeft(59));
        _startTimer();
        await processUseCase<ApplicationStatusEntity>(
            () => getStatusCancellationUseCase(
                GetStatusCancellationUseCaseParams(correlationId: appState.result.correlationId ?? '')),
            onSuccess: (result) {
          if (result.status == ApplicationResultStatus.ok && result.code != null && result.description != null) {
            navigator.navigateToErrorScreen(result.code ?? '', false);
          }
          if (result.status == ApplicationResultStatus.ok && result.code == null && result.description == null) {
            navigator.navigateToCancelResultScreen();
          }
        });
      }
      emit(state.setAppState(appState.result));
    }
    emit(state.setLoading(false));
  }

  void _startTimer() {
    Timer? timer;
    timer = Timer.periodic(const Duration(seconds: 1), (timer) {
      if (state.showTimeLeft > 0) {
        final newShowTimeLeft = state.showTimeLeft - 1;
        emit(state.setShowTimeLeft(newShowTimeLeft));
      }
    });
  }
}

class ApplicationResultStatus {
  static const ok = "OK";
  static const retry = "RETRY";
}
