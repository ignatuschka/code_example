import 'package:built_value/built_value.dart';
import 'package:early_repayment/l10n/early_repayment.localizations.dart';
import 'package:early_repayment/presentation/pages/early_repayment_waiting/cubit/early_repayment_waiting_screen_cubit.dart';
import 'package:early_repayment/presentation/pages/early_repayment_waiting/cubit/early_repayment_waiting_screen_state.dart';
import 'package:early_repayment/resources/early_repayment_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_atb_common/extensions/build_context_extenstions.dart';
import 'package:flutter_atb_common/localization/localized_widget.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ui_kit/theme.dart';
import 'package:ui_kit/ui_kit.dart';

class EarlyRepaymentWaitingScreen extends StatefulWidget {
  const EarlyRepaymentWaitingScreen({super.key});

  @override
  State<EarlyRepaymentWaitingScreen> createState() => _EarlyRepaymentWaitingScreenState();
}

class _EarlyRepaymentWaitingScreenState extends State<EarlyRepaymentWaitingScreen>
    with LocalizedWidget<EarlyRepaymentLocalizations, EarlyRepaymentWaitingScreen> {
  @override
  Widget build(BuildContext context) {
    final colors = context.theme.extension<AtbColors>();
    final textThemeExtension = Theme.of(context).extension<TextThemeExtension>();

    return Scaffold(
      appBar: TopStatusBar(
        statusBarHeight: context.mediaQueryPadding.top,
        state: TopStatusBarState.neutral,
      ),
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16),
          child: BlocBuilder<EarlyRepaymentWaitingScreenCubit, EarlyRepaymentWaitingScreenState>(
              builder: (context, state) {
            return Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                const Gap(32),
                Text(
                    (state.appState?.isCreateResultScreen ?? true)
                        ? EarlyRepaymentText.registrApp
                        : EarlyRepaymentText.appCancellation,
                    style: textThemeExtension?.titleExtra,
                    textAlign: TextAlign.center),
                const Gap(8),
                Text(
                  (state.appState?.isCreateResultScreen ?? true)
                      ? EarlyRepaymentText.registrAppSubtitle
                      : EarlyRepaymentText.yourAppProcessind,
                  textAlign: TextAlign.center,
                ),
                const Gap(90),
                const AtbSpinner(diameter: 88, strokeWidth: 8),
                const Gap(36),
                Text(
                  '00:${state.showTimeLeft.toString().padLeft(2, '0')}',
                  style: context.textTheme.bodyLarge!.copyWith(color: colors?.neutral[50]),
                ),
              ],
            );
          }),
        ),
      ),
    );
  }

  @override
  EarlyRepaymentLocalizations? getLocalization(BuildContext context) => EarlyRepaymentLocalizations.of(context);
}
