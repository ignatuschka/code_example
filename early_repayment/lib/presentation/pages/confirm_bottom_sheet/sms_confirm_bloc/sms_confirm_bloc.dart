import 'dart:async';

import 'package:early_repayment/domain/entity/otp_verify_entity.dart';
import 'package:early_repayment/domain/entity/repayment_state.dart';
import 'package:early_repayment/domain/usecase/otp_verify_usecase.dart';
import 'package:early_repayment/domain/usecase/repeat_otp_code.dart';
import 'package:early_repayment/domain/usecase/update_state_use_case.dart';
import 'package:flutter_atb_common/bloc/base_bloc.dart';
import 'package:flutter_atb_common/usecase/usecase.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:early_repayment/data/models/create_application_model.dart';
import 'package:early_repayment/domain/entity/create_application_params.dart';
import 'package:early_repayment/domain/entity/otp_code_entity.dart';
import 'package:early_repayment/domain/usecase/create_application_usecase.dart';
import 'package:early_repayment/domain/usecase/send_otp_code.dart';
import 'package:early_repayment/navigation/early_repayment_navigator.dart';
import 'package:early_repayment/presentation/pages/confirm_bottom_sheet/sms_confirm_bloc/sms_confirm_event.dart';
import 'package:early_repayment/presentation/pages/confirm_bottom_sheet/sms_confirm_bloc/sms_confirm_state.dart';
import 'package:early_repayment/resources/early_repayment_text.dart';
import 'package:intl/intl.dart';

class SmsConfirmBloc extends BaseBloc<SmsConfirmEvent, SmsConfirmState> {
  final SendOtpCodeUseCase sendOtpCodeUseCase;
  final RepeatOtpCodeUseCase repeatOtpCodeUseCase;
  final OtpVerifyCodeUseCase otpVerifyCodeUseCase;
  final CreateApplicationUseCase createApplicationUseCase;
  final CreateApplicationParams params;
  final EarlyRepaymentNavigator navigator;
  final int repeatSeconds;
  final String title;
  final String smsVerificationSessionId;
  final UpdateStateUseCase updateStateUseCase;

  SmsConfirmBloc({
    required this.updateStateUseCase,
    required this.sendOtpCodeUseCase,
    required this.repeatOtpCodeUseCase,
    required this.otpVerifyCodeUseCase,
    required this.createApplicationUseCase,
    required this.params,
    required this.navigator,
    required this.repeatSeconds,
    required this.title,
    required this.smsVerificationSessionId,
  }) : super(SmsConfirmState.initial(repeatSeconds: repeatSeconds, title: title, sessionId: smsVerificationSessionId)) {
    on<RepeatCodeSmsConfirmEvent>(_repeatCode);
    on<ConfirmCodeSmsConfirmEvent>(_confirmCode);
    on<ChangeCodeEvent>(_changeCode);
    on<ToggleRepeatSendButton>(_toggleButtonWidget);
  }

  Future<void> _repeatCode(RepeatCodeSmsConfirmEvent event, Emitter<SmsConfirmState> emit) async {
    SmsConfirmState newState = state.setLoading(true).failure(null);
    emit(newState);

    newState = newState.setLoading(false);
    final UseCaseResult<OtpCodeEntity> useCaseResult =
        await repeatOtpCodeUseCase.call(RepeatOtpCodeUseCaseParams(sessionId: state.sessionId));
    if (useCaseResult.isSuccessful) {
      final otpCodeEntity = useCaseResult.result;
      final otpStatus = otpCodeEntity.messageStatus.status;
      newState = newState.setSessionId(otpCodeEntity.sessionId);

      /// Время для счетчика
      final repeatSeconds = _getDateTimeDifference(DateTime.tryParse(otpCodeEntity.repeatFrom));
      if (otpStatus == OTPCommand.delivery || otpStatus == OTPCommand.send || otpStatus == OTPCommand.wait) {
        /// Обновляем счетчик
        newState = newState.setRepeatSeconds(repeatSeconds).setRepeatActive(false);
      } else {
        /// Экран ошибки подтверждения отп
        /// todo: текст ошибки
        newState = newState.failure("Произошла ошибка, попробуйте позже");
      }
    } else {
      /// Можно написать обработку ошибку и отправлять нужный текст
      newState = newState.failure(useCaseResult.exception.toString());
    }

    emit(newState);
  }

  Future<void> _confirmCode(event, emit) async {
    SmsConfirmState newState = state.setLoading(true).failure(null);
    emit(newState);

    newState = newState.setTitle(title);

    await processUseCase<OtpVerifyEntity>(
      () => otpVerifyCodeUseCase(OtpVerifyCodeUseCaseParams(sessionId: state.sessionId, valueOtp: state.codeValue)),
      onSuccess: (result) {
        final otpStatus = result.status;
        if (otpStatus == OTPCommand.valid) {
          _createApp();
        } else if (otpStatus == OTPCommand.invalid || otpStatus == OTPCommand.timeOut) {
          /// Ошибка: неверный код
          /// Изменить заголовок на "Введите код еще раз"
          newState = newState.failure(EarlyRepaymentText.invalidOTP).setTitle(EarlyRepaymentText.verifyOTPAgain);
        } else {
          /// Экран ошибки подтверждения отп
          /// todo: текст ошибки подтверждения отп при иных случаях
          newState = newState.failure("Произошла ошибка, попробуйте еще раз");
        }
      },
    );
    emit(newState.setLoading(false));
    emit(newState);
  }

  Future<void> _createApp() async {
    await processUseCase<CreateApplicationModel>(
      () => createApplicationUseCase(
          CreateApplicationUseCaseParams(appParams: params, session: state.sessionId, otp: state.codeValue)),
      onSuccess: (result) {
        updateStateUseCase(RepaymentStatePartial(
            plannedExecutionDate:
                DateFormat("yyyy-MM-dd", "ru_RU").parse(result.repaymentData?.plannedExecutionDate ?? ''),
            monthlyPaymentAmount: result.repaymentData?.monthlyPaymentAmount,
            courtDebt: result.repaymentData?.courtDept,
            loanInterest: result.repaymentData?.loanInterest,
            overdueDept: result.repaymentData?.overdueDept,
            accountBalance: result.repaymentData?.accountBalance,
            replenishmentAmount: result.repaymentData?.replenishmentAmount,
            amount: result.repaymentData?.amount,
            correlationId: result.correlationId,
            isCreateResultScreen: true));
        navigator.navigateToWaitingScreen();
      },
    );
  }

  void _changeCode(ChangeCodeEvent event, Emitter<SmsConfirmState> emit) {
    emit(state.changeCodeValue(event.value).failure(null));
  }

  void _toggleButtonWidget(ToggleRepeatSendButton event, Emitter<SmsConfirmState> emit) {
    emit(state.setRepeatActive(event.isActiveButton));
  }

  /// Возвращает секунды для счетчика
  int _getDateTimeDifference(DateTime? dateUntil) {
    final now = DateTime.now();
    final difference = dateUntil?.difference(now);
    return (difference?.inSeconds) ?? 0;
  }
}

class OTPCommand {
  static const valid = "VALID";
  static const invalid = "INVALID";
  static const delivery = "DELIVERY";
  static const send = "SEND";
  static const wait = "WAIT";
  static const error = "ERROR";
  static const notDelivery = "NOT_DELIVERY";
  static const unknown = "UNKNOWN";
  static const timeOut = "TIMEOUT";
  static const sessionNotFound = "SESSION_NOT_FOUND";
  static const tooManyTimes = "TOO_MANY_TIMES";
}
