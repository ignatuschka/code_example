import 'package:built_value/built_value.dart';
import 'package:flutter_atb_common/usecase/def_state.dart';

part 'sms_confirm_state.g.dart';

abstract class SmsConfirmState with DefState<SmsConfirmState>
    implements Built<SmsConfirmState, SmsConfirmStateBuilder> {
  SmsConfirmState._();

  bool get isSuccess;

  int get failedTry;

  String get codeValue;

  String get sessionId;

  String get title;

  bool get validCodeLength => codeValue.length == 4;

  bool get isRepeatActive;

  int get repeatSeconds;

  factory SmsConfirmState([Function(SmsConfirmStateBuilder b) updates]) = _$SmsConfirmState;

  factory SmsConfirmState.initial({required int repeatSeconds, required String title, required String sessionId}) {
    return SmsConfirmState((b) => b
      ..codeValue = ''
      ..sessionId = sessionId
      ..title = title
      ..failedTry = 0
      ..repeatSeconds = repeatSeconds
      ..isLoading = false
      ..isSuccess = false
      ..isRepeatActive = false);
  }

  SmsConfirmState setSuccess(bool value) => rebuild((b) => b..isSuccess = value);

  SmsConfirmState changeCodeValue(String value) => rebuild((b) => b..codeValue = value);

  SmsConfirmState setRepeatActive(bool value) => rebuild((b) => b..isRepeatActive = value);

  SmsConfirmState setRepeatSeconds(int value) => rebuild((b) => b..repeatSeconds = value);

  SmsConfirmState setSessionId(String value) => rebuild((b) => b..sessionId = value);

  SmsConfirmState setTitle(String value) => rebuild((b) => b..title = value);
}
