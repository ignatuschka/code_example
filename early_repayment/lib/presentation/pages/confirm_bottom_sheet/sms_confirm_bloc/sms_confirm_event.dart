abstract class SmsConfirmEvent {}

/// Отправить код еще раз
class RepeatCodeSmsConfirmEvent extends SmsConfirmEvent {}

/// Подтверждение кода
class ConfirmCodeSmsConfirmEvent extends SmsConfirmEvent {
  final String value;

  ConfirmCodeSmsConfirmEvent({required this.value});
}

/// Изменение строки кода, введенная пользователем
class ChangeCodeEvent extends SmsConfirmEvent {
  final String value;

  ChangeCodeEvent({required this.value});
}

/// Отобразить/скрыть кнопку повторной отправки кода
class ToggleRepeatSendButton extends SmsConfirmEvent {
  final bool isActiveButton;

  ToggleRepeatSendButton({required this.isActiveButton});
}