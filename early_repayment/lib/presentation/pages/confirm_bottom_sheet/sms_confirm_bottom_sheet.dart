import 'package:flutter/material.dart';
import 'package:flutter_atb_common/bloc/bloc_instance_provider.dart';
import 'package:flutter_atb_common/extensions/build_context_extenstions.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ui_kit/theme.dart';
import 'package:ui_kit/ui_kit.dart';
import 'package:verification/resources/verification_text.dart';
import 'package:verification/sms_verification/presentation/timer_widget.dart';

import 'package:early_repayment/presentation/pages/confirm_bottom_sheet/sms_confirm_bloc/sms_confirm_bloc.dart';
import 'package:early_repayment/presentation/pages/confirm_bottom_sheet/sms_confirm_bloc/sms_confirm_event.dart';
import 'package:early_repayment/presentation/pages/confirm_bottom_sheet/sms_confirm_bloc/sms_confirm_state.dart';

class SmsConfirmBottomSheetView extends StatefulWidget {
  const SmsConfirmBottomSheetView({super.key});

  @override
  State<SmsConfirmBottomSheetView> createState() => _SmsConfirmBottomSheetViewState();
}

class _SmsConfirmBottomSheetViewState extends StateWithBLoc<SmsConfirmBloc, SmsConfirmBottomSheetView> {
  @override
  Widget build(BuildContext context) {
    final colors = context.theme.extension<AtbColors>();
    return BlocBuilder<SmsConfirmBloc, SmsConfirmState>(
      builder: (context, state) {
        final isLoading = state.isLoading;
        final bloc = context.read<SmsConfirmBloc>();
        return SafeArea(
          child: Padding(
            padding: EdgeInsets.only(left: 16, top: 4, right: 16, bottom: 24 + context.mediaQueryViewInsets.bottom),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 16, right: 8, top: 20, bottom: 16),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Expanded(
                        child: Text(
                          state.title,
                          softWrap: true,
                          style: context.textTheme.titleMedium?.copyWith(color: colors?.neutral[10]),
                        ),
                      ),
                      const Gap(16),
                      InkWell(
                        highlightColor: Colors.transparent,
                        splashColor: Colors.transparent,
                        hoverColor: Colors.transparent,
                        onTap: Navigator.of(context).pop,
                        child: AtbIcon(
                          assetPath: UiAssets.cross24,
                          width: 24,
                          height: 24,
                          color: colors?.neutral[50],
                        ),
                      ),
                    ],
                  ),
                ),
                AtbTextField(
                  type: TextInputType.number,
                  controller: TextEditingController()
                    ..value = TextEditingValue(
                        text: state.codeValue, selection: TextSelection.collapsed(offset: state.codeValue.length)),
                  onChange: (v) {
                    bloc.add(ChangeCodeEvent(value: v));
                  },
                  errorText: state.errorMessage,
                  labelText: VerificationText.smsLabel,
                  hintText: VerificationText.smsHint,
                ),
                if (state.repeatSeconds != 0)
                  SizedBox(
                    height: 56,
                    child: Padding(
                      padding: const EdgeInsets.only(left: 16),
                      child: state.isRepeatActive
                          ? InkWell(
                              onTap: () => isLoading ? null : bloc.add(RepeatCodeSmsConfirmEvent()),
                              splashColor: Colors.transparent,
                              child: Text(
                                VerificationText.smsSendCodeAgain,
                                style: context.textTheme.titleMedium?.copyWith(color: colors?.primary),
                              ),
                            )
                          : TimerWidget(
                              repeatSecond: state.repeatSeconds,
                              onTimerEnd: () => bloc.add(ToggleRepeatSendButton(isActiveButton: true)),
                            ),
                    ),
                  )
                else
                  const Gap(56),
                AtbButton(
                  onPressed: () => isLoading ? null : bloc.add(ConfirmCodeSmsConfirmEvent(value: state.codeValue)),
                  title: VerificationText.smsConfirm,
                  state: getMainButtonState(state),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}

AtbButtonState getMainButtonState(SmsConfirmState state) {
  if (state.isLoading) {
    return AtbButtonState.loading;
  } else if (state.validCodeLength) {
    return AtbButtonState.active;
  } else {
    return AtbButtonState.disabled;
  }
}
