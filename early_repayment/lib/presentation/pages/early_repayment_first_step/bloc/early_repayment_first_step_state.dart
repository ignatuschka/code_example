import 'package:built_value/built_value.dart';
import 'package:early_repayment/domain/entity/repayment_state.dart';
import 'package:early_repayment/domain/enum/early_repayment_statuses.dart';
import 'package:flutter_atb_common/usecase/def_state.dart';

part 'early_repayment_first_step_state.g.dart';

abstract class EarlyRepaymentFirstStepState
    with DefState<EarlyRepaymentFirstStepState>
    implements Built<EarlyRepaymentFirstStepState, EarlyRepaymentFirstStepStateBuilder> {
  RepaymentState? get repayment;

  bool get needToChoose => repayment?.type == null;

  EarlyRepaymentFirstStepState._();

  factory EarlyRepaymentFirstStepState([void Function(EarlyRepaymentFirstStepStateBuilder)? updates]) =
      _$EarlyRepaymentFirstStepState;

  factory EarlyRepaymentFirstStepState.initial() => EarlyRepaymentFirstStepState((b) => b
    ..repayment = null
    ..isLoading = true);

  EarlyRepaymentFirstStepState setRepayment(RepaymentState repayment) => rebuild((b) => (b)..repayment = repayment);
  EarlyRepaymentFirstStepState setErrorMessage(String? message) => rebuild((b) => (b)..errorMessage = message);
  EarlyRepaymentFirstStepState setType(EarlyRepaymentType type) =>
      rebuild((b) => (b)..repayment = repayment?.copyWith(type: type));
}
