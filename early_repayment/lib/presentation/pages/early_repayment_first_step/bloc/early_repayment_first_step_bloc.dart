import 'package:early_repayment/domain/entity/repayment_state.dart';
import 'package:early_repayment/domain/enum/early_repayment_statuses.dart';
import 'package:early_repayment/domain/usecase/get_state_use_case.dart';
import 'package:early_repayment/domain/usecase/update_state_use_case.dart';
import 'package:early_repayment/navigation/early_repayment_navigator.dart';
import 'package:early_repayment/presentation/pages/early_repayment_first_step/bloc/early_repayment_first_step_event.dart';
import 'package:early_repayment/presentation/pages/early_repayment_first_step/bloc/early_repayment_first_step_state.dart';
import 'package:early_repayment/resources/early_repayment_text.dart';
import 'package:flutter_atb_common/bloc/base_bloc.dart';
import 'package:flutter_atb_common/exception/error_handler.dart';
import 'package:flutter_atb_common/usecase/usecase.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class EarlyRepaymentFirstStepBloc extends BaseBloc<EarlyRepaymentFirstStepEvent, EarlyRepaymentFirstStepState> {
  final GetStateUseCase getStateUseCase;
  final UpdateStateUseCase updateStateUseCase;
  final EarlyRepaymentInternalNavigator navigator;
  EarlyRepaymentFirstStepBloc({
    required this.updateStateUseCase,
    required this.navigator,
    required this.getStateUseCase,
    required ErrorHandler errorHandler,
  }) : super(EarlyRepaymentFirstStepState.initial(), errorHandler: errorHandler) {
    on<EarlyRepaymentFirstStepInitialEvent>(_initialEvent);
    on<ChooseEarlyRepaymentTypeEvent>(_chooseRepaymentType);
    on<TryContinueEvent>(_onTryContinueEvent);

    add(EarlyRepaymentFirstStepInitialEvent());
  }

  Future<void> _initialEvent(
      EarlyRepaymentFirstStepInitialEvent event, Emitter<EarlyRepaymentFirstStepState> emit) async {
    final repayment = getStateUseCase(EmptyUsecaseParams()).result;
    emit(state.setRepayment(repayment).setLoading(false));
  }

  void _chooseRepaymentType(ChooseEarlyRepaymentTypeEvent event, Emitter<EarlyRepaymentFirstStepState> emit) {
    updateStateUseCase(RepaymentStatePartial(type: event.type));
    emit(state.setType(event.type).setErrorMessage(null));
  }

  void _onTryContinueEvent(TryContinueEvent event, Emitter<EarlyRepaymentFirstStepState> emit) {
    if (state.needToChoose) {
      emit(state.setErrorMessage(EarlyRepaymentText.chooseOptionErrorText));
      return;
    }

    if (state.repayment?.type == EarlyRepaymentType.full) {
      navigator.navigateToFullRepaymentSecondStepScreen();
    } else {
      navigator.navigateToPartRepaymentSecondStepScreen();
    }
  }
}
