import 'package:early_repayment/domain/enum/early_repayment_statuses.dart';

abstract class EarlyRepaymentFirstStepEvent {
  const EarlyRepaymentFirstStepEvent();
}

class EarlyRepaymentFirstStepInitialEvent extends EarlyRepaymentFirstStepEvent {
  EarlyRepaymentFirstStepInitialEvent();
}

class ChooseEarlyRepaymentTypeEvent extends EarlyRepaymentFirstStepEvent {
  final EarlyRepaymentType type;
  const ChooseEarlyRepaymentTypeEvent({required this.type});
}

class CheckChooseEvent extends EarlyRepaymentFirstStepEvent {}

class TryContinueEvent extends EarlyRepaymentFirstStepEvent {}
