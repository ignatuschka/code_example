import 'package:early_repayment/domain/enum/early_repayment_statuses.dart';
import 'package:early_repayment/extensions/string_extensions.dart';
import 'package:early_repayment/l10n/early_repayment.localizations.dart';
import 'package:early_repayment/navigation/early_repayment_navigator.dart';
import 'package:early_repayment/presentation/pages/early_repayment_first_step/bloc/early_repayment_first_step_bloc.dart';
import 'package:early_repayment/presentation/pages/early_repayment_first_step/bloc/early_repayment_first_step_event.dart';
import 'package:early_repayment/presentation/pages/early_repayment_first_step/bloc/early_repayment_first_step_state.dart';
import 'package:early_repayment/presentation/widgets/choose_item.dart' as er_choose_item;
import 'package:early_repayment/resources/early_repayment_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_atb_common/bloc/bloc_instance_provider.dart';
import 'package:flutter_atb_common/localization/localized_widget.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ui_kit/ui_kit.dart';

class EarlyRepaymentFirstStepScreen extends StatefulWidget {
  const EarlyRepaymentFirstStepScreen({super.key});

  @override
  State<EarlyRepaymentFirstStepScreen> createState() => _EarlyRepaymentFirstStepScreenState();
}

class _EarlyRepaymentFirstStepScreenState
    extends StateWithBLoc<EarlyRepaymentFirstStepBloc, EarlyRepaymentFirstStepScreen>
    with LocalizedWidget<EarlyRepaymentLocalizations, EarlyRepaymentFirstStepScreen> {
  @override
  EarlyRepaymentLocalizations? getLocalization(BuildContext context) => EarlyRepaymentLocalizations.of(context);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: HeaderInner(
        onBackButtonPressed: () => EarlyRepaymentInternalNavigator.navigateBack(),
        title: EarlyRepaymentText.earlyRepaymentAppBarTitle,
      ),
      body: BlocBuilder<EarlyRepaymentFirstStepBloc, EarlyRepaymentFirstStepState>(builder: (context, state) {
        final repayment = state.repayment;
        if (repayment == null) return const SizedBox();
        return SafeArea(
          child: ConstrainedBox(
            constraints: BoxConstraints(
              minWidth: MediaQuery.of(context).size.width,
              minHeight: MediaQuery.of(context).size.height,
            ),
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16),
              child: Column(
                children: [
                  const StepsBuilder(text: EarlyRepaymentText.stepOneOfThree, progressBarValue: 1 / 3),
                  Form(
                    child: ChooseOption(
                      title: localization.chooseRepaymentType,
                      error: state.errorMessage,
                      showError: state.errorMessage != null,
                      chooseItems: Row(
                        children: [
                          er_choose_item.ChooseItem(
                            value: EarlyRepaymentType.part,
                            groupValue: repayment.type,
                            onChanged: (item) {},
                            onTap: () => bloc.add(const ChooseEarlyRepaymentTypeEvent(type: EarlyRepaymentType.part)),
                            textRadioButton: localization.partRepaymentAppBarTitle,
                            labelMonthyPayment: localization.amountPartRepayment,
                            monthlyPayment: 'от 1 ${repayment.currency}',
                          ),
                          const SizedBox(width: 4),
                          er_choose_item.ChooseItem(
                            value: EarlyRepaymentType.full,
                            groupValue: repayment.type,
                            onChanged: (item) {},
                            onTap: () => bloc.add(const ChooseEarlyRepaymentTypeEvent(type: EarlyRepaymentType.full)),
                            textRadioButton: localization.fullRepaymentAppBarTitle,
                            labelMonthyPayment: localization.debtAmount.split(' ').join('\n'),
                            monthlyPayment: repayment.courtDebt.formatToAmount(repayment.currency),
                          ),
                        ],
                      ),
                    ),
                  ),
                  const Spacer(),
                  AtbButton(
                    title: localization.continueButtonText,
                    size: AtbButtonSize.large,
                    onPressed: () {
                      bloc.add(TryContinueEvent());
                    },
                  )
                ],
              ),
            ),
          ),
        );
      }),
    );
  }
}
