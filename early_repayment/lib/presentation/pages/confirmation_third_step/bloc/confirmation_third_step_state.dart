import 'package:built_value/built_value.dart';
import 'package:early_repayment/domain/entity/repayment_state.dart';
import 'package:flutter_atb_common/usecase/def_state.dart';

part 'confirmation_third_step_state.g.dart';

abstract class ConfirmationThirdStepState
    with DefState<ConfirmationThirdStepState>
    implements Built<ConfirmationThirdStepState, ConfirmationThirdStepStateBuilder> {
  RepaymentState? get appState;
  bool get agreementCheck;
  bool get creditCheck;

  String? get agreementError;
  String? get creditError;

  const ConfirmationThirdStepState._();

  factory ConfirmationThirdStepState([void Function(ConfirmationThirdStepStateBuilder)? updates]) =
      _$ConfirmationThirdStepState;

  factory ConfirmationThirdStepState.initial() => ConfirmationThirdStepState((b) => b
    ..agreementCheck = false
    ..creditCheck = false
    ..appState = null
    ..isLoading = true);

  ConfirmationThirdStepState setAgreement(bool value) => rebuild((b) => (b)..agreementCheck = value);

  ConfirmationThirdStepState setCreditBox(bool value) => rebuild((b) => (b)..creditCheck = value);

  ConfirmationThirdStepState setAppState(RepaymentState? state) => rebuild((b) => (b)..appState = state);

  ConfirmationThirdStepState setAgreementError(String? value) => rebuild((b) => (b)..agreementError = value);
  ConfirmationThirdStepState setCreditError(String? value) => rebuild((b) => (b)..creditError = value);
}
