import 'package:flutter_atb_common/bloc/news.dart';

import 'package:early_repayment/domain/entity/create_application_params.dart';

class SmsVerifyBlocNews extends BlocNews {
  final CreateApplicationParams params;

  final int repeatSeconds;
  final String smsVerificationSessionId;

  SmsVerifyBlocNews({
    required this.params,
    required this.repeatSeconds,
    required this.smsVerificationSessionId,
  });
}
