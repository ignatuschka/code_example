import 'package:early_repayment/domain/entity/create_application_params.dart';
import 'package:early_repayment/domain/entity/otp_code_entity.dart';
import 'package:early_repayment/domain/entity/reduction_type.dart';
import 'package:early_repayment/domain/usecase/create_application_usecase.dart';
import 'package:early_repayment/domain/usecase/get_state_use_case.dart';
import 'package:early_repayment/domain/usecase/repeat_otp_code.dart';
import 'package:early_repayment/domain/usecase/send_otp_code.dart';
import 'package:early_repayment/l10n/early_repayment.localizations.dart';
import 'package:early_repayment/navigation/early_repayment_navigator.dart';
import 'package:early_repayment/presentation/pages/confirmation_third_step/bloc/bloc_news.dart';
import 'package:early_repayment/presentation/pages/confirmation_third_step/bloc/confirmation_third_step_event.dart';
import 'package:early_repayment/presentation/pages/confirmation_third_step/bloc/confirmation_third_step_state.dart';
import 'package:flutter_atb_common/bloc/base_bloc.dart';
import 'package:flutter_atb_common/exception/error_handler.dart';
import 'package:flutter_atb_common/usecase/usecase.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';

class ConfirmationThirdStepBloc extends BaseBloc<ConfirmationThirdStepEvent, ConfirmationThirdStepState> {
  static final _format = DateFormat('yyyy-MM-dd');

  final EarlyRepaymentInternalNavigator navigator;
  final CreateApplicationUseCase createApplicationUseCase;
  final SendOtpCodeUseCase sendOtpCodeUseCase;
  final RepeatOtpCodeUseCase repeatOtpCodeUseCase;
  final GetStateUseCase getStateUseCase;
  final EarlyRepaymentLocalizations? localizations;

  ConfirmationThirdStepBloc({
    this.localizations,
    required ErrorHandler errorHandler,
    required this.getStateUseCase,
    required this.navigator,
    required this.createApplicationUseCase,
    required this.sendOtpCodeUseCase,
    required this.repeatOtpCodeUseCase,
  }) : super(ConfirmationThirdStepState.initial(), errorHandler: errorHandler) {
    on<ConfirmationThirdStepInitialEvent>(_initialEvent);
    on<SendCodeEvent>(_sendCodelEvent);
    on<AgreementCheckboxChanged>(_onAgreementCheckboxChanged);
    on<CreditCheckboxChanged>(_onCreditCheckboxChanged);
    add(ConfirmationThirdStepInitialEvent());
  }

  /// Время, когда можно посылать следующий запрос на отправку отп
  DateTime? _repeatFrom;

  Future<void> _initialEvent(ConfirmationThirdStepEvent event, Emitter<ConfirmationThirdStepState> emit) async {
    final appState = getStateUseCase(EmptyUsecaseParams()).result;

    emit(state.setLoading(false).setAppState(appState));
  }

  void _onAgreementCheckboxChanged(AgreementCheckboxChanged event, Emitter<ConfirmationThirdStepState> emit) {
    emit(state.setAgreement(event.value));
    emit(state.setAgreementError(null));
  }

  void _onCreditCheckboxChanged(CreditCheckboxChanged event, Emitter<ConfirmationThirdStepState> emit) {
    emit(state.setCreditBox(event.value));
    emit(state.setCreditError(null));
  }

  Future<void> _sendCodelEvent(SendCodeEvent event, Emitter<ConfirmationThirdStepState> emit) async {
    var newState = state;
    var result = false;
    if (!state.agreementCheck) {
      newState = newState.setAgreementError(localizations?.errorMessage);
      result = true;
    }
    if (!state.creditCheck) {
      newState = newState.setCreditError(localizations?.errorMessage);
      result = true;
    }
    emit(newState);
    if (result) {
      return;
    }

    // final newState = state;

    

    /// [repeatSeconds] - время для повторной отправки смс
    int? repeatSeconds;
    String? sessionId;
    await processUseCase<OtpCodeEntity>(
      () => sendOtpCodeUseCase(EmptyUsecaseParams()),
      onSuccess: (result) {
        /// Расчет секунд для счетчика
        _repeatFrom = DateTime.tryParse(result.repeatFrom);
        final difference = _repeatFrom?.difference(DateTime.now());
        repeatSeconds = difference?.inSeconds;
        sessionId = result.sessionId;
      },
    );
    emit(newState);

    final appState = state.appState;

    if (appState == null) return;

    /// Подтверждение смс
    if (sessionId != null) {
      addNews(SmsVerifyBlocNews(
        params: CreateApplicationParams(
            amount: appState.amount ?? 0,
            date: _format.format(DateTime.now()),
            productId: appState.productId,
            type: appState.type?.name.toString().toUpperCase() ?? 'FULL',
            withMonthlyPaymentReduction: appState.typeRecalculation == ReductionType.amount),
        repeatSeconds: repeatSeconds as int,
        smsVerificationSessionId: sessionId as String,
      ));
    }
  }
}
