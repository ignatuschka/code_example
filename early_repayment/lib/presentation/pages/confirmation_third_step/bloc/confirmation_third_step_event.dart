abstract class ConfirmationThirdStepEvent {
  const ConfirmationThirdStepEvent();
}

class ConfirmationThirdStepInitialEvent extends ConfirmationThirdStepEvent {}

class SendCodeEvent extends ConfirmationThirdStepEvent {}

class OnChangeCheckBoxEvent extends ConfirmationThirdStepEvent{
  OnChangeCheckBoxEvent({required this.onChangeCheckBox});

  final bool onChangeCheckBox;
}

class CreditCheckboxChanged extends ConfirmationThirdStepEvent {
  CreditCheckboxChanged({required this.value});

  final bool value;
}

class AgreementCheckboxChanged extends ConfirmationThirdStepEvent {
  AgreementCheckboxChanged({required this.value});

  final bool value;
}
