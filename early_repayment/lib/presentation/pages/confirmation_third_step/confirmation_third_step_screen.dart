import 'package:early_repayment/di/early_repayment_di_module.dart';
import 'package:early_repayment/domain/entity/reduction_type.dart';
import 'package:early_repayment/domain/enum/early_repayment_statuses.dart';
import 'package:early_repayment/extensions/string_extensions.dart';
import 'package:early_repayment/l10n/early_repayment.localizations.dart';
import 'package:early_repayment/navigation/early_repayment_navigator.dart';
import 'package:early_repayment/presentation/pages/confirmation_third_step/bloc/bloc_news.dart';
import 'package:early_repayment/presentation/pages/confirmation_third_step/bloc/confirmation_third_step_bloc.dart';
import 'package:early_repayment/presentation/pages/confirmation_third_step/bloc/confirmation_third_step_event.dart';
import 'package:early_repayment/presentation/pages/confirmation_third_step/bloc/confirmation_third_step_state.dart';
import 'package:early_repayment/resources/early_repayment_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_atb_common/bloc/bloc_instance_provider.dart';
import 'package:flutter_atb_common/bloc/news.dart';
import 'package:flutter_atb_common/extensions/build_context_extenstions.dart';
import 'package:flutter_atb_common/localization/localized_widget.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:ui_kit/theme.dart';
import 'package:ui_kit/ui_kit.dart';

final _format = DateFormat('dd.MM.yyyy');

class ConfirmationThirdStepScreen extends StatefulWidget {
  const ConfirmationThirdStepScreen({super.key});

  @override
  State<ConfirmationThirdStepScreen> createState() => _ConfirmationThirdStepScreenState();
}

class _ConfirmationThirdStepScreenState extends StateWithBLoc<ConfirmationThirdStepBloc, ConfirmationThirdStepScreen>
    with LocalizedWidget<EarlyRepaymentLocalizations, ConfirmationThirdStepScreen> {
  @override
  void onNewsReceived(BlocNews news) {
    if (news is SmsVerifyBlocNews) {
      SmsConfirmBottomSheet.showBottomSheet(
        context: context,
        title: EarlyRepaymentText.verifyOTP,
        repeatSeconds: news.repeatSeconds,
        params: news.params,
        smsVerificationSessionId: news.smsVerificationSessionId,
      );
    } else {
      super.onNewsReceived(news);
    }
  }

  @override
  Widget build(BuildContext context) {
    final colors = context.theme.extension<AtbColors>();
    return Scaffold(
      appBar: HeaderInner(
        onBackButtonPressed: () => EarlyRepaymentInternalNavigator.navigateBack(),
        title: localization.confirmationAppBarTitle,
      ),
      body: BlocBuilder<ConfirmationThirdStepBloc, ConfirmationThirdStepState>(builder: (context, state) {
        final appState = state.appState;

        if (state.isLoading || appState == null) return const SizedBox();

        return SafeArea(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16),
            child: Column(
              children: [
                StepsBuilder(text: localization.stepThreeOfThree, progressBarValue: 3 / 3),
                Expanded(
                  child: SingleChildScrollView(
                    child: Column(children: [
                      ...(switch (appState.type) {
                        EarlyRepaymentType.full => [
                            Padding(
                              padding: const EdgeInsets.only(top: 20, bottom: 8),
                              child: Title16(title: localization.fullRepaymentAppBarTitle),
                            ),
                            Display(
                              label: localization.amount,
                              text: appState.totalDebt?.formatToAmount(appState.currency) ?? '',
                              size: DisplaySize.S,
                            ),
                          ],
                        EarlyRepaymentType.part => [
                            Padding(
                              padding: const EdgeInsets.only(top: 20, bottom: 8),
                              child: Title16(title: localization.partEarlyRepayment),
                            ),
                            Display(
                              label: localization.amount,
                              text: appState.amount?.formatToAmount(appState.currency) ?? '',
                              size: DisplaySize.S,
                            ),
                            Display(
                              label: localization.recalculationType,
                              text: switch (appState.typeRecalculation) {
                                ReductionType.amount => localization.reducingMonthlyPayments,
                                ReductionType.duration => localization.reductionLoanTerm,
                                _ => ''
                              },
                              size: DisplaySize.S,
                            ),
                          ],
                        _ => []
                      }),
                      Display(
                        label: localization.executionDate,
                        text: _format.format(DateTime.now()),
                        size: DisplaySize.S,
                      ),
                      ContentCheckbox(
                          hasError: state.creditError != null,
                          errorWidget: Padding(
                            padding: const EdgeInsets.only(top: 4.0),
                            child: Text(state.creditError ?? '', style: context.textTheme.bodyLarge?.copyWith(color: colors?.error)),
                          ),
                          title: Text(
                            localization.firstContentCheckBoxText,
                            style: context.textTheme.bodyLarge,
                          ),
                          onChanged: (value) => bloc.add(CreditCheckboxChanged(value: value ?? false)),
                          value: state.creditCheck),
                      ContentCheckbox(
                          hasError: state.agreementError != null,
                          errorWidget: Padding(
                            padding: const EdgeInsets.only(top: 4.0),
                            child: Text(state.agreementError ?? '',
                                style: context.textTheme.bodyLarge?.copyWith(color: colors?.error)),
                          ),
                          title: Text(
                            localization.secondContentCheckBoxText,
                            style: context.textTheme.bodyLarge,
                          ),
                          onChanged: (value) => bloc.add(AgreementCheckboxChanged(value: value ?? false)),
                          value: state.agreementCheck),
                    ]),
                  ),
                ),
                const Gap(8),
                AtbButton(
                  title: localization.continueButtonText,
                  size: AtbButtonSize.large,
                  onPressed: () {
                    bloc.add(SendCodeEvent());
                  },
                )
              ],
            ),
          ),
        );
      }),
    );
  }

  @override
  EarlyRepaymentLocalizations? getLocalization(BuildContext context) => EarlyRepaymentLocalizations.of(context);
}
