abstract class EarlyRepaymentDetailsScreenEvent {
  const EarlyRepaymentDetailsScreenEvent();
}

class EarlyRepaymentDetailsScreenInitialEvent extends EarlyRepaymentDetailsScreenEvent {}

class OnCancelEvent extends EarlyRepaymentDetailsScreenEvent {}

class OnReplenishEvent extends EarlyRepaymentDetailsScreenEvent {
  double replenishmentAmount;
  OnReplenishEvent({required this.replenishmentAmount});
}
