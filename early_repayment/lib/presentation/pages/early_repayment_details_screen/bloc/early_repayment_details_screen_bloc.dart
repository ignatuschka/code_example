import 'package:accounts/domain/entity/account_entity.dart';
import 'package:accounts/domain/use_case/get_account_by_number_use_case.dart';
import 'package:accounts_transfer/domain/entity/account_transfer_initial_data.dart';
import 'package:accounts_transfer/navigation/accounts_transfer_navigator.dart';
import 'package:early_repayment/domain/entity/application_data_entity.dart';
import 'package:early_repayment/domain/entity/cancel_application_entity.dart';
import 'package:early_repayment/domain/entity/repayment_state.dart';
import 'package:early_repayment/domain/usecase/cancel_application_usecase.dart';
import 'package:early_repayment/domain/usecase/get_application_data_usecase.dart';
import 'package:early_repayment/domain/usecase/get_state_use_case.dart';
import 'package:early_repayment/domain/usecase/update_state_use_case.dart';
import 'package:early_repayment/navigation/early_repayment_navigator.dart';
import 'package:early_repayment/presentation/pages/early_repayment_details_screen/bloc/early_repayment_details_screen_event.dart';
import 'package:early_repayment/presentation/pages/early_repayment_details_screen/bloc/early_repayment_details_screen_state.dart';
import 'package:flutter_atb_common/bloc/base_bloc.dart';
import 'package:flutter_atb_common/exception/error_handler.dart';
import 'package:flutter_atb_common/usecase/usecase.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class EarlyRepaymentDetailsScreenBloc
    extends BaseBloc<EarlyRepaymentDetailsScreenEvent, EarlyRepaymentDetailsScreenState> {
  final GetApplicationDataUseCase applicationDataUseCase;
  final CancelApplicationUseCase cancelApplicationUseCase;
  final AccountsTransferNavigator accountTransferNavigator;
  final GetAccountByNumberUseCase getAccountByNumberUseCase;
  final GetStateUseCase getStateUseCase;
  final EarlyRepaymentNavigator earlyRepaymentNavigator;
  final UpdateStateUseCase updateStateUseCase;

  final String applicationId;

  late final RepaymentState appState;

  EarlyRepaymentDetailsScreenBloc({
    required ErrorHandler errorHandler,
    required this.applicationDataUseCase,
    required this.getStateUseCase,
    required this.cancelApplicationUseCase,
    required this.applicationId,
    required this.accountTransferNavigator,
    required this.getAccountByNumberUseCase,
    required this.earlyRepaymentNavigator,
    required this.updateStateUseCase,
  }) : super(EarlyRepaymentDetailsScreenState.initial(), errorHandler: errorHandler) {
    on<EarlyRepaymentDetailsScreenInitialEvent>(_initialEvent);
    on<OnCancelEvent>((event, emit) => _onCancel(emit));
    on<OnReplenishEvent>((event, emit) => _onReplenish(emit, event.replenishmentAmount));
    add(EarlyRepaymentDetailsScreenInitialEvent());
  }

  Future<void> _initialEvent(
      EarlyRepaymentDetailsScreenEvent event, Emitter<EarlyRepaymentDetailsScreenState> emit) async {
    emit(state.setLoading(true));
    appState = getStateUseCase(EmptyUsecaseParams()).result;
    await processUseCase<ApplicationDataEntity>(
        () => applicationDataUseCase(
            GetApplicationDataUseCaseParams(productId: appState.productId, applicationId: applicationId)),
        onSuccess: (result) {
      emit(state.setApplicationData(result));
    });
    emit(state.setLoading(false));
  }

  Future<void> _onCancel(Emitter<EarlyRepaymentDetailsScreenState> emit) async {
    await processUseCase<CancelApplicationEntity>(
      () => cancelApplicationUseCase(
          CancelApplicationUseCaseParams(productId: appState.productId, applicationId: applicationId)),
      onSuccess: (result) {
        updateStateUseCase(RepaymentStatePartial(correlationId: result.correlationId, isCreateResultScreen: false));
      },
    );
    earlyRepaymentNavigator.navigateToWaitingScreen();
  }

  Future<void> _onReplenish(Emitter<EarlyRepaymentDetailsScreenState> emit, double replenishmentAmount) async {
    AccountTransferInitialData initialData;
    AccountEntity accountEntity;
    await processUseCase<AccountEntity>(() => getAccountByNumberUseCase(appState.accountNumber), onSuccess: (result) {
      accountEntity = result;
      initialData = AccountTransferInitialData(
          accountCredit: accountEntity,
          amountWithOutCurrency: replenishmentAmount.toString(),
          allowChangingAmount: true);
      accountTransferNavigator.navigateToTransferBetweenAccountsScreen(initialData: initialData);
    }, onError: (error) {
      accountTransferNavigator.navigateToTransferBetweenAccountsScreen();
    });
  }
}
