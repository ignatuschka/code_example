import 'package:built_value/built_value.dart';
import 'package:early_repayment/domain/entity/application_data_entity.dart';
import 'package:flutter_atb_common/usecase/def_state.dart';

part 'early_repayment_details_screen_state.g.dart';

abstract class EarlyRepaymentDetailsScreenState
    with DefState<EarlyRepaymentDetailsScreenState>
    implements Built<EarlyRepaymentDetailsScreenState, EarlyRepaymentDetailsScreenStateBuilder> {
      ApplicationDataEntity? get applicationData;

  EarlyRepaymentDetailsScreenState._();

  factory EarlyRepaymentDetailsScreenState([void Function(EarlyRepaymentDetailsScreenStateBuilder)? updates]) =
      _$EarlyRepaymentDetailsScreenState;

  factory EarlyRepaymentDetailsScreenState.initial() => EarlyRepaymentDetailsScreenState((b) => b
    ..applicationData = null
    ..isLoading = true);

  EarlyRepaymentDetailsScreenState setApplicationData(ApplicationDataEntity? applicationData) =>
      rebuild((b) => (b)..applicationData = applicationData);
}
