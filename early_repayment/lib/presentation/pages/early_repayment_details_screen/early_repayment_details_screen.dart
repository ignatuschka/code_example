import 'package:currency/domain/entity/currency.dart';
import 'package:early_repayment/domain/enum/early_repayment_statuses.dart';
import 'package:early_repayment/extensions/string_extensions.dart';
import 'package:early_repayment/l10n/early_repayment.localizations.dart';
import 'package:early_repayment/navigation/early_repayment_navigator.dart';
import 'package:early_repayment/presentation/pages/early_repayment_details_screen/bloc/early_repayment_details_screen_bloc.dart';
import 'package:early_repayment/presentation/pages/early_repayment_details_screen/bloc/early_repayment_details_screen_event.dart';
import 'package:early_repayment/presentation/pages/early_repayment_details_screen/bloc/early_repayment_details_screen_state.dart';
import 'package:early_repayment/presentation/widgets/archive_details_widget.dart';
import 'package:early_repayment/presentation/widgets/not_prosseced_details_widget.dart';
import 'package:early_repayment/resources/early_repayment_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_atb_common/bloc/bloc_instance_provider.dart';
import 'package:flutter_atb_common/localization/localized_widget.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ui/widgets/loading_widget.dart';
import 'package:ui_kit/ui_kit.dart';
import 'package:ui/resources/ui_text.dart' as ui_text;

class EarlyRepaymentDetailsScreen extends StatefulWidget {
  const EarlyRepaymentDetailsScreen({super.key});

  @override
  State<EarlyRepaymentDetailsScreen> createState() => _EarlyRepaymentDetailsScreenState();
}

class _EarlyRepaymentDetailsScreenState
    extends StateWithBLoc<EarlyRepaymentDetailsScreenBloc, EarlyRepaymentDetailsScreen>
    with LocalizedWidget<EarlyRepaymentLocalizations, EarlyRepaymentDetailsScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: HeaderInner(
        onBackButtonPressed: () => EarlyRepaymentInternalNavigator.navigateBack(),
        title: localization.repaymentApplicationAppBarTitle,
      ),
      body: BlocBuilder<EarlyRepaymentDetailsScreenBloc, EarlyRepaymentDetailsScreenState>(builder: (context, state) {
        final appData = state.applicationData;
        if (state.isLoading || appData == null) return const LoadingWidget(title: ui_text.UiText.loading);
        String type = '';
        if (state.applicationData?.type == EarlyRepaymentText.fullEarlyRepaymentType) {
          type = EarlyRepaymentText.full;
        } else if (state.applicationData?.type == EarlyRepaymentText.partEarlyRepaymentType) {
          type = EarlyRepaymentText.partEarly;
        } else {
          type = state.applicationData?.type ?? '';
        }

        return SafeArea(
          child: ConstrainedBox(
            constraints: BoxConstraints(
              minWidth: MediaQuery.of(context).size.width,
              minHeight: MediaQuery.of(context).size.height,
            ),
            child: state.applicationData?.status == ApplicationStatus.notProcessed
                ? NotProcessedDetailWidget(
                    type: type,
                    plannedExecutionDate: state.applicationData?.plannedExecutionDate ?? DateTime.now(),
                    withMonthlyPaymentReduction: state.applicationData?.withMonthlyPaymentReduction ?? false,
                    currency: Currency.rub.character,
                    monthlyPaymentAmount: appData.monthlyPaymentAmount,
                    amount: appData.amount,
                    accountBalance: appData.accountBalance,
                    replenishmentAmount: appData.replenishmentAmount,
                    onTapCancel: () {
                      bloc.add(OnCancelEvent());
                    },
                    onTapReplenish: () {
                      bloc.add(OnReplenishEvent(
                        replenishmentAmount: state.applicationData?.replenishmentAmount ?? 0.0,
                      ));
                    },
                  )
                : ArchiveDetailWidget(
                    kind: state.applicationData?.kind ?? '',
                    number: state.applicationData?.number ?? '',
                    type: type,
                    withMonthlyPaymentReduction: state.applicationData?.withMonthlyPaymentReduction ?? false,
                    amount: state.applicationData?.amount.formatToAmount(Currency.rub.character) ?? '',
                    plannedExecutionDate: state.applicationData?.plannedExecutionDate ?? DateTime.timestamp(),
                    applicationStatus: state.applicationData?.status ?? ApplicationStatus.unknown,
                  ),
          ),
        );
      }),
    );
  }

  @override
  EarlyRepaymentLocalizations? getLocalization(BuildContext context) => EarlyRepaymentLocalizations.of(context);
}
