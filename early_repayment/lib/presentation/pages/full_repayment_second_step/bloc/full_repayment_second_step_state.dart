import 'package:built_value/built_value.dart';
import 'package:early_repayment/domain/entity/repayment_state.dart';
import 'package:flutter_atb_common/usecase/def_state.dart';

part 'full_repayment_second_step_state.g.dart';

abstract class FullRepaymentSecondStepScreenState
    with DefState<FullRepaymentSecondStepScreenState>
    implements Built<FullRepaymentSecondStepScreenState, FullRepaymentSecondStepScreenStateBuilder> {
  RepaymentState? get appState;

  const FullRepaymentSecondStepScreenState._();

  factory FullRepaymentSecondStepScreenState([void Function(FullRepaymentSecondStepScreenStateBuilder)? updates]) =
      _$FullRepaymentSecondStepScreenState;

  factory FullRepaymentSecondStepScreenState.initial() => FullRepaymentSecondStepScreenState((b) => b
    ..appState = null
    ..isLoading = true);

  FullRepaymentSecondStepScreenState setAppState(RepaymentState state) => rebuild((b) => b..appState = state);
}
