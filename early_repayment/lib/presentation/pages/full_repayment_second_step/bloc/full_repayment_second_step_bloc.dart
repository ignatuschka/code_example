import 'dart:async';

import 'package:early_repayment/domain/entity/precomputation_entity.dart';
import 'package:early_repayment/domain/entity/repayment_state.dart';
import 'package:early_repayment/domain/usecase/get_precomputation_usecase.dart';
import 'package:early_repayment/domain/usecase/get_state_use_case.dart';
import 'package:early_repayment/domain/usecase/update_state_use_case.dart';
import 'package:early_repayment/navigation/early_repayment_navigator.dart';
import 'package:early_repayment/presentation/pages/full_repayment_second_step/bloc/full_repayment_second_step_event.dart';
import 'package:early_repayment/presentation/pages/full_repayment_second_step/bloc/full_repayment_second_step_state.dart';
import 'package:flutter_atb_common/bloc/base_bloc.dart';
import 'package:flutter_atb_common/exception/error_handler.dart';
import 'package:flutter_atb_common/usecase/usecase.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class FullRepaymentSecondStepScreenBloc
    extends BaseBloc<FullRepaymentSecondStepScreenEvent, FullRepaymentSecondStepScreenState> {
  final GetPrecomputationUseCase getPrecomputationUseCase;
  final GetStateUseCase getStateUseCase;
  final UpdateStateUseCase updateStateUseCase;
  final EarlyRepaymentInternalNavigator navigator;

  late PrecomputationEntity precomputation;

  FullRepaymentSecondStepScreenBloc({
    required this.updateStateUseCase,
    required this.getStateUseCase,
    required this.getPrecomputationUseCase,
    required this.navigator,
    required ErrorHandler errorHandler,
  }) : super(FullRepaymentSecondStepScreenState.initial(), errorHandler: errorHandler) {
    on<FullRepaymentSecondStepInitialEvent>(_initialEvent);
    on<OnContinueClickedEvent>(_continue);
    add(FullRepaymentSecondStepInitialEvent());
  }

  Future<void> _initialEvent(
      FullRepaymentSecondStepScreenEvent event, Emitter<FullRepaymentSecondStepScreenState> emit) async {
    emit(state.setLoading(true));
    final appState = getStateUseCase(EmptyUsecaseParams()).result;

    final type = appState.type;

    if (type == null) {
      // TODO: по идее надо обработать такой кейс, но его не должно быть вообще
      return;
    }

    await processUseCase<PrecomputationEntity>(
      () => getPrecomputationUseCase(
          GetPrecomputationUseCaseParams(productId: appState.productId, type: type, date: DateTime.now())),
      onSuccess: (result) {
        precomputation = result;

        updateStateUseCase(RepaymentStatePartial(
          amount: precomputation.totalDebt,
          totalDebt: precomputation.totalDebt,
          courtDebt: precomputation.courtDebt,
          loanInterest: precomputation.loanInterest,
          accountBalance: precomputation.accountBalance,
          replenishmentAmount: precomputation.replenishmentAmount,
        ));
        emit(state.setAppState(getStateUseCase(EmptyUsecaseParams()).result).setLoading(false));
      },
    );
  }

  FutureOr<void> _continue(OnContinueClickedEvent event, Emitter<FullRepaymentSecondStepScreenState> emit) {
    navigator.navigateToConfirmationThirdStepScreen();
  }
}
