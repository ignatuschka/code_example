abstract class FullRepaymentSecondStepScreenEvent {
  const FullRepaymentSecondStepScreenEvent();
}

class FullRepaymentSecondStepInitialEvent extends FullRepaymentSecondStepScreenEvent {}

class OnContinueClickedEvent extends FullRepaymentSecondStepScreenEvent{}