import 'package:early_repayment/domain/enum/early_repayment_statuses.dart';
import 'package:early_repayment/extensions/string_extensions.dart';
import 'package:early_repayment/l10n/early_repayment.localizations.dart';
import 'package:early_repayment/navigation/early_repayment_navigator.dart';
import 'package:early_repayment/presentation/pages/full_repayment_second_step/bloc/full_repayment_second_step_bloc.dart';
import 'package:early_repayment/presentation/pages/full_repayment_second_step/bloc/full_repayment_second_step_event.dart';
import 'package:early_repayment/presentation/pages/full_repayment_second_step/bloc/full_repayment_second_step_state.dart';
import 'package:early_repayment/presentation/pages/part_repayment_second_step/part_repayment_second_step_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_atb_common/bloc/bloc_instance_provider.dart';
import 'package:flutter_atb_common/localization/localized_widget.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:ui/widgets/loading_widget.dart';
import 'package:ui/resources/ui_text.dart' as ui_text;
import 'package:ui_kit/ui_kit.dart';

final _format = DateFormat('dd.MM.yyyy');

class RepaymentSecondStepScreen extends StatelessWidget {
  const RepaymentSecondStepScreen({super.key});

  @override
  Widget build(BuildContext context) {
    final state = context.watch<FullRepaymentSecondStepScreenBloc>().state;
    return switch (state.appState?.type) {
      EarlyRepaymentType.full => const FullRepaymentSecondStepScreen(),
      EarlyRepaymentType.part => const PartRepaymentSecondStepScreen(),
      _ => const SizedBox()
    };
  }
}

class FullRepaymentSecondStepScreen extends StatefulWidget {
  const FullRepaymentSecondStepScreen({super.key});

  @override
  State<FullRepaymentSecondStepScreen> createState() => _FullRepaymentSecondStepScreenState();
}

class _FullRepaymentSecondStepScreenState
    extends StateWithBLoc<FullRepaymentSecondStepScreenBloc, FullRepaymentSecondStepScreen>
    with LocalizedWidget<EarlyRepaymentLocalizations, FullRepaymentSecondStepScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: HeaderInner(
        onBackButtonPressed: () => EarlyRepaymentInternalNavigator.navigateBack(),
        title: localization.fullRepaymentAppBarTitle,
      ),
      body:
          BlocBuilder<FullRepaymentSecondStepScreenBloc, FullRepaymentSecondStepScreenState>(builder: (context, state) {
        final appState = state.appState;
        if (state.isLoading || appState == null) return const LoadingWidget(title: ui_text.UiText.loading);
        return SafeArea(
            child: ConstrainedBox(
                constraints: BoxConstraints(
                  minWidth: MediaQuery.of(context).size.width,
                  minHeight: MediaQuery.of(context).size.height,
                ),
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16),
                  child: Column(
                    children: [
                      StepsBuilder(text: localization.stepTwoOfThree, progressBarValue: 2 / 3),
                      Expanded(
                          child: SingleChildScrollView(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Display(
                              label: localization.plannedExecutionDate,
                              text: _format.format(DateTime.now()),
                              size: DisplaySize.S,
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 20, bottom: 8),
                              child: Title16(title: localization.aboutFullRepayment),
                            ),
                            Display(
                              label: localization.amountFullEarlyRepayment,
                              text: '${appState.totalDebt?.formatToAmount(appState.currency)}',
                              size: DisplaySize.S,
                            ),
                            Display(
                              label: localization.mainDebt,
                              text: appState.courtDebt.formatToAmount(appState.currency),
                              size: DisplaySize.S,
                            ),
                            Display(
                              label: localization.creditAgreementRequirements,
                              text: '${appState.loanInterest?.formatToAmount(appState.currency)}',
                              size: DisplaySize.S,
                            ),
                            Display(
                              label: localization.repaymentAccountBalance,
                              text: '${appState.accountBalance?.formatToAmount(appState.currency)}',
                              size: DisplaySize.S,
                            ),
                            Display(
                              label: localization.needToReplenishment,
                              text: '${appState.replenishmentAmount?.formatToAmount(appState.currency)}',
                              size: DisplaySize.S,
                            ),
                          ],
                        ),
                      )),
                      AtbButton(
                        title: localization.continueButtonText,
                        size: AtbButtonSize.large,
                        onPressed: () {
                          bloc.add(OnContinueClickedEvent());
                        },
                      ),
                    ],
                  ),
                )));
      }),
    );
  }

  @override
  EarlyRepaymentLocalizations? getLocalization(BuildContext context) => EarlyRepaymentLocalizations.of(context);
}
