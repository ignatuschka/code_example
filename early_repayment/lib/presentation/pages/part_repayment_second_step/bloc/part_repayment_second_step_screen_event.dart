import 'package:early_repayment/domain/entity/reduction_type.dart';

abstract class PartRepaymentSecondStepScreenEvent {
  const PartRepaymentSecondStepScreenEvent();
}

class PartRepaymentSecondStepInitialEvent extends PartRepaymentSecondStepScreenEvent {}

class SetPaymentTypeEvent extends PartRepaymentSecondStepScreenEvent {
  final ReductionType type;

  SetPaymentTypeEvent({required this.type});
}

class OnChangeQueryAmountEvent extends PartRepaymentSecondStepScreenEvent {
  final String queryAmount;

  OnChangeQueryAmountEvent({required this.queryAmount});
}

class OnNewPrecomputation extends PartRepaymentSecondStepScreenEvent {
  OnNewPrecomputation();
}

class OnContinueCLickedEvent extends PartRepaymentSecondStepScreenEvent {}
