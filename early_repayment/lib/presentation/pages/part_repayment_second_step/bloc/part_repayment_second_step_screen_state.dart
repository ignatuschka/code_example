import 'package:built_value/built_value.dart';
import 'package:early_repayment/domain/entity/reduction_type.dart';
import 'package:early_repayment/domain/entity/repayment_state.dart';
import 'package:flutter_atb_common/usecase/def_state.dart';

part 'part_repayment_second_step_screen_state.g.dart';

abstract class PartRepaymentSecondStepScreenState
    with DefState<PartRepaymentSecondStepScreenState>
    implements Built<PartRepaymentSecondStepScreenState, PartRepaymentSecondStepScreenStateBuilder> {
  RepaymentState? get appState;
  ReductionType? get reductionType;
  String get queryAmount;
  double? get newAmount;
  DateTime? get newDate;
  DateTime? get oldPlanEndDate;
  bool get recalcInProgress;

  const PartRepaymentSecondStepScreenState._();

  factory PartRepaymentSecondStepScreenState([void Function(PartRepaymentSecondStepScreenStateBuilder)? updates]) =
      _$PartRepaymentSecondStepScreenState;

  factory PartRepaymentSecondStepScreenState.initial() => PartRepaymentSecondStepScreenState((b) => b
    ..queryAmount = ''
    ..newAmount = null
    ..newDate = null
    ..reductionType = null
    ..recalcInProgress = false
    ..isLoading = true);

  PartRepaymentSecondStepScreenState setQueryAmount(String queryAmount) =>
      rebuild((b) => (b)..queryAmount = queryAmount);
  PartRepaymentSecondStepScreenState setReductionType(ReductionType type) => rebuild((b) => (b)..reductionType = type);

  PartRepaymentSecondStepScreenState setNewAmount(double? newAmount) => rebuild((b) => (b)..newAmount = newAmount);
  PartRepaymentSecondStepScreenState setAppState(RepaymentState? state) => rebuild((b) => (b)..appState = state);

  PartRepaymentSecondStepScreenState setNewDate(DateTime? newDate) => rebuild((b) => (b)..newDate = newDate);

  PartRepaymentSecondStepScreenState setRecalcInProgress(bool value) => rebuild((b) => (b)..recalcInProgress = value);

  PartRepaymentSecondStepScreenState setOldPlanEndDate(DateTime? value) => rebuild((b) => (b)..oldPlanEndDate = value);

  PartRepaymentSecondStepScreenState setErrorMessage(String? value) => rebuild((b) => (b)..errorMessage = value);
}
