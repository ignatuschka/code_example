import 'dart:async';

import 'package:early_repayment/domain/entity/precomputation_entity.dart';
import 'package:early_repayment/domain/entity/reduction_type.dart';
import 'package:early_repayment/domain/entity/repayment_state.dart';
import 'package:early_repayment/domain/usecase/get_precomputation_usecase.dart';
import 'package:early_repayment/domain/usecase/get_state_use_case.dart';
import 'package:early_repayment/domain/usecase/update_state_use_case.dart';
import 'package:early_repayment/navigation/early_repayment_navigator.dart';
import 'package:early_repayment/presentation/pages/part_repayment_second_step/bloc/part_repayment_second_step_screen_event.dart';
import 'package:early_repayment/presentation/pages/part_repayment_second_step/bloc/part_repayment_second_step_screen_state.dart';
import 'package:flutter_atb_common/bloc/base_bloc.dart';
import 'package:flutter_atb_common/exception/common_exceptions.dart';
import 'package:flutter_atb_common/exception/error_handler.dart';
import 'package:flutter_atb_common/extensions/sum_parser.dart';
import 'package:flutter_atb_common/usecase/usecase.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:rxdart/rxdart.dart';

class PartRepaymentSecondStepScreenBloc
    extends BaseBloc<PartRepaymentSecondStepScreenEvent, PartRepaymentSecondStepScreenState> {
  final GetPrecomputationUseCase getPrecomputationUseCase;
  final EarlyRepaymentInternalNavigator navigator;
  final GetStateUseCase getStateUseCase;
  final UpdateStateUseCase updateStateUseCase;

  PartRepaymentSecondStepScreenBloc({
    required this.getPrecomputationUseCase,
    required this.getStateUseCase,
    required this.updateStateUseCase,
    required this.navigator,
    required ErrorHandler errorHandler,
  }) : super(PartRepaymentSecondStepScreenState.initial(), errorHandler: errorHandler) {
    on<PartRepaymentSecondStepInitialEvent>(_initialEvent);
    on<SetPaymentTypeEvent>(_setPaymentType);
    on<OnChangeQueryAmountEvent>(_changeQueryAmount);
    on<OnNewPrecomputation>(_getNewPrecomputation, transformer: _debounceSequential(const Duration(milliseconds: 500)));
    on<OnContinueCLickedEvent>(_continue);
    add(PartRepaymentSecondStepInitialEvent());
  }

  EventTransformer<Event> _debounceSequential<Event>(Duration duration) {
    return (events, mapper) => events.debounceTime(duration).asyncExpand(mapper);
  }

  Future<void> _initialEvent(
      PartRepaymentSecondStepScreenEvent event, Emitter<PartRepaymentSecondStepScreenState> emit) async {
    emit(state.setLoading(true));
    final appState = getStateUseCase(EmptyUsecaseParams()).result;
    final type = appState.type;
    emit(state.setAppState(appState));
    if (type == null) {
      return;
    }
    await processUseCase<PrecomputationEntity>(
      () => getPrecomputationUseCase(
          GetPrecomputationUseCaseParams(productId: appState.productId, type: type, date: DateTime.now())),
      onSuccess: (result) {
        final precomputation = result;
        updateStateUseCase(RepaymentStatePartial(
          amount: precomputation.courtDebt,
          totalDebt: precomputation.totalDebt,
          courtDebt: precomputation.courtDebt,
          loanInterest: precomputation.loanInterest,
          accountBalance: precomputation.accountBalance,
          replenishmentAmount: precomputation.replenishmentAmount,
          monthlyPaymentAmount: precomputation.monthlyPaymentAmount,
          overdueDept: precomputation.overdueDept,
          loanPaymentDate: precomputation.loanPaymentDate,
        ));
        emit(state.setLoading(false).setOldPlanEndDate(precomputation.oldPlanEndDate));
      },
    );
    _updateAppState(emit);
  }

  void _setPaymentType(SetPaymentTypeEvent event, Emitter<PartRepaymentSecondStepScreenState> emit) {
    emit(state.setReductionType(event.type).setErrorMessage(null));
    updateStateUseCase(RepaymentStatePartial(typeRecalculation: event.type));
    add(OnNewPrecomputation());
  }

  void _changeQueryAmount(OnChangeQueryAmountEvent event, Emitter<PartRepaymentSecondStepScreenState> emit) {
    emit(state.setQueryAmount(event.queryAmount).setErrorMessage(null));
    add(OnNewPrecomputation());
  }

  Future<void> _getNewPrecomputation(
      PartRepaymentSecondStepScreenEvent event, Emitter<PartRepaymentSecondStepScreenState> emit) async {
    double dQueryAmount = state.queryAmount.replaceAll(state.appState?.currency ?? '', '').parseSumToDouble() ?? 0;
    emit(state.setRecalcInProgress(true));
    final appState = getStateUseCase(EmptyUsecaseParams()).result;
    final type = appState.type;
    if (type == null) {
      return;
    }
    await processUseCase<PrecomputationEntity>(
      () => getPrecomputationUseCase(GetPrecomputationUseCaseParams(
          productId: appState.productId,
          type: type,
          date: DateTime.now(),
          withMonthlyPaymentReduction: state.reductionType == ReductionType.amount,
          amount: dQueryAmount)),
      onSuccess: (result) {
        final precomputation = result;

        final double? newAmount = precomputation.newAmount;
        final DateTime? newDate = precomputation.newDate;

        updateStateUseCase(RepaymentStatePartial(
          amount: dQueryAmount,
          totalDebt: precomputation.totalDebt,
          courtDebt: precomputation.courtDebt,
          loanInterest: precomputation.loanInterest,
          accountBalance: precomputation.accountBalance,
          replenishmentAmount: precomputation.replenishmentAmount,
          monthlyPaymentAmount: precomputation.monthlyPaymentAmount,
          loanPaymentDate: precomputation.loanPaymentDate,
        ));

        emit(state
            .setNewAmount(newAmount)
            .setNewDate(newDate)
            .setLoading(false)
            .setOldPlanEndDate(precomputation.oldPlanEndDate));
      },
      onError: (error) {
        if (error is CancelationException) {
        } else {
          handleUseCaseError(error);
        }
      },
    );

    _updateAppState(emit);

    emit(state.setRecalcInProgress(false));
  }

  void _updateAppState(Emitter<PartRepaymentSecondStepScreenState> emit) {
    final appState = getStateUseCase(EmptyUsecaseParams()).result;
    emit(state.setAppState(appState));
  }

  void _continue(OnContinueCLickedEvent event, Emitter<PartRepaymentSecondStepScreenState> emit) {
    if (state.reductionType == null || state.queryAmount.isEmpty) {
      emit(state.setErrorMessage('_ERRRO'));
      return;
    }
    if (state.isLoading || state.recalcInProgress) return;
    navigator.navigateToConfirmationThirdStepScreen();
  }
}
