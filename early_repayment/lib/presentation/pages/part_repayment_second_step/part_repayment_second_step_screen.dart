import 'dart:ui';

import 'package:auto_route/auto_route.dart';
import 'package:early_repayment/domain/entity/reduction_type.dart';

import 'package:early_repayment/extensions/string_extensions.dart';
import 'package:early_repayment/l10n/early_repayment.localizations.dart';
import 'package:early_repayment/navigation/early_repayment_navigator.dart';
import 'package:early_repayment/presentation/pages/part_repayment_second_step/bloc/part_repayment_second_step_screen_bloc.dart';
import 'package:early_repayment/presentation/pages/part_repayment_second_step/bloc/part_repayment_second_step_screen_event.dart';
import 'package:early_repayment/presentation/pages/part_repayment_second_step/bloc/part_repayment_second_step_screen_state.dart';
import 'package:early_repayment/presentation/pages/part_repayment_second_step/formatter.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_atb_common/bloc/bloc_instance_provider.dart';
import 'package:flutter_atb_common/extensions/build_context_extenstions.dart';
import 'package:flutter_atb_common/formatter/amount_formatter.dart';
import 'package:flutter_atb_common/localization/localized_widget.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:ui/widgets/loading_widget.dart';
import 'package:ui/resources/ui_text.dart' as ui_text;
import 'package:ui_kit/theme.dart';
import 'package:ui_kit/ui_kit.dart';

final _format = DateFormat('dd.MM.yyyy');
final _formatMonth = DateFormat('MM.yyyy');

class PartRepaymentSecondStepScreen extends StatefulWidget {
  const PartRepaymentSecondStepScreen({super.key});

  @override
  State<PartRepaymentSecondStepScreen> createState() => _PartRepaymentSecondStepScreenState();
}

class _PartRepaymentSecondStepScreenState
    extends StateWithBLoc<PartRepaymentSecondStepScreenBloc, PartRepaymentSecondStepScreen>
    with LocalizedWidget<EarlyRepaymentLocalizations, PartRepaymentSecondStepScreen>, SingleTickerProviderStateMixin {
  late Ticker _ticker;

  final ValueNotifier<double> _offset = ValueNotifier(0);

  static const Duration _duration = Duration(milliseconds: 2000);

  @override
  void initState() {
    super.initState();

    _ticker = createTicker((elapsed) {
      _offset.value =
          lerpDouble(-1.0, 3.0, (elapsed.inMilliseconds % _duration.inMilliseconds) / _duration.inMilliseconds) ?? 0;
    });

    _ticker.start();
  }

  @override
  void dispose() {
    _ticker.dispose();
    super.dispose();
  }

  void _stopTicker() {
    _ticker.stop();
    _offset.value = -1.0;
  }

  @override
  Widget build(BuildContext context) {
    final colors = context.theme.extension<AtbColors>();
    final textTheme = context.textTheme;
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: HeaderInner(
        onBackButtonPressed: () => EarlyRepaymentInternalNavigator.navigateBack(),
        title: localization.partRepaymentAppBarTitle,
      ),
      body: BlocConsumer<PartRepaymentSecondStepScreenBloc, PartRepaymentSecondStepScreenState>(
          listener: (context, state) {
        if (state.recalcInProgress && !_ticker.isActive) {
          _ticker.start();
        } else {
          _stopTicker();
        }
      }, builder: (context, state) {
        final appState = state.appState;
        if (state.isLoading || appState == null) return const LoadingWidget(title: ui_text.UiText.loading);

        final newDate = state.newDate;
        final hasError = state.errorMessage != null;
        return SafeArea(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16),
            child: Column(
              children: [
                StepsBuilder(text: localization.stepTwoOfThree, progressBarValue: 2 / 3),
                Expanded(
                  child: ListView(
                    children: [
                      const Gap(16),
                      AtbTextField(
                        initialValue: '',
                        maxLength: 17,
                        type: const TextInputType.numberWithOptions(decimal: true),
                        errorText: hasError && state.queryAmount.isEmpty ? localization.needToChooseAmount : null,
                        hintText: '0 ${appState.currency}',
                        labelText: localization.amountEarlyRepayment,
                        textFieldStyleType: AtbTextFieldStyleType.sum,
                        suffixIcon: UiAssets.info24,
                        suffixOnPressed: () => showInfoSheet(),
                        onChange: (String queryAmount) {
                          if (queryAmount.isNotEmpty) {
                            bloc.add(OnChangeQueryAmountEvent(queryAmount: queryAmount));
                          }
                        },
                        inputFormatters: [
                          MaxAmount(maxAmount: state.appState?.courtDebt ?? 0),
                          const AmountFormatter(),
                          SuffixFormatter(suffix: ' ${appState.currency}')
                        ],
                      ),
                      const Gap(32),
                      SelectFormFieldWidget(
                        errorText:
                            hasError && state.reductionType == null ? localization.needToChooseRepaymentType : null,
                        labelText: localization.paymentScheduleRecalcType,
                        hintText: localization.chooseRecalcType,
                        text: state.reductionType?.localize(localization) ?? localization.chooseRecalcType,
                        onTap: () {
                          showRecalculationTypes(context: context);
                        },
                        textStyle: context.theme.textTheme.bodyLarge
                            ?.copyWith(color: state.reductionType == null ? colors?.neutral[50] : colors?.neutral[10]),
                        suffixWidget: const Padding(
                          padding: EdgeInsets.all(10),
                          child: AtbIcon(assetPath: UiAssets.trailingIcon, width: 14, height: 8),
                        ),
                      ),
                      const SizedBox(
                        height: 16,
                      ),
                      Display(
                        label: localization.executionDate,
                        text: _format.format(DateTime.now()),
                        size: DisplaySize.S,
                      ),
                      if (state.reductionType != null)
                        Padding(
                          padding: const EdgeInsets.only(top: 20.0, bottom: 16.0),
                          child: Text(
                              state.reductionType == ReductionType.amount
                                  ? localization.newAmount
                                  : localization.newDuration,
                              style: textTheme.titleMedium),
                        ),
                      Row(
                        children: [
                          ...(switch (state.reductionType) {
                            ReductionType.amount => [
                                Text(
                                  '${appState.monthlyPaymentAmount?.formatToAmount(appState.currency)}',
                                  style: context.theme.textTheme.bodyLarge
                                      ?.copyWith(color: colors?.neutral[50], decoration: TextDecoration.lineThrough),
                                ),
                                const Gap(16),
                                _wrapShader(Text(
                                    '${(state.newAmount ?? appState.monthlyPaymentAmount)?.formatToAmount(appState.currency)}'))
                              ],
                            ReductionType.duration => newDate != null
                                ? [
                                    Text(
                                      '${_formatMonth.format(state.oldPlanEndDate ?? DateTime.now())} г.',
                                      style: context.theme.textTheme.bodyLarge?.copyWith(
                                          color: colors?.neutral[50], decoration: TextDecoration.lineThrough),
                                    ),
                                    const Gap(16),
                                    _wrapShader(Text('${_formatMonth.format(newDate)} г.'))
                                  ]
                                : [],
                            _ => []
                          })
                        ],
                      ),
                      if (state.reductionType != null)
                        Padding(
                          padding: const EdgeInsets.only(top: 8.0),
                          child: Text(
                            switch (state.reductionType) {
                              ReductionType.amount => localization.secondStepTextAmount,
                              ReductionType.duration => localization.secondStepTextDuration,
                              _ => ''
                            },
                            style: context.theme.textTheme.bodyMedium,
                          ),
                        ),
                      Padding(
                        padding: const EdgeInsets.only(top: 20, bottom: 8),
                        child: Title16(title: localization.aboutNextPayment),
                      ),
                      Display(
                        label: localization.paymentDate,
                        text: _format.format(appState.loanPaymentDate ?? DateTime.now()),
                        size: DisplaySize.S,
                      ),
                      CustomDisplay(
                        title: Text(localization.paymentAmount),
                        text: _wrapShader(Text('${appState.monthlyPaymentAmount?.formatToAmount(appState.currency)}')),
                      ),
                      Display(
                        label: localization.overdueDept,
                        text: '${appState.overdueDept?.formatToAmount(appState.currency)}',
                        size: DisplaySize.S,
                      ),
                      if (appState.amount != null)
                        CustomDisplay(
                          title: Text(localization.amountPartEarlyRepayment,
                              style: textTheme.labelMedium?.copyWith(color: colors?.neutral[50])),
                          text: _wrapShader(Text('${appState.amount?.formatToAmount(appState.currency)}',
                              style: textTheme.bodyLarge?.copyWith(color: colors?.neutral[10]))),
                        ),
                      Display(
                        label: localization.accountBalance,
                        text: '${appState.accountBalance?.formatToAmount(appState.currency)}',
                        size: DisplaySize.S,
                      ),
                      CustomDisplay(
                        title: Text(localization.needToReplenishment),
                        text: _wrapShader(Text('${appState.replenishmentAmount?.formatToAmount(appState.currency)}')),
                      ),
                      const Gap(16),
                    ],
                  ),
                ),
                AtbButton(
                  title: localization.continueButtonText,
                  size: AtbButtonSize.large,
                  state: state.recalcInProgress ? AtbButtonState.disabled : AtbButtonState.active,
                  onPressed: () {
                    bloc.add(OnContinueCLickedEvent());
                  },
                )
              ],
            ),
          ),
        );
      }),
    );
  }

  Widget _wrapShader(Widget child) {
    final colors = context.theme.extension<AtbColors>();
    return ValueListenableBuilder(
      valueListenable: _offset,
      child: child,
      builder: (context, value, c) => ShaderMask(
          blendMode: BlendMode.srcATop,
          shaderCallback: (rect) {
            final baseColor = (_ticker.isActive ? colors?.neutral[50] : colors?.neutral[10]) ?? Colors.black;
            final highlightColor = colors?.neutral[90] ?? Colors.white;
            return LinearGradient(colors: <Color>[
              baseColor,
              highlightColor,
              baseColor,
            ]).createShader(rect.shift(Offset(rect.width * value, 0)));
          },
          child: c),
    );
  }

  void showInfoSheet() {
    showModalBottomSheet(
        context: context,
        useRootNavigator: true,
        isDismissible: true,
        builder: (context) => SafeArea(
                child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                TextModalHeader(text: localization.aboutPartRepayment, onClosePressed: Navigator.of(context).pop),
                const Gap(8),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16.0),
                  child: Text(
                    localization.infoText,
                    style: context.textTheme.bodyLarge,
                  ),
                ),
                const Gap(16),
              ],
            )));
  }

  void showRecalculationTypes({required BuildContext context}) {
    showModalBottomSheet(
      context: context,
      useRootNavigator: true,
      isDismissible: true,
      shape: const RoundedRectangleBorder(borderRadius: BorderRadius.vertical(top: Radius.circular(24))),
      builder: (BuildContext context) {
        return BlocProvider.value(
          value: bloc,
          child: BlocBuilder<PartRepaymentSecondStepScreenBloc, PartRepaymentSecondStepScreenState>(
              builder: (context, state) {
            final appState = state.appState;
            if (appState == null) return const SizedBox();
            return SafeArea(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  ModalHeader(
                    title: Text(
                      localization.recalcType,
                      style: context.theme.textTheme.titleLarge,
                    ),
                    onClosePressed: () => AutoRouter.of(context).pop(),
                  ),
                  ...ReductionType.values.map(
                    (e) => ModalListItem(
                      title: e.localize(localization) ?? '',
                      onTap: () {
                        bloc.add(SetPaymentTypeEvent(type: e));
                        AutoRouter.of(context).pop();
                      },
                      checked: state.reductionType == e ? ModalListItemChecked.ON : ModalListItemChecked.OFF,
                    ),
                  ),
                ],
              ),
            );
          }),
        );
      },
    );
  }

  @override
  EarlyRepaymentLocalizations? getLocalization(BuildContext context) => EarlyRepaymentLocalizations.of(context);
}

class CustomDisplay extends StatelessWidget {
  final Widget title;
  final Widget text;

  const CustomDisplay({super.key, required this.title, required this.text});

  @override
  Widget build(BuildContext context) {
    final textTheme = context.textTheme;
    final colors = context.theme.extension<AtbColors>();
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          DefaultTextStyle.merge(style: textTheme.labelMedium?.copyWith(color: colors?.neutral[50]), child: title),
          DefaultTextStyle.merge(style: textTheme.bodyLarge?.copyWith(color: colors?.neutral[10]), child: text)
        ],
      ),
    );
  }
}
