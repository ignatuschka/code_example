import 'dart:math';

import 'package:flutter/services.dart';

class SuffixFormatter extends TextInputFormatter {
  final String? suffix;
  final String? prefix;

  SuffixFormatter({this.suffix, this.prefix});
  @override
  TextEditingValue formatEditUpdate(TextEditingValue oldValue, TextEditingValue newValue) {
    return newValue.copyWith(text: '${prefix ?? ''}${newValue.text}${suffix ?? ''}');
  }
}

class MaxAmount extends TextInputFormatter {
  final double maxAmount;

  MaxAmount({required this.maxAmount});

  @override
  TextEditingValue formatEditUpdate(TextEditingValue oldValue, TextEditingValue newValue) {
    final parsedValue = double.tryParse(newValue.text.replaceAll(RegExp(r'[^\,\d]'), '').replaceAll(',', '.'));
    if (parsedValue == null) {
      return newValue.copyWith(text: '0');
    }

    final newAmount = min(parsedValue, maxAmount);

    var newText = '';
    // Проверяем что у новой суммы нет дробной части(равна 0)
    // Чтобы не добавлять лишние нули в конце
    if (newAmount.truncate() == newAmount) {
      newText = newAmount.toInt().toString();
    } else {
      newText = newAmount.toString();
    }

    return newValue.copyWith(text: newText);
  }
}
