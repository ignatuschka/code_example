import 'package:early_repayment/extensions/string_extensions.dart';
import 'package:early_repayment/l10n/early_repayment.localizations.dart';
import 'package:flutter/material.dart';
import 'package:flutter_atb_common/extensions/build_context_extenstions.dart';
import 'package:flutter_atb_common/localization/localized_widget.dart';
import 'package:intl/intl.dart';
import 'package:ui_kit/theme.dart';
import 'package:ui_kit/ui_kit.dart';

class NotProcessedDetailWidget extends StatefulWidget {
  final String type;
  final DateTime plannedExecutionDate;
  final bool withMonthlyPaymentReduction;
  final double monthlyPaymentAmount;
  final double amount;
  final double accountBalance;
  final double replenishmentAmount;
  final VoidCallback onTapCancel;
  final VoidCallback onTapReplenish;
  final String currency;

  const NotProcessedDetailWidget(
      {super.key,
      required this.type,
      required this.plannedExecutionDate,
      required this.withMonthlyPaymentReduction,
      required this.monthlyPaymentAmount,
      required this.amount,
      required this.accountBalance,
      required this.replenishmentAmount,
      required this.onTapCancel,
      required this.onTapReplenish,
      required this.currency});

  @override
  State<NotProcessedDetailWidget> createState() => _NotProcessedDetailWidgetState();
}

class _NotProcessedDetailWidgetState extends State<NotProcessedDetailWidget>
    with LocalizedWidget<EarlyRepaymentLocalizations, NotProcessedDetailWidget> {
  @override
  Widget build(BuildContext context) {
    final colors = context.theme.extension<AtbColors>();
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: Column(
        children: [
          Expanded(
              child: SingleChildScrollView(
            child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              Display(
                label: localization.repaymentType,
                text: widget.type,
                size: DisplaySize.S,
              ),
              Display(
                label: localization.plannedExecutionDate,
                text: DateFormat('dd.MM.yyyy').format(widget.plannedExecutionDate),
                size: DisplaySize.S,
              ),
              if (widget.type == localization.partEarly)
                Display(
                  label: localization.recalculationType,
                  text: widget.withMonthlyPaymentReduction
                      ? localization.reducingMonthlyPayments
                      : localization.reductionLoanTerm,
                  size: DisplaySize.S,
                ),
              Display(
                label: localization.monthlyPaymentAmount,
                text: widget.monthlyPaymentAmount.formatToAmount(widget.currency),
                size: DisplaySize.S,
              ),
              Display(
                label: widget.type == localization.partEarly
                    ? localization.amountPartEarlyRepayment
                    : localization.amountFullEarlyRepayment,
                text: widget.amount.formatToAmount(widget.currency),
                size: DisplaySize.S,
              ),
              Display(
                label: localization.accountBalance,
                text: widget.accountBalance.formatToAmount(widget.currency),
                size: DisplaySize.S,
              ),
              Display(
                label: localization.needToReplenish,
                text: widget.replenishmentAmount.formatToAmount(widget.currency),
                size: DisplaySize.S,
              ),
              Display(
                label: localization.status,
                text: localization.inProcessing,
                textColor: colors?.tertiary,
                size: DisplaySize.S,
              ),
            ]),
          )),
          if (widget.replenishmentAmount > 0)
            AtbButton(
              title: localization.replenish,
              size: AtbButtonSize.large,
              onPressed: widget.onTapReplenish,
            ),
          const Gap(16),
          AtbButton(
            title: localization.cancel,
            size: AtbButtonSize.large,
            type: AtbButtonType.secondary,
            onPressed: widget.onTapCancel,
          ),
        ],
      ),
    );
  }

  @override
  EarlyRepaymentLocalizations? getLocalization(BuildContext context) => EarlyRepaymentLocalizations.of(context);
}
