import 'package:early_repayment/domain/enum/early_repayment_statuses.dart';
import 'package:early_repayment/resources/early_repayment_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_atb_common/extensions/build_context_extenstions.dart';
import 'package:intl/intl.dart';
import 'package:ui_kit/theme.dart';
import 'package:ui_kit/ui_kit.dart';

class ArchiveDetailWidget extends StatelessWidget {
  final String kind;

  final String number;

  final String type;

  final bool withMonthlyPaymentReduction;

  final String amount;

  final DateTime plannedExecutionDate;

  final ApplicationStatus applicationStatus;

  const ArchiveDetailWidget({
    Key? key,
    required this.kind,
    required this.number,
    required this.type,
    required this.withMonthlyPaymentReduction,
    required this.amount,
    required this.plannedExecutionDate,
    required this.applicationStatus,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final colors = context.theme.extension<AtbColors>();
    final String statusText;
    final MaterialColor? statusTextColor;
    if (applicationStatus == ApplicationStatus.completed) {
      statusText = EarlyRepaymentText.completed;
      statusTextColor = colors?.success;
    } else if (applicationStatus == ApplicationStatus.canceled) {
      statusText = EarlyRepaymentText.canceled;
      statusTextColor = colors?.error;
    } else {
      statusTextColor = null;
      statusText = EarlyRepaymentText.unknown;
    }
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16.0),
        child: Column(
          children: [
            Display(
              label: EarlyRepaymentText.creditKind,
              text: kind,
              size: DisplaySize.S,
            ),
            Display(
              label: EarlyRepaymentText.contractNumber,
              text: number,
              size: DisplaySize.S,
            ),
            Display(
              label: EarlyRepaymentText.repaymentType,
              text: type,
              size: DisplaySize.S,
            ),
            if (type == EarlyRepaymentText.partEarly)
              Display(
                label: EarlyRepaymentText.recalculationType,
                text: withMonthlyPaymentReduction
                    ? EarlyRepaymentText.reducingMonthlyPayments
                    : EarlyRepaymentText.reductionLoanTerm,
                size: DisplaySize.S,
              ),
            Display(
              label: EarlyRepaymentText.appDate,
              text: DateFormat('dd MMMM yyyy', 'ru_RU').format(plannedExecutionDate),
              size: DisplaySize.S,
            ),
            Display(
              label: EarlyRepaymentText.amountRepayment,
              text: amount,
              size: DisplaySize.S,
            ),
            Display(
              label: EarlyRepaymentText.executionDate,
              text: DateFormat('dd.MM.yyyy').format(plannedExecutionDate),
              size: DisplaySize.S,
            ),
            Display(
              label: EarlyRepaymentText.status,
              text: statusText,
              textColor: statusTextColor,
              size: DisplaySize.S,
            ),
          ],
        ),
      ),
    );
  }
}
