import 'package:flutter/material.dart';
import 'package:ui_kit/theme.dart';
import 'package:ui_kit/ui_kit.dart';

class ChooseOption extends StatelessWidget {
  const ChooseOption({
    super.key,
    required this.title,
    this.error,
    this.showError = false,
    required this.chooseItems,
  });

  /// заголовок виджета
  final String title;

  /// текст ошибки
  final String? error;

  /// булеан для отображения ошибки
  final bool showError;

  /// виджет варианты для выбора
  final Widget chooseItems;

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;
    final colors = Theme.of(context).extension<AtbColors>();
    if (colors == null) {
      throw Exception('colors not set');
    }

    final border = Border.all(color: showError ? colors.error : colors.surface);

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          title,
          style: textTheme.titleMedium,
        ),
        const Gap(8),
        AnimatedContainer(
          duration: const Duration(milliseconds: 100),
          decoration: BoxDecoration(
            color: colors.surface,
            border: border,
            borderRadius: BorderRadius.circular(16.0),
          ),
          child: chooseItems,
        ),
        const Gap(4),
        if ((error != null) & (showError == true))
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 32.0),
            child: Text(
              error ?? '',
              style: textTheme.labelMedium?.copyWith(color: colors.error),
            ),
          ),
      ],
    );
  }
}
