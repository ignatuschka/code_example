import 'package:flutter/material.dart';
import 'package:ui_kit/theme.dart';
import 'package:ui_kit/ui_kit.dart';

class ChooseItem<T> extends StatelessWidget {
  ///виджет для выбора опции в кредитах
  const ChooseItem({
    super.key,
    this.labelRadioButton,
    required this.labelMonthyPayment,
    this.rateLabel,
    required this.monthlyPayment,
    this.rate,
    required this.value,
    required this.groupValue,
    required this.onChanged,
    this.onTap,
    required this.textRadioButton,
  });

  /// заголовок виджета
  final String? labelRadioButton;

  /// название опции
  final String textRadioButton;

  /// значение, представляемое этим виджетом
  final T value;

  /// текущее выбранное значение
  final T? groupValue;

  /// callback для [AtbRadio]
  final ValueChanged<T?> onChanged;

  /// callback для контейнера
  final GestureTapCallback? onTap;

  /// label виджета для отображения ежемесячного платежа
  final String labelMonthyPayment;

  /// подзаголовок отображающий ежемесячный платеж
  final String monthlyPayment;

  /// label виджета для отображения ставки
  final String? rateLabel;

  /// подзаголовок отображающий проценты по кредиту
  final String? rate;

  @override
  Widget build(BuildContext context) {
    final colors = Theme.of(context).extension<AtbColors>();
    if (colors == null) {
      throw Exception('colors not set');
    }
    final textTheme = Theme.of(context).textTheme;
    final chosen = value == groupValue;
    final border = chosen ? Border.all(color: colors.primary) : Border.all(color: colors.surface);
    List<BoxShadow>? boxShadow;

    boxShadow = groupValue == null
        ? const [
            BoxShadow(
              offset: Offset(0, 4),
              blurRadius: 17,
              spreadRadius: 3,
              color: Colors.black12,
            ),
          ]
        : chosen
            ? <BoxShadow>[
                BoxShadow(
                  color: colors.primary.withOpacity(0.25),
                  offset: const Offset(0.0, 4.0),
                  blurRadius: 17.0,
                  spreadRadius: 3,
                )
              ]
            : null;

    return Flexible(
      child: Material(
        type: MaterialType.transparency,
        child: InkWell(
          onTap: onTap,
          borderRadius: BorderRadius.circular(16.0),
          child: AnimatedContainer(
            duration: const Duration(milliseconds: 100),
            decoration: BoxDecoration(
              color: colors.surface,
              border: border,
              boxShadow: boxShadow,
              borderRadius: BorderRadius.circular(16.0),
            ),
            padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 24.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                if (labelRadioButton != null)
                  Text(
                    labelRadioButton ?? '',
                    style: textTheme.bodyMedium?.copyWith(color: colors.neutral[50]),
                  ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    AtbRadio<T>(
                      materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                      visualDensity: VisualDensity.compact,
                      activeColor: colors.primary,
                      onChanged: onChanged,
                      value: value,
                      groupValue: groupValue,
                    ),
                    const Gap(8),
                    Flexible(
                      child: Text(
                        textRadioButton,
                        style: textTheme.bodyLarge?.copyWith(color: colors.neutral[10]),
                      ),
                    )
                  ],
                ),
                const Gap(16),
                Text(
                  labelMonthyPayment,
                  style: textTheme.bodyMedium?.copyWith(color: colors.neutral[50]),
                ),
                const Gap(4),
                Text(monthlyPayment, style: textTheme.titleLarge?.copyWith(color: colors.primary)),
                const Gap(4),
                if (rateLabel != null)
                  Text(
                    rateLabel ?? '',
                    style: textTheme.bodyMedium?.copyWith(color: colors.neutral[50]),
                  ),
                if (rate != null)
                  Text(
                    rate ?? '',
                    style: textTheme.titleLarge?.copyWith(color: colors.neutral[10]),
                  ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
