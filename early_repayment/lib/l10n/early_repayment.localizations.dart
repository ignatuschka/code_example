import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:intl/intl.dart' as intl;

import 'early_repayment_ru.localizations.dart';

/// Callers can lookup localized strings with an instance of EarlyRepaymentLocalizations
/// returned by `EarlyRepaymentLocalizations.of(context)`.
///
/// Applications need to include `EarlyRepaymentLocalizations.delegate()` in their app's
/// `localizationDelegates` list, and the locales they support in the app's
/// `supportedLocales` list. For example:
///
/// ```dart
/// import 'l10n/early_repayment.localizations.dart';
///
/// return MaterialApp(
///   localizationsDelegates: EarlyRepaymentLocalizations.localizationsDelegates,
///   supportedLocales: EarlyRepaymentLocalizations.supportedLocales,
///   home: MyApplicationHome(),
/// );
/// ```
///
/// ## Update pubspec.yaml
///
/// Please make sure to update your pubspec.yaml to include the following
/// packages:
///
/// ```yaml
/// dependencies:
///   # Internationalization support.
///   flutter_localizations:
///     sdk: flutter
///   intl: any # Use the pinned version from flutter_localizations
///
///   # Rest of dependencies
/// ```
///
/// ## iOS Applications
///
/// iOS applications define key application metadata, including supported
/// locales, in an Info.plist file that is built into the application bundle.
/// To configure the locales supported by your app, you’ll need to edit this
/// file.
///
/// First, open your project’s ios/Runner.xcworkspace Xcode workspace file.
/// Then, in the Project Navigator, open the Info.plist file under the Runner
/// project’s Runner folder.
///
/// Next, select the Information Property List item, select Add Item from the
/// Editor menu, then select Localizations from the pop-up menu.
///
/// Select and expand the newly-created Localizations item then, for each
/// locale your application supports, add a new item and select the locale
/// you wish to add from the pop-up menu in the Value field. This list should
/// be consistent with the languages listed in the EarlyRepaymentLocalizations.supportedLocales
/// property.
abstract class EarlyRepaymentLocalizations {
  EarlyRepaymentLocalizations(String locale) : localeName = intl.Intl.canonicalizedLocale(locale.toString());

  final String localeName;

  static EarlyRepaymentLocalizations? of(BuildContext context) {
    return Localizations.of<EarlyRepaymentLocalizations>(context, EarlyRepaymentLocalizations);
  }

  static const LocalizationsDelegate<EarlyRepaymentLocalizations> delegate = _EarlyRepaymentLocalizationsDelegate();

  /// A list of this localizations delegate along with the default localizations
  /// delegates.
  ///
  /// Returns a list of localizations delegates containing this delegate along with
  /// GlobalMaterialLocalizations.delegate, GlobalCupertinoLocalizations.delegate,
  /// and GlobalWidgetsLocalizations.delegate.
  ///
  /// Additional delegates can be added by appending to this list in
  /// MaterialApp. This list does not have to be used at all if a custom list
  /// of delegates is preferred or required.
  static const List<LocalizationsDelegate<dynamic>> localizationsDelegates = <LocalizationsDelegate<dynamic>>[
    delegate,
    GlobalMaterialLocalizations.delegate,
    GlobalCupertinoLocalizations.delegate,
    GlobalWidgetsLocalizations.delegate,
  ];

  /// A list of this localizations delegate's supported locales.
  static const List<Locale> supportedLocales = <Locale>[
    Locale('ru')
  ];

  /// No description provided for @earlyRepaymentAppBarTitle.
  ///
  /// In ru, this message translates to:
  /// **'Досрочное погашение'**
  String get earlyRepaymentAppBarTitle;

  /// No description provided for @partRepaymentAppBarTitle.
  ///
  /// In ru, this message translates to:
  /// **'Частичное погашение'**
  String get partRepaymentAppBarTitle;

  /// No description provided for @fullRepaymentAppBarTitle.
  ///
  /// In ru, this message translates to:
  /// **'Полное погашение'**
  String get fullRepaymentAppBarTitle;

  /// No description provided for @full.
  ///
  /// In ru, this message translates to:
  /// **'Полное'**
  String get full;

  /// No description provided for @repaymentApplicationAppBarTitle.
  ///
  /// In ru, this message translates to:
  /// **'Заявка на погашение'**
  String get repaymentApplicationAppBarTitle;

  /// No description provided for @apply.
  ///
  /// In ru, this message translates to:
  /// **'Подать заявку'**
  String get apply;

  /// No description provided for @currentApplication.
  ///
  /// In ru, this message translates to:
  /// **'Текущая заявка'**
  String get currentApplication;

  /// No description provided for @applicationArchive.
  ///
  /// In ru, this message translates to:
  /// **'Архив заявок'**
  String get applicationArchive;

  /// No description provided for @repaymentType.
  ///
  /// In ru, this message translates to:
  /// **'Тип погашения'**
  String get repaymentType;

  /// No description provided for @executionDate.
  ///
  /// In ru, this message translates to:
  /// **'Срок исполнения'**
  String get executionDate;

  /// No description provided for @plannedExecutionDate.
  ///
  /// In ru, this message translates to:
  /// **'Дата исполнения'**
  String get plannedExecutionDate;

  /// No description provided for @recalculationType.
  ///
  /// In ru, this message translates to:
  /// **'Тип перерасчета графика платежей'**
  String get recalculationType;

  /// No description provided for @reducingMonthlyPayments.
  ///
  /// In ru, this message translates to:
  /// **'Сокращение суммы ежемесячных платежей'**
  String get reducingMonthlyPayments;

  /// No description provided for @reductionLoanTerm.
  ///
  /// In ru, this message translates to:
  /// **'Сокращение срока кредита'**
  String get reductionLoanTerm;

  /// No description provided for @monthlyPaymentAmount.
  ///
  /// In ru, this message translates to:
  /// **'Сумма ежемесячного платежа'**
  String get monthlyPaymentAmount;

  /// No description provided for @amountPartEarlyRepayment.
  ///
  /// In ru, this message translates to:
  /// **'Сумма частично-досрочного погашения'**
  String get amountPartEarlyRepayment;

  /// No description provided for @amountEarlyRepayment.
  ///
  /// In ru, this message translates to:
  /// **'Сумма досрочного погашения'**
  String get amountEarlyRepayment;

  /// No description provided for @amountPartRepayment.
  ///
  /// In ru, this message translates to:
  /// **'Сумма частичного погашения'**
  String get amountPartRepayment;

  /// No description provided for @amountFullEarlyRepayment.
  ///
  /// In ru, this message translates to:
  /// **'Сумма полного досрочного погашения'**
  String get amountFullEarlyRepayment;

  /// No description provided for @paymentAmount.
  ///
  /// In ru, this message translates to:
  /// **'Сумма платежа'**
  String get paymentAmount;

  /// No description provided for @partEarly.
  ///
  /// In ru, this message translates to:
  /// **'Частично-досрочное'**
  String get partEarly;

  /// No description provided for @partEarlyRepayment.
  ///
  /// In ru, this message translates to:
  /// **'Частично-досрочное погашение'**
  String get partEarlyRepayment;

  /// No description provided for @accountBalance.
  ///
  /// In ru, this message translates to:
  /// **'Остаток на счете погашения'**
  String get accountBalance;

  /// No description provided for @needToReplenish.
  ///
  /// In ru, this message translates to:
  /// **'Необходимо пополнить'**
  String get needToReplenish;

  /// No description provided for @needToReplenishment.
  ///
  /// In ru, this message translates to:
  /// **'Необходимо внести'**
  String get needToReplenishment;

  /// No description provided for @inProcessing.
  ///
  /// In ru, this message translates to:
  /// **'В обработке'**
  String get inProcessing;

  /// No description provided for @completed.
  ///
  /// In ru, this message translates to:
  /// **'Исполнена'**
  String get completed;

  /// No description provided for @canceled.
  ///
  /// In ru, this message translates to:
  /// **'Аннулирована'**
  String get canceled;

  /// No description provided for @status.
  ///
  /// In ru, this message translates to:
  /// **'Статус'**
  String get status;

  /// No description provided for @replenish.
  ///
  /// In ru, this message translates to:
  /// **'Пополнить'**
  String get replenish;

  /// No description provided for @cancel.
  ///
  /// In ru, this message translates to:
  /// **'Удалить'**
  String get cancel;

  /// No description provided for @creditKind.
  ///
  /// In ru, this message translates to:
  /// **'Тип кредита'**
  String get creditKind;

  /// No description provided for @contractNumber.
  ///
  /// In ru, this message translates to:
  /// **'Номер договора'**
  String get contractNumber;

  /// No description provided for @appDate.
  ///
  /// In ru, this message translates to:
  /// **'Дата заявки'**
  String get appDate;

  /// No description provided for @amountRepayment.
  ///
  /// In ru, this message translates to:
  /// **'Сумма погашения'**
  String get amountRepayment;

  /// No description provided for @unknown.
  ///
  /// In ru, this message translates to:
  /// **'Неизвестно'**
  String get unknown;

  /// No description provided for @stepOneOfThree.
  ///
  /// In ru, this message translates to:
  /// **'Шаг 1 из 3'**
  String get stepOneOfThree;

  /// No description provided for @stepTwoOfThree.
  ///
  /// In ru, this message translates to:
  /// **'Шаг 2 из 3'**
  String get stepTwoOfThree;

  /// No description provided for @stepThreeOfThree.
  ///
  /// In ru, this message translates to:
  /// **'Шаг 3 из 3'**
  String get stepThreeOfThree;

  /// No description provided for @chooseRepaymentType.
  ///
  /// In ru, this message translates to:
  /// **'Выберите тип погашения'**
  String get chooseRepaymentType;

  /// No description provided for @debtAmount.
  ///
  /// In ru, this message translates to:
  /// **'Сумма задолженности'**
  String get debtAmount;

  /// No description provided for @continueButtonText.
  ///
  /// In ru, this message translates to:
  /// **'Продолжить'**
  String get continueButtonText;

  /// No description provided for @chooseOptionErrorText.
  ///
  /// In ru, this message translates to:
  /// **'Необходимо выбрать тип погашения'**
  String get chooseOptionErrorText;

  /// No description provided for @aboutFullRepayment.
  ///
  /// In ru, this message translates to:
  /// **'О полном погашении'**
  String get aboutFullRepayment;

  /// No description provided for @aboutFullRepaymentCredit.
  ///
  /// In ru, this message translates to:
  /// **'О полном погашении кредита'**
  String get aboutFullRepaymentCredit;

  /// No description provided for @mainDebt.
  ///
  /// In ru, this message translates to:
  /// **'Основной долг'**
  String get mainDebt;

  /// No description provided for @creditAgreementRequirements.
  ///
  /// In ru, this message translates to:
  /// **'Требования по кредитному договору'**
  String get creditAgreementRequirements;

  /// No description provided for @repaymentAccountBalance.
  ///
  /// In ru, this message translates to:
  /// **'Остаток на счете погашения'**
  String get repaymentAccountBalance;

  /// No description provided for @confirmationAppBarTitle.
  ///
  /// In ru, this message translates to:
  /// **'Подтверждение'**
  String get confirmationAppBarTitle;

  /// No description provided for @firstContentCheckBoxText.
  ///
  /// In ru, this message translates to:
  /// **'Настоящим прошу Банк внести изменения в график погашения по вышеуказанному Кредитному договору'**
  String get firstContentCheckBoxText;

  /// No description provided for @secondContentCheckBoxText.
  ///
  /// In ru, this message translates to:
  /// **'Подтверждаю и согласен(а), с условием обеспечить на моем счете наличие суммы достаточной для исполнения заявки, иначе Банком будет списан имеющийся на счете остаток, а при отсутствии средств данная заявка будет аннулирована в дату наступления срока ее исполнения'**
  String get secondContentCheckBoxText;

  /// No description provided for @needToChooseRepaymentType.
  ///
  /// In ru, this message translates to:
  /// **'Необходимо выбрать тип перерасчета'**
  String get needToChooseRepaymentType;

  /// No description provided for @needToChooseAmount.
  ///
  /// In ru, this message translates to:
  /// **'Необходимо ввести сумму досрочного гашения'**
  String get needToChooseAmount;

  /// No description provided for @paymentScheduleRecalcType.
  ///
  /// In ru, this message translates to:
  /// **'Тип перерасчета графика платежей'**
  String get paymentScheduleRecalcType;

  /// No description provided for @chooseRecalcType.
  ///
  /// In ru, this message translates to:
  /// **'Выберите тип перерасчета'**
  String get chooseRecalcType;

  /// No description provided for @recalcType.
  ///
  /// In ru, this message translates to:
  /// **'Тип перерасчета'**
  String get recalcType;

  /// No description provided for @aboutNextPayment.
  ///
  /// In ru, this message translates to:
  /// **'О ближайшем платеже'**
  String get aboutNextPayment;

  /// No description provided for @overdueDept.
  ///
  /// In ru, this message translates to:
  /// **'Просроченная задолженность'**
  String get overdueDept;

  /// No description provided for @secondStepTextAmount.
  ///
  /// In ru, this message translates to:
  /// **'Расчет является предварительным на основе полученных данных по договору. Окончательная сумма ежемесячного платежа будет рассчитана после исполнения заявки на частично-досрочное погашение кредита.'**
  String get secondStepTextAmount;

  /// No description provided for @secondStepTextDuration.
  ///
  /// In ru, this message translates to:
  /// **'Расчет является предварительным на основе полученных данных по договору. Окончательная дата будет рассчитана после исполнения заявки на частично-досрочное погашение кредита.'**
  String get secondStepTextDuration;

  /// No description provided for @applicationAccepted.
  ///
  /// In ru, this message translates to:
  /// **'Заявка принята'**
  String get applicationAccepted;

  /// No description provided for @applicationNotAccepted.
  ///
  /// In ru, this message translates to:
  /// **'Заявка не зарегистрирована'**
  String get applicationNotAccepted;

  /// No description provided for @applicationErrorMessage.
  ///
  /// In ru, this message translates to:
  /// **'Произошла ошибка.\nОтправьте номер ошибки в чат поддержки и оператор поможет завершить регистрацию'**
  String get applicationErrorMessage;

  /// No description provided for @backToMain.
  ///
  /// In ru, this message translates to:
  /// **'На главную'**
  String get backToMain;

  /// No description provided for @paymentDate.
  ///
  /// In ru, this message translates to:
  /// **'Дата платежа'**
  String get paymentDate;

  /// No description provided for @amount.
  ///
  /// In ru, this message translates to:
  /// **'Сумма'**
  String get amount;

  /// No description provided for @amountResult.
  ///
  /// In ru, this message translates to:
  /// **'Новая сумма ежемесячного платежа будет размещена в графике платежей после исполнения заявки'**
  String get amountResult;

  /// No description provided for @durationResult.
  ///
  /// In ru, this message translates to:
  /// **'Новая дата закрытия договора будет размещена в графике платежей после исполнения заявки'**
  String get durationResult;

  /// No description provided for @cancelResultTitle.
  ///
  /// In ru, this message translates to:
  /// **'Ваша заявка на досрочное погашение\nбудет аннулирована'**
  String get cancelResultTitle;

  /// No description provided for @cancelResultText.
  ///
  /// In ru, this message translates to:
  /// **'Статус заявки будет доступен\nв мобильном приложении'**
  String get cancelResultText;

  /// No description provided for @getHelp.
  ///
  /// In ru, this message translates to:
  /// **'Обратиться в чат'**
  String get getHelp;

  /// No description provided for @notAvailable.
  ///
  /// In ru, this message translates to:
  /// **'Сервис частичного погашения недоступен по кредитному договору.'**
  String get notAvailable;

  /// No description provided for @errorMessage.
  ///
  /// In ru, this message translates to:
  /// **'Ваше подтверждение обязательно'**
  String get errorMessage;

  /// No description provided for @tryLater.
  ///
  /// In ru, this message translates to:
  /// **'Попробуйте позже'**
  String get tryLater;

  /// No description provided for @newAmount.
  ///
  /// In ru, this message translates to:
  /// **'Новая сумма ежемесячного платежа'**
  String get newAmount;

  /// No description provided for @newDuration.
  ///
  /// In ru, this message translates to:
  /// **'Новый срок закрытия договора'**
  String get newDuration;

  /// No description provided for @aboutPartRepayment.
  ///
  /// In ru, this message translates to:
  /// **'О частичном погашении'**
  String get aboutPartRepayment;

  /// No description provided for @goToChat.
  ///
  /// In ru, this message translates to:
  /// **'Открыть чат'**
  String get goToChat;

  /// No description provided for @infoText.
  ///
  /// In ru, this message translates to:
  /// **'К дате исполнения досрочного погашения необходимо обеспечить на счете списания сумму, указанную в заявке. Для погашение ежемесячного платежа сумма средств вносится дополнительно.'**
  String get infoText;
}

class _EarlyRepaymentLocalizationsDelegate extends LocalizationsDelegate<EarlyRepaymentLocalizations> {
  const _EarlyRepaymentLocalizationsDelegate();

  @override
  Future<EarlyRepaymentLocalizations> load(Locale locale) {
    return SynchronousFuture<EarlyRepaymentLocalizations>(lookupEarlyRepaymentLocalizations(locale));
  }

  @override
  bool isSupported(Locale locale) => <String>['ru'].contains(locale.languageCode);

  @override
  bool shouldReload(_EarlyRepaymentLocalizationsDelegate old) => false;
}

EarlyRepaymentLocalizations lookupEarlyRepaymentLocalizations(Locale locale) {


  // Lookup logic when only language code is specified.
  switch (locale.languageCode) {
    case 'ru': return EarlyRepaymentLocalizationsRu();
  }

  throw FlutterError(
    'EarlyRepaymentLocalizations.delegate failed to load unsupported locale "$locale". This is likely '
    'an issue with the localizations generation tool. Please file an issue '
    'on GitHub with a reproducible sample app and the gen-l10n configuration '
    'that was used.'
  );
}
