import 'early_repayment.localizations.dart';

/// The translations for Russian (`ru`).
class EarlyRepaymentLocalizationsRu extends EarlyRepaymentLocalizations {
  EarlyRepaymentLocalizationsRu([String locale = 'ru']) : super(locale);

  @override
  String get earlyRepaymentAppBarTitle => 'Досрочное погашение';

  @override
  String get partRepaymentAppBarTitle => 'Частичное погашение';

  @override
  String get fullRepaymentAppBarTitle => 'Полное погашение';

  @override
  String get full => 'Полное';

  @override
  String get repaymentApplicationAppBarTitle => 'Заявка на погашение';

  @override
  String get apply => 'Подать заявку';

  @override
  String get currentApplication => 'Текущая заявка';

  @override
  String get applicationArchive => 'Архив заявок';

  @override
  String get repaymentType => 'Тип погашения';

  @override
  String get executionDate => 'Срок исполнения';

  @override
  String get plannedExecutionDate => 'Дата исполнения';

  @override
  String get recalculationType => 'Тип перерасчета графика платежей';

  @override
  String get reducingMonthlyPayments => 'Сокращение суммы ежемесячных платежей';

  @override
  String get reductionLoanTerm => 'Сокращение срока кредита';

  @override
  String get monthlyPaymentAmount => 'Сумма ежемесячного платежа';

  @override
  String get amountPartEarlyRepayment => 'Сумма частично-досрочного погашения';

  @override
  String get amountEarlyRepayment => 'Сумма досрочного погашения';

  @override
  String get amountPartRepayment => 'Сумма частичного погашения';

  @override
  String get amountFullEarlyRepayment => 'Сумма полного досрочного погашения';

  @override
  String get paymentAmount => 'Сумма платежа';

  @override
  String get partEarly => 'Частично-досрочное';

  @override
  String get partEarlyRepayment => 'Частично-досрочное погашение';

  @override
  String get accountBalance => 'Остаток на счете погашения';

  @override
  String get needToReplenish => 'Необходимо пополнить';

  @override
  String get needToReplenishment => 'Необходимо внести';

  @override
  String get inProcessing => 'В обработке';

  @override
  String get completed => 'Исполнена';

  @override
  String get canceled => 'Аннулирована';

  @override
  String get status => 'Статус';

  @override
  String get replenish => 'Пополнить';

  @override
  String get cancel => 'Удалить';

  @override
  String get creditKind => 'Тип кредита';

  @override
  String get contractNumber => 'Номер договора';

  @override
  String get appDate => 'Дата заявки';

  @override
  String get amountRepayment => 'Сумма погашения';

  @override
  String get unknown => 'Неизвестно';

  @override
  String get stepOneOfThree => 'Шаг 1 из 3';

  @override
  String get stepTwoOfThree => 'Шаг 2 из 3';

  @override
  String get stepThreeOfThree => 'Шаг 3 из 3';

  @override
  String get chooseRepaymentType => 'Выберите тип погашения';

  @override
  String get debtAmount => 'Сумма задолженности';

  @override
  String get continueButtonText => 'Продолжить';

  @override
  String get chooseOptionErrorText => 'Необходимо выбрать тип погашения';

  @override
  String get aboutFullRepayment => 'О полном погашении';

  @override
  String get aboutFullRepaymentCredit => 'О полном погашении кредита';

  @override
  String get mainDebt => 'Основной долг';

  @override
  String get creditAgreementRequirements => 'Требования по кредитному договору';

  @override
  String get repaymentAccountBalance => 'Остаток на счете погашения';

  @override
  String get confirmationAppBarTitle => 'Подтверждение';

  @override
  String get firstContentCheckBoxText => 'Настоящим прошу Банк внести изменения в график погашения по вышеуказанному Кредитному договору';

  @override
  String get secondContentCheckBoxText => 'Подтверждаю и согласен(а), с условием обеспечить на моем счете наличие суммы достаточной для исполнения заявки, иначе Банком будет списан имеющийся на счете остаток, а при отсутствии средств данная заявка будет аннулирована в дату наступления срока ее исполнения';

  @override
  String get needToChooseRepaymentType => 'Необходимо выбрать тип перерасчета';

  @override
  String get needToChooseAmount => 'Необходимо ввести сумму досрочного гашения';

  @override
  String get paymentScheduleRecalcType => 'Тип перерасчета графика платежей';

  @override
  String get chooseRecalcType => 'Выберите тип перерасчета';

  @override
  String get recalcType => 'Тип перерасчета';

  @override
  String get aboutNextPayment => 'О ближайшем платеже';

  @override
  String get overdueDept => 'Просроченная задолженность';

  @override
  String get secondStepTextAmount => 'Расчет является предварительным на основе полученных данных по договору. Окончательная сумма ежемесячного платежа будет рассчитана после исполнения заявки на частично-досрочное погашение кредита.';

  @override
  String get secondStepTextDuration => 'Расчет является предварительным на основе полученных данных по договору. Окончательная дата будет рассчитана после исполнения заявки на частично-досрочное погашение кредита.';

  @override
  String get applicationAccepted => 'Заявка принята';

  @override
  String get applicationNotAccepted => 'Заявка не зарегистрирована';

  @override
  String get applicationErrorMessage => 'Произошла ошибка.\nОтправьте номер ошибки в чат поддержки и оператор поможет завершить регистрацию';

  @override
  String get backToMain => 'На главную';

  @override
  String get paymentDate => 'Дата платежа';

  @override
  String get amount => 'Сумма';

  @override
  String get amountResult => 'Новая сумма ежемесячного платежа будет размещена в графике платежей после исполнения заявки';

  @override
  String get durationResult => 'Новая дата закрытия договора будет размещена в графике платежей после исполнения заявки';

  @override
  String get cancelResultTitle => 'Ваша заявка на досрочное погашение\nбудет аннулирована';

  @override
  String get cancelResultText => 'Статус заявки будет доступен\nв мобильном приложении';

  @override
  String get getHelp => 'Обратиться в чат';

  @override
  String get notAvailable => 'Сервис частичного погашения недоступен по кредитному договору.';

  @override
  String get errorMessage => 'Ваше подтверждение обязательно';

  @override
  String get tryLater => 'Попробуйте позже';

  @override
  String get newAmount => 'Новая сумма ежемесячного платежа';

  @override
  String get newDuration => 'Новый срок закрытия договора';

  @override
  String get aboutPartRepayment => 'О частичном погашении';

  @override
  String get goToChat => 'Открыть чат';

  @override
  String get infoText => 'К дате исполнения досрочного погашения необходимо обеспечить на счете списания сумму, указанную в заявке. Для погашение ежемесячного платежа сумма средств вносится дополнительно.';
}
