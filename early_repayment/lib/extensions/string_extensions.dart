import 'dart:math';

extension StringNumberExtension on String {
  String splitDoubleBySize(int size, [String? suffix]) {
    final chunks = [];
    final parts = split('.');
    final floatingPart = '.${(parts.length == 1 ? '' : parts.last).padRight(2, '0')}';
    final letters = parts.first.split('');

    for (var i = letters.length; i > 0; i -= size) {
      final s = letters.sublist(max(i - size, 0), min(i, letters.length));
      chunks.add(s);
    }
    return '${chunks.reversed.map((e) => e.join('')).join(' ')}$floatingPart${suffix ?? ''}';
  }
}

extension FormatAmount on double {
  String formatToAmount([String? suffix]) => toString().splitDoubleBySize(3, suffix != null ? ' $suffix' : '');
}
