import 'package:intl/date_symbol_data_local.dart';
import 'package:intl/intl.dart';

extension DateTimeStringExtensions on String {
  DateTime isoToDateTime() {
    initializeDateFormatting("ru_RU", null);
    return DateFormat("yyyy-MM", "ru_RU").parse(this);
  }

  DateTime isoToDateTimeWithDate() {
    initializeDateFormatting("ru_RU", null);
    return DateFormat("yyyy-MM-dd", "ru_RU").parse(this);
  }
}
