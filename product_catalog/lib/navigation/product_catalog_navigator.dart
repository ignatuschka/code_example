import 'package:flutter_atb_common/di/get_it_extensions.dart';

abstract class ProductCatalogNavigator {

  void navigateBack();
  Future<void> navigateToProductCatalogScreen();
  void pushNamed(String path);
}

class ProductCatalogInternalNavigator {
  static final _nav = getIt<ProductCatalogNavigator>();

  static navigateBack() => _nav.navigateBack();
  static Future<void> navigateToProductCatalogScreen() => _nav.navigateToProductCatalogScreen();
  void pushNamed(String path) => _nav.pushNamed(path);

}