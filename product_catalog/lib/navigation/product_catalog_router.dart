import 'package:auto_route/auto_route.dart';
import 'package:auto_route/empty_router_widgets.dart';
import 'package:product_catalog/di/product_catalog_di_module.dart';

class ProductCatalogRouter {
  static const root = 'RootProductCatalog';
  static const name = 'RootProductCatalog';
  static const router = AutoRoute(
    path: root,
    name: name,
    page: EmptyRouterPage,
    children: [
      AutoRoute(
        path: productCatalogScreen,
        page: ProductCatalogScreen,
        initial: true,
      ),
    ],
  );
  static const productCatalogScreen = 'product_catalog_screen';
}