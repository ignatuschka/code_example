import 'package:flutter_atb_common/di/get_it_extensions.dart';
import 'package:product_catalog/navigation/product_catalog_navigator.dart';

abstract class ProductCatalogEntryPoint {
  static final _nav = getIt<ProductCatalogNavigator>();
  static Future<void> navigateToProductCatalogScreen() => _nav.navigateToProductCatalogScreen();

}