
import 'package:product_catalog/domain/entity/product_catalog_entity.dart';

abstract class ProductCatalogRepository {
  Future<ProductCatalogEntity> getProductCatalog();
}