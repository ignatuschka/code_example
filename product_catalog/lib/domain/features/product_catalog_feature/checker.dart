import 'package:feature_checker/domain/entity/checker.dart';
import 'package:feature_checker/domain/use_case/checker_deleagate.dart';

const _toggleKey = "CORE-47-products-catalog";

class ProductCatalogChecker implements Checker {
  final CheckerDelegate _delegate;

  ProductCatalogChecker(this._delegate);

  @override
  bool isEnable() => _delegate.isEnable(_toggleKey);
}