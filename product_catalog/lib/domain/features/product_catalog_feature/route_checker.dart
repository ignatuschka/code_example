
import 'package:feature_checker/domain/entity/route_checker.dart';

import 'package:product_catalog/navigation/product_catalog_router.dart';

class ProductCatalogRouteChecker extends RouteChecker {
  @override
  Set<String> get authorizedRoutes => <String>{
    /// Открыть новый продукт
    ProductCatalogRouter.name,
  };
}
