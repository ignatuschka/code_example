
import 'package:feature_checker/domain/entity/feature.dart';

class ProductCatalogFeature extends Feature {
  ProductCatalogFeature({required super.checker, required super.routeChecker});
}
