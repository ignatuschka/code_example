import 'package:feature_checker/domain/use_case/feature_checker.dart';
import 'package:flutter_atb_common/usecase/usecase.dart';

import 'package:product_catalog/domain/entity/banner_entity.dart';
import 'package:product_catalog/domain/entity/catalog_data_entity.dart';
import 'package:product_catalog/domain/entity/product_catalog_entity.dart';
import 'package:product_catalog/domain/entity/product_entity.dart';
import 'package:product_catalog/domain/repository/product_catalog_repository.dart';

class GetProductCatalogUseCase extends UseCase<ProductCatalogEntity, EmptyUsecaseParams> {
  final ProductCatalogRepository repository;
  final FeatureChecker featureChecker;

  GetProductCatalogUseCase(this.repository, this.featureChecker);

  @override
  Future<UseCaseResult<ProductCatalogEntity>> call(EmptyUsecaseParams params) => innerCall(() async {
    final catalog = await repository.getProductCatalog();
    final allBanners = catalog.data.banners;
    final allProducts = catalog.data.products;

    /// Фильтрация баннеров по доступности
    final availableBanners = <BannerEntity>[];
    if (allBanners.isNotEmpty) {
      for (final banner in allBanners) {
        final link = banner.link.trim();
        if (link.isNotEmpty && featureChecker.isAvailableRoute(link.split('/'))) {
          availableBanners.add(banner);
        }
      }
    }

    /// Фильтрация продуктов по доступности
    final availableProducts = <ProductEntity>[];
    if (allProducts.isNotEmpty) {
      for (final product in allProducts) {
        final link = product.link.trim();
        if (link.isNotEmpty && featureChecker.isAvailableRoute(link.split('/'))) {
          availableProducts.add(product);
        }
      }
    }

    return ProductCatalogEntity(
      data: CatalogDataEntity(banners: availableBanners, products: availableProducts),
      result: catalog.result,
    );
  });
}
