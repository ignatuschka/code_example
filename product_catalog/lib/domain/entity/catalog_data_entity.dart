import 'package:product_catalog/domain/entity/banner_entity.dart';
import 'package:product_catalog/domain/entity/product_entity.dart';

class CatalogDataEntity {
  final List<BannerEntity> banners;
  
  final List<ProductEntity> products;

  const CatalogDataEntity({
    required this.banners,
    required this.products,
  });
}