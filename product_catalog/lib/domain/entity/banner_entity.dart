class BannerEntity {
  final int id;

  final String image;

  final String link;

  final bool isVisible;

  final int bannerCategoryId;

  final int productCategoryId;

  const BannerEntity({
    required this.id, 
    required this.image,
    required this.link,
    required this.isVisible,
    required this.bannerCategoryId,
    required this.productCategoryId
  });
}