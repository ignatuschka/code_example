import 'package:product_catalog/domain/entity/catalog_data_entity.dart';
import 'package:product_catalog/domain/entity/result_entity.dart';

class ProductCatalogEntity {
  final ResultEntity? result;
  
  final CatalogDataEntity data;

  const ProductCatalogEntity({
    this.result,
    required this.data,
  });
}