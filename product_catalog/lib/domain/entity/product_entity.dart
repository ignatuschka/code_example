class ProductEntity {
  final int id;

  final String header;

  final String text;

  final String image;

  final String link;

  final bool isVisible;

  final int productCategoryId;

  const ProductEntity({
    required this.id,
    required this.header,
    required this.text,
    required this.image,
    required this.link,
    required this.isVisible,
    required this.productCategoryId,
  });
}