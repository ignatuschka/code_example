class ResultEntity {
  final DateTime? timestamp;

  final int? status;

  final String? service;

  final String? code;

  final String? message;

  const ResultEntity({
    this.timestamp,
    this.status,
    this.service,
    this.code,
    this.message
  });
}