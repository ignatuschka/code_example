import 'package:json_annotation/json_annotation.dart';
import 'package:product_catalog/domain/entity/banner_entity.dart';

part 'banner_dto.g.dart';

@JsonSerializable(createToJson: false)
class BannerDto {
  final int? id;

  final String? image;

  final String? link;

  final bool? isVisible;

  final int? bannerCategoryId;

  final int? productCategoryId;

  const BannerDto({
    this.id, 
    this.image,
    this.link,
    this.isVisible,
    this.bannerCategoryId,
    this.productCategoryId,
  });

  factory BannerDto.fromJson(Map<String, dynamic> json) => _$BannerDtoFromJson(json);

  BannerEntity toEntity() {
    if(id == null) throw Exception("'id' must not be null");
    if(image == null) throw Exception("'image' must not be null");
    if(link == null) throw Exception("'link' must not be null");
    if(isVisible == null) throw Exception("'isVisible' must not be null");
    if(bannerCategoryId == null) throw Exception("'bannerCategoryId' must not be null");

    return BannerEntity(
        id: id!,
        image: image!,
        link: link!,
        isVisible: isVisible!,
        bannerCategoryId: bannerCategoryId!,
        productCategoryId: productCategoryId!,
      );
  }
}

