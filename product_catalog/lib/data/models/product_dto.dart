import 'package:json_annotation/json_annotation.dart';
import 'package:product_catalog/domain/entity/product_entity.dart';

part 'product_dto.g.dart';

@JsonSerializable(createToJson: false)
class ProductDto {
  final int? id;

  final String? header;

  final String? text;

  final String? image;

  final String? link;

  final bool? isVisible;

  final int? productCategoryId;

  const ProductDto({
    this.id, 
    this.header,
    this.text,
    this.image,
    this.link,
    this.isVisible,
    this.productCategoryId,
  });

  factory ProductDto.fromJson(Map<String, dynamic> json) => _$ProductDtoFromJson(json);

  ProductEntity toEntity() {
    if(id == null) throw Exception("'id' must not be null");
    if(header == null) throw Exception("'header' must not be null");
    if(text == null) throw Exception("'text' must not be null");
    if(image == null) throw Exception("'image' must not be null");
    if(link == null) throw Exception("'link' must not be null");
    if(isVisible == null) throw Exception("'isVisible' must not be null");
    if(productCategoryId == null) throw Exception("'productCategoryId' must not be null");

    return ProductEntity(
        id: id!,
        header: header!,
        text: text!,
        image: image!,
        link: link!,
        isVisible: isVisible!,
        productCategoryId: productCategoryId!,
      );
  }
}