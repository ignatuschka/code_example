import 'package:json_annotation/json_annotation.dart';
import 'package:product_catalog/data/models/banner_dto.dart';
import 'package:product_catalog/data/models/product_dto.dart';
import 'package:product_catalog/domain/entity/catalog_data_entity.dart';

part 'catalog_data_dto.g.dart';

@JsonSerializable(createToJson: false)
class CatalogDataDto {
  final List<BannerDto>? banners;
  
  final List<ProductDto>? products;

  const CatalogDataDto({
    this.banners,
    this.products,
  });

  factory CatalogDataDto.fromJson(Map<String, dynamic> json) => _$CatalogDataDtoFromJson(json);

  CatalogDataEntity toEntity() {
    if(banners == null) throw Exception("'banners' must not be null");
    if(products == null) throw Exception("'products' must not be null");

    return CatalogDataEntity(
        banners: banners!.map((e) => e.toEntity()).toList(),
        products: products!.map((e) => e.toEntity()).toList(),
      );
  }
}