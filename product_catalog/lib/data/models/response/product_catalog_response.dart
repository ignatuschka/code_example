import 'package:flutter_atb_common/entity/command_entity.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:product_catalog/data/models/catalog_data_dto.dart';
import 'package:product_catalog/data/models/result_dto.dart';
import 'package:product_catalog/domain/entity/catalog_data_entity.dart';
import 'package:product_catalog/domain/entity/product_catalog_entity.dart';

part 'product_catalog_response.g.dart';

@JsonSerializable(createToJson: false)
class ProductCatalogResponse with CommandEntity {
  final ResultDto? result;
  final CatalogDataDto? data;

  ProductCatalogResponse(this.result, this.data);

  factory ProductCatalogResponse.fromJson(Map<String, dynamic> json) => _$ProductCatalogResponseFromJson(json);

  ProductCatalogEntity toEntity() {
    return ProductCatalogEntity(
      result: result?.toEntity(),
      data: data?.toEntity() ?? CatalogDataEntity(banners: [], products: []),
    );
  }
}