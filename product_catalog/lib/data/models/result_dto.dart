import 'package:json_annotation/json_annotation.dart';
import 'package:product_catalog/domain/entity/result_entity.dart';
import 'package:intl/date_symbol_data_local.dart';

part 'result_dto.g.dart';

@JsonSerializable(createToJson: false)
class ResultDto {
  final String? timestamp;

  final int? status;

  final String? service;

  final String? code;

  final String? message;

  const ResultDto({
    this.timestamp,
    this.status,
    this.service,
    this.code,
    this.message
  });

  factory ResultDto.fromJson(Map<String, dynamic> json) => _$ResultDtoFromJson(json);

  ResultEntity toEntity() {
    return ResultEntity(
        timestamp: timestamp!.isoToDateTime(),
        status: status,
        service: service,
        code: code,
        message: message,
      );
  }
}

extension DateTimeStringExtensions on String{
  DateTime isoToDateTime(){
    initializeDateFormatting("ru_RU", null);
    return DateTime.parse(this);
  }
}