import 'package:product_catalog/data/data_sources/remote_data_source.dart';
import 'package:product_catalog/domain/entity/product_catalog_entity.dart';
import 'package:product_catalog/domain/repository/product_catalog_repository.dart';

class ProductCatalogRepositoryImpl extends ProductCatalogRepository{
  final ProductCatalogRemoteDataSource remoteDataSource;

  ProductCatalogRepositoryImpl({required this.remoteDataSource});

  @override
  Future<ProductCatalogEntity> getProductCatalog() async {
    final catalog = await remoteDataSource.fetchProductCatalog();
    return catalog.toEntity();
  }
}