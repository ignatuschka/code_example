import 'package:dio/dio.dart';
import 'package:api/api_client/api_client.dart';
import 'package:product_catalog/data/models/response/product_catalog_response.dart';
import 'package:retrofit/retrofit.dart';

part 'product_catalog_api.g.dart';

@RestApi()
abstract class ProductCatalogApi {
  factory ProductCatalogApi(ApiClient client) => _ProductCatalogApi(client.dio);

  @GET('/atb-gateway/mobile/api/msfl/v1/business-products/products-catalog')
  Future<ProductCatalogResponse> fetchProductCatalog();
}
