import 'package:product_catalog/data/models/response/product_catalog_response.dart';
import 'package:product_catalog/data/network/product_catalog_api.dart';
import 'package:api/exceptions.dart';

abstract class ProductCatalogRemoteDataSource {
  Future<ProductCatalogResponse> fetchProductCatalog();
}

class ProductCatalogRemoteDataSourceImpl
    extends ProductCatalogRemoteDataSource {
  final ProductCatalogApi _productCatalogApi;

  ProductCatalogRemoteDataSourceImpl(this._productCatalogApi);

  @override
  Future<ProductCatalogResponse> fetchProductCatalog() =>
      checkNetErrors(() => _productCatalogApi.fetchProductCatalog());
}
