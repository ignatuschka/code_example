import 'package:api/api_client.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_atb_common/di/get_it_extensions.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:product_catalog/data/data_sources/remote_data_source.dart';
import 'package:product_catalog/data/network/product_catalog_api.dart';
import 'package:product_catalog/data/repository/product_catalog_repository_impl.dart';
import 'package:product_catalog/domain/repository/product_catalog_repository.dart';
import 'package:product_catalog/domain/use_case/get_product_catalog_use_case.dart';
import 'package:product_catalog/navigation/product_catalog_navigator.dart';
import 'package:product_catalog/presentation/bloc/product_catalog_bloc.dart';
import 'package:product_catalog/presentation/widgets/product_catalog_widget.dart';

class ProductCatalogDIModule extends BaseDIModule {
  @override
  void updateInjections(GetIt instance) {
    instance.registerLazySingleton<ProductCatalogRemoteDataSource>(
        () => ProductCatalogRemoteDataSourceImpl(instance.get()));

    instance.registerLazySingleton<ProductCatalogRepository>(
        () => ProductCatalogRepositoryImpl(remoteDataSource: instance.get()));

    instance.registerFactory(() => GetProductCatalogUseCase(instance.get(), instance.get()));

    instance.registerLazySingleton(() =>
        ProductCatalogApi(instance.get(instanceName: ApiClientType.base.name)));
  }
}

class ProductCatalogScreen extends StatefulWidget {
  const ProductCatalogScreen({super.key});

  @override
  State<ProductCatalogScreen> createState() => _ProductCatalogScreenState();
}

class _ProductCatalogScreenState extends State<ProductCatalogScreen> {
  GetIt get getIt => GetIt.I;

  // @override
  // void initState() {
  //   super.initState();
  //   getIt.pushNewScope(scopeName: ProductCatalogRouter.root);
  //   getIt.installModule(ProductCatalogDIModule());
  // }
  //
  // @override
  // void dispose() {
  //   getIt.popScopesTill(ProductCatalogRouter.root);
  //   super.dispose();
  // }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => ProductCatalogBloc(getProductCatalogUseCase: getIt(), errorHandler: getIt(), navigator: ProductCatalogInternalNavigator(),),
      child: const ProductCatalogWidget(),
    );
  }
}
