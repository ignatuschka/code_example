import 'package:built_value/built_value.dart';
import 'package:flutter_atb_common/usecase/def_state.dart';
import 'package:product_catalog/domain/entity/banner_entity.dart';
import 'package:product_catalog/domain/entity/product_entity.dart';

part 'product_catalog_state.g.dart';

abstract class ProductCatalogState
    with DefState<ProductCatalogState>
    implements Built<ProductCatalogState, ProductCatalogStateBuilder> {
  List<BannerEntity> get bannersList;

  List<ProductEntity> get productsList;

  List<BannerEntity> get productBanner;

  ProductCatalogState._();

  factory ProductCatalogState([void Function(ProductCatalogStateBuilder)? updates]) = _$ProductCatalogState;

  factory ProductCatalogState.initial() => ProductCatalogState((b) => b
    ..isLoading = true
    ..bannersList = []
    ..productsList = []
    ..productBanner = []);

  ProductCatalogState setBannersList(List<BannerEntity>? bannersList) =>
      rebuild((b) => (b)..bannersList = bannersList);

  ProductCatalogState setProductsList(List<ProductEntity>? productsList) =>
      rebuild((b) => (b)..productsList = productsList);

  ProductCatalogState setProductBannerList(List<BannerEntity>? productBanner) =>
      rebuild((b) => (b)..productBanner = productBanner);
}
