abstract class ProductCatalogEvent {
  const ProductCatalogEvent();
}

class ProductCatalogInitialEvent extends ProductCatalogEvent {}

class ProductCatalogTapEvent extends ProductCatalogEvent {
  String path;
  ProductCatalogTapEvent(this.path);
}
