import 'package:flutter_atb_common/bloc/base_bloc.dart';
import 'package:flutter_atb_common/bloc/news.dart';
import 'package:flutter_atb_common/usecase/usecase.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:product_catalog/domain/entity/banner_entity.dart';
import 'package:product_catalog/domain/entity/catalog_data_entity.dart';
import 'package:product_catalog/domain/entity/product_catalog_entity.dart';
import 'package:product_catalog/domain/entity/product_entity.dart';
import 'package:product_catalog/domain/use_case/get_product_catalog_use_case.dart';
import 'package:product_catalog/navigation/product_catalog_navigator.dart';
import 'package:product_catalog/presentation/bloc/product_catalog_event.dart';
import 'package:product_catalog/presentation/bloc/product_catalog_state.dart';
import 'package:flutter_atb_common/exception/error_handler.dart';

class ProductCatalogBloc extends BaseBloc<ProductCatalogEvent, ProductCatalogState> {
  final GetProductCatalogUseCase getProductCatalogUseCase;
  final ProductCatalogInternalNavigator navigator;

  ProductCatalogBloc({
    required this.getProductCatalogUseCase,
    required this.navigator,
    required ErrorHandler errorHandler,
  }) : super(ProductCatalogState.initial(), errorHandler: errorHandler) {
    on<ProductCatalogInitialEvent>(_initialEvent);
    on<ProductCatalogTapEvent>((event, emit) => _tap(event.path, emit),);
    add(ProductCatalogInitialEvent());
  }

  Future<void> _initialEvent(ProductCatalogEvent event, Emitter<ProductCatalogState> emit) async {
    emit(state.setLoading(true));
    await processUseCase<ProductCatalogEntity>(
      () => getProductCatalogUseCase(EmptyUsecaseParams()),
      onSuccess: (catalog) {
        final CatalogDataEntity data = catalog.data;
        final List<BannerEntity> banners = data.banners;
        final List<ProductEntity> products = data.products;
        final List<BannerEntity> visibleBanners = [];
        final List<ProductEntity> visibleProducts = [];
        final List<BannerEntity> productBanners = [];

        for (var element in banners) {
          if (element.isVisible == true) {
            if (element.bannerCategoryId == 1) {
              visibleBanners.add(element);
            }
            if (element.bannerCategoryId == 2) {
              productBanners.add(element);
            }
          }
        }

        for (var element in products) {
          if (element.isVisible == true) {
            visibleProducts.add(element);
          }
        }

        emit(state
            .setLoading(false)
            .setBannersList(visibleBanners)
            .setProductsList(visibleProducts)
            .setProductBannerList(productBanners));
      },
    );
  }

  @override
  void handleUseCaseError(Exception error) {
    errorHandler?.proceed(error, (message) {
      addNews(ErrorWithPopBlocNews(message));
      emit(state.setLoading(false));
    });
  }

  void _tap(String path, Emitter<ProductCatalogState> emit) {
    navigator.pushNamed(path);
  }
}
