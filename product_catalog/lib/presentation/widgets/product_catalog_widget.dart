import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_atb_common/bloc/bloc_instance_provider.dart';
import 'package:flutter_atb_common/extensions/build_context_extenstions.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:product_catalog/navigation/product_catalog_navigator.dart';
import 'package:product_catalog/presentation/bloc/product_catalog_bloc.dart';
import 'package:product_catalog/presentation/bloc/product_catalog_event.dart';
import 'package:product_catalog/presentation/bloc/product_catalog_state.dart';
import 'package:product_catalog/resources/product_catalog_text.dart';
import 'package:ui/resources/ui_text.dart' as ui_text;
import 'package:ui/widgets/loading_widget.dart';
import 'package:ui_kit/ui_kit.dart';

class ProductCatalogWidget extends StatefulWidget {
  const ProductCatalogWidget({Key? key}) : super(key: key);

  @override
  State<ProductCatalogWidget> createState() => _ProductCatalogWidgetState();
}

class _ProductCatalogWidgetState extends StateWithBLoc<ProductCatalogBloc, ProductCatalogWidget> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: HeaderInner(
        onBackButtonPressed: () => ProductCatalogInternalNavigator.navigateBack(),
        title: ProductCatalogText.appBarTitle,
      ),
      body: BlocBuilder<ProductCatalogBloc, ProductCatalogState>(
          bloc: bloc,
          builder: (context, state) {
            final productsList = state.productsList;
            final productItems = <Widget>[];
            final productBannerList = state.productBanner;
            final bannerList = state.bannersList;
            final bannerItems = <Widget>[];
            final isLoading = state.isLoading;
            if (state.productsList.isNotEmpty) {
              for (var element in bannerList) {
                bannerItems.add(Padding(
                  padding: const EdgeInsets.fromLTRB(8, 8, 8, 4),
                  child: InkWell(
                    borderRadius: BorderRadius.circular(16.0),
                    onTap: () {
                      context.read<ProductCatalogBloc>().add(ProductCatalogTapEvent(element.link));
                    },
                    child: CachedNetworkImage(
                      height: 120,
                      imageUrl: element.image,
                      fit: BoxFit.fill,
                    ),
                  ),
                ));
              }
              for (var element in productsList) {
                productItems.add(Padding(
                  padding: const EdgeInsets.fromLTRB(16, 0, 16, 16),
                  child: ProductCard(
                    title: element.header,
                    subtitle: element.text,
                    leading: CachedNetworkImage(
                      imageUrl: element.image,
                      width: 44,
                      height: 44,
                    ),
                    onPressed: () => context.read<ProductCatalogBloc>().add(ProductCatalogTapEvent(element.link)),
                  ),
                ));
                if (productItems.length == 1 && productBannerList.isNotEmpty) {
                  productItems.add(Padding(
                    padding: const EdgeInsets.fromLTRB(16, 0, 16, 16),
                    child: InkWell(
                      borderRadius: BorderRadius.circular(16.0),
                      onTap: () {
                        context.read<ProductCatalogBloc>().add(ProductCatalogTapEvent(productBannerList.first.link));
                      },
                      child: CachedNetworkImage(
                        imageUrl: productBannerList.first.image,
                        fit: BoxFit.fill,
                      ),
                    ),
                  ));
                }
              }

              return SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SingleChildScrollView(
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 8),
                        child: Row(
                          children: bannerItems,
                        ),
                      ),
                      scrollDirection: Axis.horizontal,
                    ),
                    Container(
                      padding: const EdgeInsets.fromLTRB(16, 20, 0, 8),
                      margin: EdgeInsets.only(bottom: 4),
                      height: 52,
                      child: Text(
                        ProductCatalogText.products,
                        style: context.textTheme.titleMedium,
                      ),
                    ),
                    Column(
                      children: productItems,
                    ),
                  ],
                ),
              );
            } else if (isLoading) {
              return const LoadingWidget(title: ui_text.UiText.loading);
            } else {
              return const Center(child: Text(ProductCatalogText.listIsEmpty));
            }
          }),
    );
  }
}
