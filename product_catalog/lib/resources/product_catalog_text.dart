abstract class ProductCatalogText {
  static const appBarTitle = 'Продуктовый каталог';
  static const products = 'Продукты';
  static const listIsEmpty = 'Каталог пуст';
}