package su.atb.mobileapp.application_info

import android.content.Context
import io.flutter.embedding.engine.plugins.FlutterPlugin
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel

private const val GET_APP_VERSION = "get_app_version"

private const val METHOD_NOT_IMPLEMENTED_ERROR_CODE = "0"

class AppVersionPlugin : MethodChannel.MethodCallHandler, FlutterPlugin {

    val METHOD_CHANNEL_NAME: String
        get() = "app_version_channel"

    private var applicationContext: Context? = null
    private var pluginBinding: FlutterPlugin.FlutterPluginBinding? = null
    protected var channel: MethodChannel? = null

    override fun onAttachedToEngine(binding: FlutterPlugin.FlutterPluginBinding) {
        applicationContext = binding.applicationContext
        pluginBinding = binding
        channel = MethodChannel(binding.binaryMessenger, METHOD_CHANNEL_NAME)
        channel!!.setMethodCallHandler(this)

    }

    override fun onDetachedFromEngine(binding: FlutterPlugin.FlutterPluginBinding) {
        applicationContext = null
        pluginBinding = null
        channel?.setMethodCallHandler(null)
        channel = null
    }

    @Suppress("UNCHECKED_CAST")
    override fun onMethodCall(call: MethodCall, result: MethodChannel.Result) {
        handleMethodCall(call, result)
    }

    open fun handleMethodCall(call: MethodCall, result: MethodChannel.Result) {
        when (call.method) {
            GET_APP_VERSION -> {
                val packageManager = applicationContext!!.packageManager
                val info = packageManager.getPackageInfo(applicationContext!!.packageName, 0)
                val version = info.versionName
                result.success(version)
            }
            else -> {

                result.error(
                    METHOD_NOT_IMPLEMENTED_ERROR_CODE,
                    "Method ${call.method} is not implemented in platform",
                    null
                )
            }
        }
    }
}
