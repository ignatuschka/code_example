import Flutter
import UIKit

private enum Method: String {
  case getAppVersion = "get_app_version"
}

public class SwiftAppVersionPlugin: NSObject, FlutterPlugin {
  public static func register(with registrar: FlutterPluginRegistrar) {
    let channel = FlutterMethodChannel(name: "app_version_channel", binaryMessenger: registrar.messenger())
    let instance = SwiftAppVersionPlugin()
    registrar.addMethodCallDelegate(instance, channel: channel)
  }

  public func handle(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
    switch Method(rawValue: call.method) {
    case .getAppVersion: getAppVersion(call, pass: result)
      
    default:
      result(FlutterMethodNotImplemented)
    }
  }

    private func getAppVersion (_ call: FlutterMethodCall, pass result: @escaping FlutterResult){
      let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
      result(appVersion)
    }
}
