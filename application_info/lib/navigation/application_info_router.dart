import 'package:application_info/di/application_info_di_module.dart';
import 'package:auto_route/auto_route.dart';
import 'package:auto_route/empty_router_widgets.dart';

class ApplicationInfoRouter {
  static const root = 'RootApplicationInfo';
  static const router = AutoRoute(
    path: root,
    name: root,
    page: EmptyRouterPage,
    children: [
      AutoRoute(
        page: ApplicationInfoScreen,
        path: applicationInfoScreenWidget,
        initial: true,
      ),
    ]
  );
  static const String applicationInfoScreenWidget = 'application_info_screen_widget';
}