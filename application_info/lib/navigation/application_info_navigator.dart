import 'package:flutter_atb_common/di/get_it_extensions.dart';

abstract class ApplicationInfoNavigator {
  void navigateToApplicationInfoScreen();
  void pop();
}

class ApplicationInfoInternalNavigator {
  static final nav = getIt<ApplicationInfoNavigator>();

  static navigateToApplicationInfo() => nav.navigateToApplicationInfoScreen();

  static pop() => nav.pop();
}
