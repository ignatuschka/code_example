import 'package:application_info/navigation/application_info_navigator.dart';
import 'package:flutter_atb_common/di/get_it_extensions.dart';

abstract class ApplicationInfoEntryPoint {
  static final ApplicationInfoNavigator _nav = getIt<ApplicationInfoNavigator>();
  static void navigateToApplicationInfoScreen() => _nav.navigateToApplicationInfoScreen();
}
