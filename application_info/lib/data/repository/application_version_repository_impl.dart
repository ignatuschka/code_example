import 'package:application_info/domain/repository/application_version_repository.dart';
import 'package:application_info/plugins/plugin_worker/app_version_plugin_worker.dart';

class AppVersionRepositoryImpl implements AppVersionRepository{
  final AppVersionPluginWorker worker;

  AppVersionRepositoryImpl(this.worker);

  @override
  Future<String> getAppVersion() => worker.getAppVersion();
}