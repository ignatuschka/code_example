import 'package:application_info/data/repository/application_version_repository_impl.dart';
import 'package:application_info/domain/repository/application_version_repository.dart';
import 'package:application_info/domain/use_case/get_application_version_use_case.dart';
import 'package:application_info/plugins/plugin_worker/app_version_plugin_worker.dart';
import 'package:application_info/presentation/bloc/application_info_bloc.dart';
import 'package:application_info/presentation/widgets/application_info_screen_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter_atb_common/di/get_it_extensions.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';

class ApplicationInfoDIModule extends BaseDIModule {
  @override
  void updateInjections(GetIt instance) {

    instance.registerLazySingleton<AppVersionRepository>(() => AppVersionRepositoryImpl(getIt()));

    instance.registerFactory<GetAppVersionUseCase>(() => GetAppVersionUseCase(instance.get()));
    
    instance.registerLazySingleton<AppVersionPluginWorker>(() => AppVersionPluginWorkerImpl());
  }
}

class ApplicationInfoScreen extends StatefulWidget {
  const ApplicationInfoScreen({Key? key}) : super(key: key);

  @override
  State<ApplicationInfoScreen> createState() => _ApplicationInfoScreenState();
}

class _ApplicationInfoScreenState extends State<ApplicationInfoScreen> {
  GetIt get getIt => GetIt.I;

  // @override
  // void initState() {
  //   super.initState();
  //   getIt.pushNewScope(scopeName: ApplicationInfoRouter.root);
  //   getIt.installModule(ApplicationInfoDIModule());
  // }

  // @override
  // void dispose() {
  //   getIt.popScopesTill(ApplicationInfoRouter.root);
  //   super.dispose();
  // }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => ApplicationInfoBloc(getAppVersionUseCase: getIt(), errorHandler: getIt(),),
      child: const ApplicationInfoScreenWidget(),
    );
  }
}