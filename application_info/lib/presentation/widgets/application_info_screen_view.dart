import 'package:application_info/navigation/application_info_navigator.dart';
import 'package:application_info/presentation/bloc/application_info_bloc.dart';
import 'package:application_info/presentation/bloc/application_info_state.dart';
import 'package:application_info/resourses/application_info_text.dart';
import 'package:flutter/material.dart';
import 'package:ui_kit/theme.dart';
import 'package:flutter_atb_common/extensions/build_context_extenstions.dart';
import 'package:ui_kit/ui_kit.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ui/resources/ui_text.dart' as ui_text;
import 'package:ui/widgets/loading_widget.dart';

class ApplicationInfoScreenWidget extends StatelessWidget {
  const ApplicationInfoScreenWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final colors = context.theme.extension<AtbColors>();
    return BlocBuilder<ApplicationInfoBloc, ApplicationInfoState>(
        bloc: context.read<ApplicationInfoBloc>(),
        builder: (context, state) {
          final isLoading = state.isLoading;
          if (isLoading) {
            return const LoadingWidget(title: ui_text.UiText.loading);
          } else {
            return Scaffold(
              appBar: HeaderInner(
              onBackButtonPressed: () => ApplicationInfoInternalNavigator.pop(),
              title: ApplicationInfoText.appBarTitle,
            ),
              body: ConstrainedBox(
                constraints: BoxConstraints(
                  minWidth: MediaQuery.of(context).size.width,
                  minHeight: MediaQuery.of(context).size.height,
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const SizedBox(
                      height: 88,
                    ),
                    Image.asset(UiAssets.logoATB,
                        height: 48, width: 228, package: UiText.packageName),
                    const SizedBox(
                      height: 40,
                    ),
                    Text(
                      '${ApplicationInfoText.applicationVersionNumber} ${state.appVersion}',
                      style: context.textTheme.bodyLarge,
                    ),
                    const Spacer(),
                    Container(
                      alignment: Alignment.center,
                      margin: const EdgeInsets.symmetric(horizontal: 18),
                      child: Text(
                        ApplicationInfoText.bankLicense,
                        style: context.textTheme.bodyMedium
                            ?.copyWith(color: colors?.neutral[50]),
                        textAlign: TextAlign.center,
                      ),
                    ),
                    const SizedBox(
                      height: 84,
                    ),
                  ],
                ),
              ),
            );
          }
        });
  }
}
