import 'package:analytics/domain/models/analytics_error.dart';
import 'package:application_info/domain/use_case/get_application_version_use_case.dart';
import 'package:application_info/presentation/bloc/application_info_event.dart';
import 'package:application_info/presentation/bloc/application_info_state.dart';
import 'package:application_info/resourses/application_info_text.dart';
import 'package:flutter_atb_common/bloc/base_bloc.dart';
import 'package:flutter_atb_common/exception/error_handler.dart';
import 'package:flutter_atb_common/usecase/usecase.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'dart:async';
import 'package:analytics/domain/interactor/interactor.dart';
import 'package:flutter_atb_common/di/get_it_extensions.dart';

class ApplicationInfoBloc
    extends BaseBloc<ApplicationInfoEvent, ApplicationInfoState> {
  final GetAppVersionUseCase getAppVersionUseCase;
  final AnalyticsInteractor analyticsInteractor = getIt();

  ApplicationInfoBloc({
    required this.getAppVersionUseCase,
    required ErrorHandler errorHandler,
  }) : super(
          ApplicationInfoState.initial(),
          errorHandler: errorHandler,
        ) {
    on<ApplicationInfoInitial>(_initial);
    add(ApplicationInfoInitial());
  }

  Future<void> _initial(
      ApplicationInfoInitial event, Emitter<ApplicationInfoState> emit) async {
    emit(state.setLoading(true));

    await processUseCase<String>(
      () => getAppVersionUseCase(EmptyUsecaseParams()),
      onSuccess: (result) {
        final String appVersion = result;

        emit(state
        .setLoading(false)
        .setAppVersion(appVersion));
      },
      onError: (error) {
        analyticsInteractor.reportError(
            analyticsError:
                AnalyticsError(message: ApplicationInfoText.errorAppVersion));
      },
    );
  }
}
