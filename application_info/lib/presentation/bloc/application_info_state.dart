import 'package:flutter_atb_common/usecase/def_state.dart';
import 'package:built_value/built_value.dart';

part 'application_info_state.g.dart';

abstract class ApplicationInfoState with DefState<ApplicationInfoState> implements Built<ApplicationInfoState, ApplicationInfoStateBuilder> {
  String get appVersion;

  ApplicationInfoState._();

  factory ApplicationInfoState([void Function(ApplicationInfoStateBuilder)? updates]) = _$ApplicationInfoState;
  
  ApplicationInfoState setAppVersion(String appVersion) => rebuild((b) => b
      ..appVersion = appVersion);

  factory ApplicationInfoState.initial() => ApplicationInfoState((b) => b
      ..appVersion = ''
      ..isLoading = true
  );
}
