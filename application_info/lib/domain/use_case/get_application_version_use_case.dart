import 'package:application_info/domain/repository/application_version_repository.dart';
import 'package:flutter_atb_common/usecase/usecase.dart';

class GetAppVersionUseCase extends UseCase<String, EmptyUsecaseParams> {
  final AppVersionRepository repository;

  GetAppVersionUseCase(this.repository);

  @override
  Future<UseCaseResult<String>> call(EmptyUsecaseParams params) => innerCall(() => repository.getAppVersion());
}