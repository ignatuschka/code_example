abstract class AppVersionRepository{
  Future<String> getAppVersion();
}