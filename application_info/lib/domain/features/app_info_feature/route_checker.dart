
import 'package:feature_checker/domain/entity/route_checker.dart';

import 'package:application_info/navigation/application_info_router.dart';

class AppInfoRouteChecker extends RouteChecker {
  @override
  Set<String> get authorizedRoutes => {
    /// Экран информации о приложении
    ApplicationInfoRouter.root
  };
}
