
import 'package:feature_checker/domain/entity/feature.dart';

class AppInfoFeature extends Feature {
  AppInfoFeature({required super.checker, required super.routeChecker});
}
