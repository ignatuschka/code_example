
import 'package:feature_checker/domain/entity/checker.dart';

class AppInfoChecker implements Checker {
  AppInfoChecker();

  /// Временно всегда возвращает true для работы диплинков
  /// todo: после аналитики изменить
  @override
  bool isEnable() => true;
}
