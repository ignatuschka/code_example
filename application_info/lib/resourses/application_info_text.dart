abstract class ApplicationInfoText {
  static const appBarTitle = 'О приложении';
  static const applicationVersionNumber = 'Версия приложения:';  
  static const bankLicense = 'Универсальная лицензия ЦБ РФ №1810,\n© Азиатско-Тихоакеанский Банк (АО), 2023\nwww.atb.su';
  static const errorAppVersion = 'Версия приложения не определена';  
}