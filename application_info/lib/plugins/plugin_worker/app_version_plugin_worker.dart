import 'package:application_info/plugins/app_version_plugin.dart';

abstract class AppVersionPluginWorker {
  Future<String> getAppVersion();
}

class AppVersionPluginWorkerImpl implements AppVersionPluginWorker {
  @override
  Future<String> getAppVersion() => AppVersionPlugin.getAppVersion();
}
