import 'dart:async';

import 'package:flutter/services.dart';

const channelName = "app_version_channel";

enum Method {
  getAppVersion,
}

extension MethodExt on Method {
  static const _methodsMap = <Method, String>{
    Method.getAppVersion: "get_app_version",
  };

  String get name {
    return MethodExt._methodsMap[this]!;
  }
}

class AppVersionPlugin {
  static const MethodChannel _channel = const MethodChannel(channelName);

  static FutureOr<T?> _invoke<T>(Method method) async {
    return await _channel.invokeMethod<T>(method.name);
  }

  static Future<String> getAppVersion() async {
    return await _invoke(
      Method.getAppVersion,
    );
  }
}
